### Unix
##### Schedule:
1. Filesystem Hierarchy Standard
2. Definitions

#### 1. Filesystem Hierarchy Standard
All hierarchy standard could be find with: man hier
| Directory     | Description   |
| ----- |:-------------:|
| /     | Primary  directory of the entire file system (root owner) |
| /bin  | Essential command executable binaries that need to be available in single user mode (for all users). There are for example ls, grep, tar, vim ...|
| /boot | Holds files used in booting the operating system  |
| /boot/grub| Contains the grub configuration files (e.g grub.cfg in manjaro) including boot-up images and sounds |
| /dev | Devices files that are connect to the system. (you need to remember that in linux everything is a file even devices) |
| /dev/tty* | tty is a special file, representing the terminal for current process |
| /dev/sda* (/dev/sde*) | Storage devices lke hard disks, SSD and flash drives that support (SCSI, SAS, UASP, PATA, SATA or USB mass storage)|
| /dev/nvme* | For storage devices like SSD, that are attached via NVM Express. For example: /dev/nvme0n1p1 -> 0 <-> on controller 0, 1 <-> on device 1, <-> on partition 1 |
| /dev/usb | For most of the USB devices |
| /etc | Host-specific system-wide configuration files np. vimrc, bluetooth/main.conf, passwd (folder keep information that do not match any other folders)|
| /etc/opt | Configuration files for add-on packages that are stored in /opt |
| /home | User home directories, contains saving files personal setting etc. |
| /lib | Libraries essential for binaries in /bin and /sbin |
| /media | Mont points for removable media such as CD-ROMs |
| /mnt | Temporarily mounted filesystems | 
| /opt | Directory for application that are not part of the operating system distribution, but provides and independent source. |
| /proc | Places where we have information about running process |
| /root | Home directory for root user |
| /run | Run-time variable data: Information about the running system since last boot. e.g., currently logged-in users and running daemons. Files from this directory must be removed or truncated at the beginning of the boot process. | 
| /sbin | essentials system binaries eg. vim, whoami |
| /srv | Server directory where server data is stored | 
| /sys | Contains information about devices, drivers, firmware and kernel |
| /tmp | Temporary files (see also /var/tmp). Often not preserved between system reboots, and may be severely size restricted. |
| /usr | Secondary hierarchy ofr read-only user data. Contains the majority of multi-user utilities and applications. |
| /usr/bin | Non-essential command binaries for all users |
| /usr/lib | Libraries for the binaries in /usr/bin and /usr/sbin |
| /usr/local | Tertiary hierarchy for local data, specific to this host. Typically has further subdirectories, e.g. bin, lib, share. |
| /usr/sbin | Non-essential system binaries, e.g., daemons for various network-service |
| /usr/share | Architecture independent shared data |
| /usr/src  | Source code, e.g., the kernel source code with it's headers files |
| /var | Variable - files, files that content do change during operation on system. Like logs, spool files and temporary email files. |
| /var/cache | Application cache data. Such data are locally generated as a result of time-consuming I/O or calculation. The application must be able to regenerate or restore the data. The cached files can be deleted without loss of data. |
| /var/lib | State information. Persistent data modified by programs as they run, e.g., databases, packaging system metadata |
| /var/lock | Lock files. Files keeping track of resource currently in use. |
| /var/log | Various log files |
| /var/run | Run-time variable data. This directory contains system information data describing the system science it was booted. |
| /var/spool | Spool for tasks waiting to be processed, e.g., print queues and outgoing mail queue.|
| /var/tmp | Temporary files to be preserved between reboots |


#### 2. Definitions
Daemons - program or script that running in the background as process not in direct control of a user.


#### 3. Syscalls - linux system calls
It's fundamental interface between an application and the linux kernel. It let's application to hit lvl 0 being at lvl 3. So it means we can send write instruction to processor being at application level (lvl 3)


#### 4. Environmental variables
Environmental variables - In simple words it's kind of config for a running programs for our user env (e.g. if program want to save something in HOME directory of user. He will get it from HOME env).
To print specific env use: echo $VARIABLE_NAME
To print all env use: env

Env var:
* PATH - The search path for commands. It is a colon-separated list of directories in which the shell looks for commands.

#### 5. /etc/* files
* /etc/passwd - files containing information about user, UID, GUI, home directory and about program that need to be executed on login. (password is not contained). Default permission: -rw-r--r-- root root
* /etc/shadows - file contain hash of passwords to a users. Default permission: -rw-------
* /etc/group - contain information about group name, GUI, and users that are in this group. Default permission: -rw-r--r--

#### 6. Process
Each process when it's create got from the system 3 files descriptors:
- stdin (0)
- stdout (1)
- stderr (2) - usually connected to standard output of terminal

* /dev/null - special device which is like black whole. It means when we direct something to this file it disappears and when we try to read this it will give us empty output.

Directions:
* ls -l > file.txt - direct stdout of ls -l to stdin of file.txt
* 2> - direct error of a command
* 2>&1 - direct stderr on stdout (now anything send to stderr will be sent to stdout. & sing is required to inform we want to send it to descriptor 1 and not file 1)
* ls -l >> file.txt - append values to the file.txt if not exist create a file (2>> works similar for errors)
* cat file1 file2 2> err - it will print file content to stdout and any errors to file err
* cat file1 file2 > output > 2>&1 - redirect also stderr to the file
* command > output 2> errors - redirect stdout and stderr to different files