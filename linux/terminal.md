### Terminal most important sutff
#### Shortcuts
 - Ctrl + L - clear the screen
 - Alt + B - move cursor one word to the left
 - Alt + F - move cursor one word to the right
 - Ctrl + A - move cursor at start of the line
 - Ctrl + E - move cursor to the end of the line
 - Ctrl + U - clear line to the beginning
 - Ctrl + K - clear from the cursor to the end of line
 - Ctrl + W - clear just one word
 - Ctrl + C - exit execution of a program or process
 - Ctrl + Z - will sent program to the background


#### Comands
 - '*' - stands for multiple signs (files, folders) in console
 - '?' - stands for a single character (files, folder) in console
 - '.' - stands for our working directory
 - !number - set command from history number [number]
 - command1 ; command2 - run command 1 then command 2
 - command1 | command 2 - directing output of command1 to input of command2
 - man command - getting much more information about command
   * /search_pattern - search in man search_pattern
      * n / N - move to next / previous
 - apropos command - find similar command names
 - whatis command - basic information about command
 - command --help - getting basic information about command
 - pwd - print working directory
 - "cd -" => move to the last pwd result
 - ls -la list directory content, do not ignore . files and list out permission and owner of the file 
    * -l - use a long listing format
    * -a - show . files
 - unlink file - delete file
 - chmod 1777 -R directory/file - give permission to all the users (more specific info below)
    * -R is for recursion (to change permission also for content of the folders)
 - rm -r directory - delete directory with content
    * -r - stands for recursion (to confirm that we want delete folder with it's content)
 - rmdir -p abc/def/ghi - delete catalogs abc, abc/def, abc/def/ghi
 - cp /path/to/source /path/to/destination - copy items, folder from source to destination
 - ps -aux - display all active process
 - pacman -Qi appName - show dependencies and info about application
    * -Q - list installed packages and their files
    * -i - display much more information on a each given package
 - pacman -Rs appName - uninstall software with it's all dependencies
    * -R - uninstalling
    * -s - uninstalling all the dependencies
 - less - display content file in a scroll form
    * /text - search for text while being in less
 - strings file - write all the string data from a file (even from binary file)
 - grep text file - search text in a file
   * ^T - search text that starts with ^T (^T instead of text)
   * -i make ignore uper/lower case distinctions.
 - string file | grep text - search for [text] from response string
 - mv /path/to/source /path/to/destination - move files, folder from source to destination
 - mv filename1 filename2 - rename filename1 to filename2
 - sudo halt - power off the machine
 - sudo reboot - reboot the machine
 - find /path/to/search -name "to search pattern"
   * -iname - case insensitive name
 - whereis program_bin - git you absolute path to program files
 - top - display process from top to bottom of resource consumptions
 - file file_name - determine a file type
 - wc - words counts (counts the words of a text or lines)
   * -l - count lines in text (ps -aux | wc -l => counts amount of process)
 - hostname - print host name (it's also possible to change host name)
 - sync - finish all writing and reading from devices/files and clear all buffers. To check that all operations on devices are actually finished. (Like saving to pendrive)
 - sleep n - sleep for n seconds
 - stat -c "%a %n" * - octal file permission

##### Compression\Decompression
 - zip -r directory_name name_of_zip_file.zip - zip directory
   * -r - is to zip also the content of the directory
 - unzip name_of_zip_file.zip - unzip directory
 - tar -czpf file.tar.gz directory - zip directory to file name
	* c - create directory
	* z - zip
	* p - keeping permission
	* f - save to file.tar.gz
 - tar -ztf package.tar.gz -> reading archive files
 - tar -zxf - unzip directory
	* z - zip
	* x - extract
	* f - file (means this file)

#### Chmod
If we run ls -l we might see something similar to the:
```
$ ls -l
drwx-wx--- 1 user group 4096 07-16 12:21 file.txt
```

In this example we will look into: "drwrwx---"
* d - stands for define permissions for each class (SUID = 4, SGID = 2, Sticky = 1)
* first three: "rwx" - it's for the user class (i.e the file owner)
* next three: "-wx" - it's for the group class
* last three: "---" - it's for the Others class. In this example, users who are not the owner of the file and who are not members of the Group 

Scheme:
```
$ chmod [references][operator][modes] file ...
```

References:
* reference (class) - description
* u (user) - file owner
* g (group) - members of the file's group
* o (others) - users who are neither the file's owner nor members of the file's group
* a (all) - all three of the above, same as ugo

Operators:
* "+" - adds to specified modes the specified classes
* "-" - removes from the modes and specified classes
* "=" - the modes specified are to be made the exact modes for the specified classes

Modes: 
* r (read) (oct = 4)- read a file or list a directory's contents
* w (write) (oct = 2) - write to a file or directory
* x (execute) (oct = 1) - execute a file or recurse a directory tree
* s (setuid bit - SUID) - which tells the OS to execute that program with the userid of its owner. (this options is only for executable files and need to be placed in first three columns). Also when added to group it's SGID, which means whenever file is executed its executed with group permissions.
* t (sticky bit) - users which are not owners could not change names or delete in directory this permission is set on.

SGID example:
```bash
$ chmod g+s file
$ ls -la file
$ -rw-r-Sr-- 1 user user 0 00-00 00:00 file
```

SUID example:
```bash
$ chmod u+s file
$ ls -la file
$ -rwSr--r-- 1 user user 0 00-00 00:00 file
```

Examples:
* chmod u+x file - give a owner execute permission to a file
* chmod ug=rwx file - give owner and group permission to execute, read and write
* chmod u+x,g-x file1 file2 - give owner execute option to file1 and remove execute option from group on fil2

#### chown - change owner
chown [operators...] user[:grupa] files...

#### chgrp - change group
chgrp [operators...] group files...

#### Wildcards
cat [^abc] - print documents name with no letter a, b and c 
rm ./[0-9]* - delete all files that start with the number
cat [abc]* - print all documents that start with a, b or c

#### ln - make links between files
ln fileName linkName - create hard link (it's sees a file even after you delete it or create new one with the same name). But if you update it it will show updates. Link will still work even if we move a file.

ln -s - symbolic link - create link to fileName inside some path. (if we delete a file link will be broken, if we create the same name of the file link will work again). Link will be also broken if we move a file.

#### Date
scheme: date [operand...] [+FORMAT]
* date - just print regular date
* date -u - utc format
* date -r filename - print last modification time of filename
* date +"%d-%m-%R" - print day-month-year
* date +"%H:%M" - print hour:minute
* date +%T - hour:minutes:seconds
* date +%U - week of the year
* date --iso-8601 - ISO 8601 format date


#### df - file system disk space usage
scheme: df [operand...] [file...]
* df --total - print disk space usage with summary
* df -h - print in human readable form
* df -H - print in human readable form but use 1000 for a gb than 1024
* df --sync - call sync before getting information

#### du - information about file/directory space usage
scheme: du [operand...] [files...]
* du -a - count either all the files not only directories
* du ~/Desktop -a - print directories under ~/Desktop size and files inside this directories size
* du -c - print summary after.
* du -s - print only summary for each directory
   - sudo du ~/* -s -h  => print size of each directory inside home directory of user in a human readable form
  
#### file - determine the file type
scheme: file [operand...] file...

#### cmp - compare two files
* cmp -c file1 file2 - print also difference in sign and where this occur
* cmp -i num file1 file2 - ignore first num of bytes
* cmp -l file1 file2 - print each differ byte number and it's oct value

#### Input modifier commands
cat - print files content. If file not specified print anything on stdout that will be sent to stdin
Operands:
* -b number non empty lines
* -s remove empty lines that are right after another and leave just single one

head - print 10 first lines of file/input.
* -c x - print first x signs
* -n x - print first x lines

tail - print 10 last line of file/input.
* -c x - print first x signs
* -n x - print first x lines
* -f - reads to infinity increasing size file

sort - sorting input data
* -n - numeric sort
* -f - ignore size of letters
* -t x - change column separator to x
* -r - reverse sort
Example:
* cat /etc/passwd | sort -n -t ':' -k3 - sort /etc/passwd by UID

uniq - distinct lines of the file (works only for lines around) (to work fully sort filecl)
* -d - print just repeated lines.
* -u - print just unique lines.
* -c - print all lines but count repeated.

wc - word counter
* -l - count lines 
* -c - count characters
* -w - count words

cut - cut part of the file
* -c x-y - print sing number x to y
* -d x  - delete field separator x (default it's tab)
* -f num - separator number


All Examples:
grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' some.log | sort | uniq | wc -l - count number of ip address that were logged in for example
cat login.log | grep "session opened" | cut -d " " -f11 | sort | uniq -c | sort -n - check for user that were logged correctly

#### Grep
Operands:
* -v - print lines that do not match pattern
* -c - count lines that match pattern
* -i - ignore cases
* -n - print lines with this pattern
* -x - chose only lines, which fully match pattern.
* -m num - stop reading file after finding num match
* -o - print only part of lines that match pattern instead of whole line
* -n - print line number of pattern

#### Find - it's for searching files in directories
scheme: find [directory...] [operands] [pattern]
* -name - find by name like: 'a*' or 'test'
* -iname - find by name ignore cases: 'a*' or test
* -redable - searching file is readable
* -type c - searching file is:
  * d - directory
  * f - regular file
  * l - symbolic link
* -user n - owner of the file is user n
* -writable - file is writable
* -exec command \; - running a command on file file is represented by {}
* ! expression - negation of expression
* -not expression - negation of expression
* expression1 -a expression2 - expression1 and expression2
* expression1 -o expression2 - expression1 or expression2
* -regex pattern - using regex to search on files

Example: 
find . -size +5k -exec  grep '#include' {} \; -print - find files that do have '#include' code and are over 5K kilobytes


#### tr - change in string characters
Example: echo 'x' | tr 'x' 'y' -> change x to y