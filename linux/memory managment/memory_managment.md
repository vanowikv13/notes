# Memory Management Unit in linux subsystem.

### Address translation
Lets say we have an program and this program is accessing some memory location to make program more flexible, we need to use relative address of memory. Then this address is going to processor and process using address translation (hardware-based address translation) decode actual address in memory. Changing virtual to physical. To let this process happen we need the system support. Systems here need to manage memory and take care what processor is fetching.

How does it look?

![](img/address_translation.png)

Why does it need?
Mostly because it lets our program be more flexible. For example our program is compiled with address from range 0 to 12 but this space is actually used by some other program to run it now we would need to compile it again. If we use address translation we can place our program anywhere in memory.

### Memory Management Unit (MMU) in simple words
Converts logical addresses (created by processor) into physicals addresses (address in memory).

### Static linking
System libraries are treated as any other modules and they are added to binary file. Advantages of it it's that binary is more independent it doesn't really on system libraries, but disadvantages is waste of memory.

Static libraries in linux systems are specified with *.a file.

### Dynamic liking
System libraries and packages are loaded into a binary at run-time.

System loads shared libraries that doesn't need to be included in binary, and many programs can use them:
* *.so - linux shared libraries
* *.dll - windows shared libraries

In dynamic linked binary, function from shared libraries are called with the help of PLT (Procedure Linkage Table) that check if address of our function is in GOT (global offset table) if address is not specified there then called is ld.so (for linux) which is dynamic linker / loader, after that we have address to our functino (e.g printf). Then it will call our function form shared library with correct address.