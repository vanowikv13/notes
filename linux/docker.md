## Docker

List of all images that could be used is on docker hub: https://hub.docker.com/

#### Usage commands
```bash
#The following command runs an ubuntu container, attaches #interactively to your local command-line session, and runs /bin/#bash. (if you do not have image locally docker will pull it out for you, and container do not save any things you do)

docker run -i -t ubuntu /bin/bash
```

To Download image form Repository use:
* docker pull image_name_from_repo

Search for docker images in repositories:
* docker search image_name

Print running containers:
* docker ps

Build container with dockerfile:
* docker build --rm -t tag_name directory_with_dockerfile

Run container with:
* docker run --rm --publish machine_port:container_port --detach --name name tag_name
  * --rm - delete previous running image
  * --publish - make traffic internal calls on port 8000 to container image 8080
  * --detach - will run container in the background
  * --name - specify the name for container

Delete container (even when it's running):
* docker rm --force bb

List your docker images:
* docker images

Remove docker image:
* docker rmi image_id

Print logs about container:
* docker logs container_name

Print logs and listen to new one:
* docker logs -f container_name

In docker default network mode is bridge

Run/restart/stop instance of a container:
* docker start/restart/stop container_name

Details about the container:
* docker inspect container_name

Container statistics:
* docker stats

To access bash console run:
* docker exec -it container_name /bin/bash