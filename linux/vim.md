### vim shortcuts and tips:
 - i - start intert mode (it's for editing file) (status bar will change to insert)
 - esc - escape editing mode
 - :w - > save the file
 - :q - > exit editor
 - :wq - save and exit

 - :syntax on - set syntax hilighting
 - :set number - number on the row 
 - :set exandtab shiftwidth=4 softtabstop=4 - nice to set

 - vim -O file1 file2 - open file verticly
    * ctrl + w + w - swap between files open verticaly

 - shift + o - make new line above cursor and start insert mode

 - G - move to the end of the file
	* G numG - move to num line
 - :num - move to num line
 - gg - move to the begining of the file
 - e - move to the end of the wrod
 - b - move to the begining of the word
 - $ - move to the end of the line
 - ^ - move to the begining of the line

 - dd - delete line
	* 5dd - delete 5 lines

 - shift + a - insert text at the end of file

 - yy - copy current line
 - p - paste storage buffer after current line
 - shift + p - paste storage buffer at current line

To copy multiple lines:
 - shift + v - enter visual mode (mark lines)
 - v - enter visual mode mark char by char
Then:
 - with cursor or arrows to mark lines
 - y - copy lines
 - d - delete lines
 - p - paste lines

 - u - undo last operation
 - Ctrl + r - redu last undof

 - v - mark text normally 
 - shift + V - mark line
 - d - delete mark text
 - y - copy
 - p - pase

 - /search_text - serach document for search_text
 - ?reverse_search_text
   * n - move to the next instance of the result
   * shift + N - move to the previous instance of the result

more about vim:
https://www.maketecheasier.com/vim-keyboard-shortcuts-cheatsheet/

config vim: ~/.vimrc (in this file you can specify your configuration for vim editor)