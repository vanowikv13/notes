# Linux system programming.

## System call

![system call example](images/system_call.jpg)

With `syscall` in C:

```C
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>

int main(int argc, char *argv[])
{
    syscall(SYS_write, 1, "Hello\n", 6);
    return 0;
}
```

With assembly and int 0x80:

```assembly
section	.text
    global _start   ;must be declared for linker (ld)
	
_start:	            ;tells linker entry point
    mov	edx, len    ;message length
    mov	ecx, msg    ;message to write (holds the string pointer)
    mov	ebx, 1      ;file descriptor (stdout)
    mov	eax, 4      ;system call number (sys_write call)
    int	0x80        ;call kernel (interrupt to make system calls to kernel)

    mov	eax, 1      ;system call number (sys_exit)
    mov   ebx, 0    ;0 is our exit status of program
    int	0x80        ;call kernel

section	.data
    msg db 'Hello', 0x0a ;string to be printed "Hello\n"
    len equ $ - msg     ;length of the string
```

### Library functions vs system functions

```bash
# System functions.
man 2 read

# Library functions.
man 3 fread
```

## Buffering

The goal of buffering in the standard library is to minimize number of read/write operation calls to the kernel.
The library perform buffering automatically and there are three types of buffering:

1. Fully buffered - input or output data are buffered until a buffer is full, then it's written kernel.
2. Line buffered - The standard library performs system call when a newline character is received or when buffer is full.
3. Unbuffered - no data are buffered.

Function `fflush` could be used to write all content of buffer to kernel.

### ISO C standard about buffering

1. Standard input and standard output are fully buffered, if and only if they do not refer to an interactive device.
2. Standard error is never fully buffered.

### Most implementations default to the following types of buffering

1. Standard error is always unbuffered.
2. All other streams are line buffered if they refer to a terminal device; otherwise, they are fully buffered.


Reference link: https://notes.shichao.io/apue/ch5/#buffering