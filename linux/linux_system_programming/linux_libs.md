# Creating and using libraries in linux OS

## Static library

It's a library, which is just an archive (.a) with already compiled object files (.o).

### Creating static library

Let say we have two files.

1. math.h

    ```cpp
    int add(int a, int b);
    int sub(int a, int b); 
    ```

2. math.c

    ```cpp
    int add(int a, int b) { return a + b; }
    int sub(int a, int b) { return a - b; }
    ```

Then library should be compiled to an object file with:

```bash
$ gcc -c math.c -o math.o
```

Remember that the static library name should start with "lib*" and end with "*.a"

```bash
$ ar cr libmath.a math.o
```

Remember that all static library name should start with "lib*" and end with "*.a"

### Using static library

Example code that uses our static library (main.c):

```cpp
#include <stdio.h>
#include "math.h"

int main()
{
    printf("%d\n", add(1, 3));
}
```

Then compile it with the flag `-l[libname]`. After -l there should be only a library name after "lib" and before ".a". There is also `-L[path]`, which will inform the linker that it should search `[path]` for mentioned static library.

```bash
$ gcc main.c -o main -L. -lmath
```

## Shared library

Shared library group object files into one file, with format:

```
lib[library_name].so.[ver1].[ver2].[ver3]

Example:
libmath.so.0.0.0

Given version vars are optional, so it could also be named:
libmath.so
```

### Creating shared library

All object files that will be part of the shared library should be compiled with -fPIC flags.

```bash
$ gcc -c math.c -o math.o -fPIC 
```

Then all object files should be compiled with `-shared`, `-fPIC` and `-nostartfiles` flags (the last one is not required).

```bash
$ gcc -shared -nostartfiles -fPIC math.o -o libmath.so
```

### Using shared library

Flags `-Wl,-rpath=[path_to_lib]` will make the library available at runtime in case of dynamic linking.

```bash
$ gcc main.c -o main -L. -lmath -Wl,-rpath,.
```

#### Using shared library statically

Linking a shared library works almost the same as linking a static library the only difference is that now flag `-static` should be added.

```bash
$ gcc -static -L. -lmath main.c -o main
```

### Init/deinit of the shared library

Each instance of a program that is using a shared library, which is probably shared between many programs in OS has its own set of data/globals. When the shared library is loaded it could execute two functions, which could set some data and make some calls etc. Program loading library search for functions that start with `__attribute__ ((constructor))` and `__attribute__ ((destructor))`.

Library initialization:

```cpp
void __attribute__ ((constructor)) init(void)
{
    //...
}
```

Library de-initialization:

```cpp
void __attribute__ ((destructor)) deinit(void)
{
    //...
}
```


Resources:
* https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html