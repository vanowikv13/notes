#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>

int main(int argc, char *argv[])
{
    syscall(SYS_write, 1, "Hello\n", 6);
    return 0;
}
