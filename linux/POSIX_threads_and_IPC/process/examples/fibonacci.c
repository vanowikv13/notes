// for example compile and run with: gcc main.c -o main && ./main 6
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include <stdbool.h>

bool isNumber(const char*number)
{
    for (const char* it = number; *it != '\0'; it++)
    {
        int digit = (int)(*it);
        //0 = 48 dec in ascii, 9 = 57 dec in ascii
        if(digit < 48 || digit > 57)
            return false;
    }
    return true;
}

void updateArg(int value, char *arg)
{
    sprintf(arg, "%d", value);
}

int main(int argc, char *argv[], char **env)
{
    if(!isNumber(argv[1]))
    {
        fprintf(stderr, "Given number is not build from digit.\n");
        return 2;
    }

    int number = atoi(argv[1]);
    if(number < 1 || number > 13) {
        fprintf(stderr, "Given number is not in range <1;13>.\n");
        return 3;
    }

    if(number == 1 || number == 2)
        return 1;

    char value[100];
    char* newArgs[3] = {"./main", value, NULL};
    sprintf(value, "%d", number-4);

    for (int i = 1; i <= 2; ++i) {
        int proc1 = fork();
        if(proc1 == 0) {
            char arg[100] = {0};
            updateArg(number-i, arg);
            char* args[3] = {argv[0], arg, NULL};
            execvp(argv[0], args);
            fprintf(stderr, "exec failed: %s\n", strerror(errno));
            return 1;
        } else if (proc1 < 0) {
            printf("fork() failed - %s\n", strerror(errno));
            return 1;
        }
    }

    int sum = 0;
    for(int i = 1; i <= 2; i++) {
        int status, child = wait(&status);
        //printf("Parent pid: %d: child #%d exited: PID=%d, arg=%d retcode=%d\n", getpid(), i, child, number-i, WEXITSTATUS(status));
        printf("Parent: pid=%d\tChild: nr. %d, pid=%d\tinput=%d\treturn status=%d\n", getpid(), i, child, number-i, WEXITSTATUS(status));
        sum += WEXITSTATUS(status);
    }
    printf("Parent pid=%d\tfibonacci number=%d\n", getpid(), sum);
    return sum;
}