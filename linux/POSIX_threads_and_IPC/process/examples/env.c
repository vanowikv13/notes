#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define SUCCESS 0

int main(int argc, char *argv[], char *env[]) {
    // Print all env
    const char **envp = (const char**)env;
    while(*envp)
    {
        printf("%s\n", *envp++);
    }

    const char * const envVarName = "MY_NAME";
    
    // Reading env
    const char *envVarValue = getenv(envVarName); // more: man getenv, putenv
    if(NULL != envVarValue)
    {
        printf("%s=%s\n", envVarName, envVarValue);
    }
    else
    {
        printf("given env var do not exists. Creating it...\n");
    }


    // Change/set env value
    int status = setenv(envVarName, "Mario", 1); // more: man setenv
    if(SUCCESS == status)
    {
        envVarValue = getenv(envVarName);
        printf("%s=%s\n", envVarName, envVarValue);

    } else
    {
        printf("Changing env var %s failed. Errno: %d\n", envVarName, errno);
    }

    // Unset env, more in man unsetenv
    status = unsetenv(envVarName);
    if(SUCCESS == status)
    {
        // Checking if env is unset for sure
        envVarValue = getenv(envVarName);
        if(NULL == envVarValue)
        {
            printf("Env var is for sure unset.\n");
        }
    } else
    {
        printf("Changing env var %s failed. Errno: %d\n", envVarName, errno);
    }
}