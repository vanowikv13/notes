#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>

#define SUCCESS 0

void* startRoutine(void * args)
{
    // create copy of a process with only this thread running (main in forked process is not run)
    int forkReturnVal = fork();
    
    if(forkReturnVal == 0)
    {
        printf("We're in child. Child pid=%d, ppid=%d\n", getpid(),getppid());
    }
    else if(forkReturnVal > 0)
    {
        printf("We're in parent. Parent pid=%d, child pid=%d\n", getpid(), forkReturnVal);
    }
    else
    {
        printf("fork() failed - %s\n", strerror(errno));
    }

    puts("Thread ends.\n");

    // return to main from which process we call after fork
    *((int*)args) =  forkReturnVal;
    return args;
}

int main(void)
{
    pthread_t tid;
    int pid = 0;
    int status = pthread_create(&tid, NULL, startRoutine, &pid);

    if(SUCCESS == status)
    {
        // wait for created thread to finish.
        (void)pthread_join(tid, NULL);
        printf("Main thread, fork return pid is: %d\n", pid);

        // if everything goes normal PID should be different than 0
        if(0 != pid)
        {
            int processStatus = 0;

            // wait till forked process will end.
            int status = wait(&processStatus);

            printf("Process return %d\n", processStatus);
        }
    } else
    {
        printf("Error on creating thread. Errno: %d\n", errno);
    }
    return 0;
}