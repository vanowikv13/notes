cmake_minimum_required(VERSION 3.10)

# set the project name
project(MessageHandler)

# add the executable
add_executable(MessageHandler messageHandler.c)


add_executable(print print.c)