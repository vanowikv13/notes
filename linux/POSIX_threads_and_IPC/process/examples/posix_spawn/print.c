#include <stdio.h>

#include "errorTypes.h"

int main(int argc, char* argv[])
{
    if(argc < 2)
    {
        fputs("No message provided.\n", stderr);
        return INVALID_MESSAGE;
    }

    printf("%s\n", argv[1]);
    return SUCCESS;
}
