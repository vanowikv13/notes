#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>
#include <spawn.h>

#include "errorTypes.h"

extern char **environ;

int main(void)
{
    char messageBuffer[100];
    char *argv[] = {"print", messageBuffer, NULL};
    
    while(NULL != fgets(messageBuffer, 100, stdin))
    {
        pid_t pid;
        int status = posix_spawn(&pid, "print", NULL, NULL, argv, environ);
        
        if (SUCCESS == status)
        {
            int status = 0;
            int child = wait(&status);

            printf("In parent with pid=%d: child pid=%d, return status=%d\n", getpid(), child, status);
        } else
        {
            fprintf(stderr, "Error on posix_spawn. Err message: %s\n", strerror(status));
        }
    }
    return SUCCESS;
}
