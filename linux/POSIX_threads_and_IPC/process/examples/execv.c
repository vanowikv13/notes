#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define FAILURE -1

int main(void)
{
    char *argv[] = {"ls", "-la", NULL};
    if(FAILURE  == execv("/bin/ls", argv))
    {
        printf("Error on calling execv. err message: %s\n", strerror(errno));
    }
}