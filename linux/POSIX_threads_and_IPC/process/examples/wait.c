#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{
    int pid = fork();
    if(pid == 0)
    {
        printf("Child %d of parent %d\n",getpid(),getppid());
        return 2;
    } else if(pid > 0)
    {
        printf("Parent %d of child %d\n",getpid(), pid);
        sleep(2);
        int status = 0;
        int child = wait(&status);
        printf("In parent with pid=%d: child pid=%d, return status=%d\n",
                getpid(), child, WEXITSTATUS(status));
    } else
    {
        printf("fork() failed - %s\n", strerror(errno));
        return 1;
    }
    return 0;
}