#include <stdio.h> // printf
#include <errno.h> // errno
#include <unistd.h> // execv
#include <string.h> // strerror

#define FAILURE -1

int main(void)
{
    char *argv[] = {"ls", "-la"};
    if(FAILURE  == execv("/bin/ls", argv))
    {
        printf("Error on calling execv. err message: %s\n",
                strerror(errno));
    } else
    {
        // do not need because if execv will goes
        // with success this code would never be executed
    }
}