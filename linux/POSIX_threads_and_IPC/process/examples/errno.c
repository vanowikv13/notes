#include <errno.h>  //errno
#include <string.h> //strerror
#include <stdio.h>

int main()
{
    // you can clear errno at any time, but usually you do not have to
    // errno variable is defined in errno.h
    errno = 0;

    // if not running with root this should return NULL and errno should be set
    FILE * file = fopen("/proc/1/syscall", "r");
    if(NULL == file)
    {
        if(errno != 0) {
            // error were made or some problem has been made
            char* message = strerror(errno); //char *strerror(int errno)
            
            // print error code description
            printf("Error code is: %s\n", message);

            // calling again now it should goes with success
            file = fopen("./errno.c", "r");
            
            // in case of fopen failure
            if(NULL != file)
            {
                // In this case and most errno would no be changed after successfull call.
                printf("After successfully call errno now is = %d\n", errno);
            } else
            {
                message = strerror(errno); //char *strerror(int errno)
                printf("Again errno failure: %s\n", message);
            }
        }
    } else
    {
        printf("Everyting goes with success.");
    }
}