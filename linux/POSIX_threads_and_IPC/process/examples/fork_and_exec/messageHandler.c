#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>

#include "errorTypes.h"

int main(void)
{
    char messageBuffer[100];
    while(NULL != fgets(messageBuffer, 100, stdin))
    {
        pid_t forkPid = fork();
        if(0 == forkPid)
        {
            char *argv[] = {"print", messageBuffer, NULL};

            if(EXECV_FAILURE  == execv("print", argv))
            {
                fprintf(stderr, "Error on calling execv. err message: %s\n", strerror(errno));
            } else
            {
                // intentionally empty, this path should never happen
            }
        }
        else if(forkPid > 0) // wait for child process
        {
            int status = 0;
            int child = wait(&status);

            printf("In parent with pid=%d: child pid=%d, return status=%d\n", getpid(), child, status);
        }
        else
        {
            fprintf(stderr, "fork() failed: %s\n", strerror(errno));
            return FORK_FAILURE;
        }
    }
    return SUCCESS;
}
