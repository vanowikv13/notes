#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

int main(void)
{
    pid_t forkPid = fork();
    if(forkPid == 0)
    {
        printf("Child pid=%d, ppid=%d\n", getpid(),getppid());
    }
    else if(forkPid > 0)
    {
        printf("Parent pid=%d, child pid=%d\n", getpid(), forkPid);
    }
    else
    {
        printf("fork() failed: %s\n", strerror(errno));
        return 1;
    }
    return 0;
}
