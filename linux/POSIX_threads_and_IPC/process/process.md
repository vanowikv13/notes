# Process in linux

Let's assume there is some executable file either of type ELF (more about it in **man elf**) under Linux or PE (*.exe) under windows. The kernel used those files to create a process in the system.

So we can think of the process as some kind of container which has:

- List of threads, which are units of code execution.
- Memory map of the process (code section, globals, others.)
- List of all opened devices (np. files, signals)
- Other information used by scheduler (like priority of process)
- In Linux its own process directory in /proc/PID/ (where PID is process id)
- It's own virtual memory space, which operating system kernel track how to map it to physical memory.
- ID of a user and group (real and effective id) and environmental variables.

The process does not execute the code only the thread does. Each process after creation gets its main thread. After that from the first thread, could create other threads and each of them will have its own context.

Processes usually are created with `fork`/`vfork` and the process usually ends its life with an `exit` call.
After the process is exited it enters the `zombie` state, which still has an entry in the process table, but it's not executed anymore.
A zombie process is deleted by the kernel when his data are no longer needed.

## Process Attributes

- PID - process identifier - unique value represent process.
- PPID - parental process identifier - PID of the parent process that brings the child process to life.
- environment variables for this process. (It's is created by copy parent process env)
- owner - (UID) user which starts the process (or in the name it was started).
- priority - value represents the place of the process in the hierarchy. Deciding how the system treats the process. Priority values are from -20 (highest) to 19 (lowest), there is also priority rt (real-time), which is above anybody else.

Fact that each process is connected to its parent let us represent this in a tree way. Where root process is of name **init**. Init is the first process created in the system and its PID is 1, and PPID is 0, which means it has no parent and its ancestor of all processes. Init is started by the system service that starts the system and it's called `sysvinit`, also called in some systems `systemd`. System processes are started by root, so normal users cannot interact with them.

## Tracking process system calls and library calls

- System call and system signals: man strace

- Library calls and signals: man ltrace

## Linux signals

See more in man 7 signal

## More about /proc directory

In /proc/[pid] are information about specific process. Here are listed some of them.

- /proc/[pid]/comm - is name of the process
- /proc/[pid]/status - are meta information about process and it's status (status, pid, ppid ...)
- /proc/[pid]/cwd - symbolic link to where program is.
- /proc/[pid]/stack - representation of a stack in a file.
- /proc/[pid]/exe - symbolic link to executable file from which process was created.
- /proc/[pid]/environ - text file with environmental variables values during creation of a process.
- /proc/[pid]/fd - directory with list of symbolic links of all opened file descriptors
- /proc/[pid]/maps - file with map representation of memory.
- /proc/[pid]/mem - file that being interface to program memory
- /proc/[pid]/task - directory with information about all threads

pid - is your current process identifier

## Processes tools

### ps - print process information

- ps -aux - print detailed information about all the processes.

- ps -e - show information about all
- ps -p x - show information about the process with PID x
- ps -l - long information
- ps -u list - prints process by users
  - ps -u root, user

See specific information about process with params:

- Scheme: ps -eo "params"
- Params (values in code and normal could be used)
![params1](img/params1.png)

![params2](img/params2.png)

- Examples:
  - ps -eo "pid,ppid,user,%mem,pcpu" | head -10
  - ps -eo "%p %P %y %x %c %C"

### pstree - print graphical tree of process

scheme: pstree [operands...] [pid | user]

operands:
    *-p - show pid
    * -u - show user

Examples:

- pstree x -p -s - show path for process in tree with x pid

Example:

- pstree x - show pstree with x as a root
- pstree user - show pstree for the user process

### top - print top process with 5 second refresh

scheme: top [operands...]
operands:

- -d n - refresh each 'n' seconds
- -p pid - show processes with PID
- -c - show full command

While in top, you can also make a commands:

- <space> - refresh
- k - kill process with PID
- n - the amount of presented process
- l - turn on/off information about the load
- m - turn on/off information about memory
- t - turn on/off information about the process
- M - sort by memory
- N - sort by PID
- P - sort by CPU usage

### time - monitoring time of the process

Time print time in 3 options:

- real - full time of a process
- user - time of a process is in the user space
- sys - time of a process is in the system space

Example:

- time ps -aux

### kill - sending signals to the system

Signals:

- SIGTERM (code: 15) - request the process to terminate. The process can have a handler for this signal, which could also ignore termination.
- SIGINT (code: 2) - a process with this signal is informed that should finish its work but can also ignore it. (Ctrl+c combination)
- SIGKILL (code: 9) - process, which gets this signal immediately finishes its work.

More signal could found in manual signal(7)

Scheme: kill [-signal] [operands...] pid...
If signal is not specified by default it's SIGTERM

EXAMPLES:

- kill -9 1000 (same as: kill -SIGKILL 1000)

### killall - kill process by name

scheme: killall [operands...] program_name...

Operands:

- -I - ignore cases
- -r regex - use regex instead of name
- -u user - send a signal only to the user process.

Example:

- killall -s SIGKILL chrome - kill all chrome process
- killall -s SIGTERM chrome -u user - kill all chrome process made by user user and SIGTERM them.

## Process priority - nice, renice

Each process can have priority from -19 (higher) to 20 (lower). Process by default get priority after parent. Process if they do not have root permission cannot set higher priority for task only lower (means cannot set priority below their priority e.g. if they have 10 they cannot set 9)

### Nice - running command with priority

scheme: nice -n priority command

### Renice - change priority of the running process

scheme: renice priority pid

## Levels of process

Foreground process - It's the actual process and to start another we need to wait until the previous process will be deleted from the system.

Background process - the process is running in the background, it's another way to run many processes in one terminal window.

If we use Ctrl+z on the process this process will be stopped and sent to the background. If we use **bg** this background process will start running in the background. The process can also be moved to the foreground with **fg**. To see the list of background processes run **jobs**.

If you have more background process use. To select some use: `fg %x` or `bg %x` (for background process with id x (it's id, not PID)).

## Completion code - exit status

Each instruction and program in Linux after it's finished its job return a completion code. If it's 0 it means that our operation success, otherwise there might be some errors.

```bash
echo $? # Print last exit status.
```

## Process in linux with C language

### Errors

To find information about errors in system calls use an errno.h library. Errno is set by system calls or library calls to signalize errors on calls. It's thread-local, which means that setting it for one thread does not affect its value in any other thread. POSIX threads library (pthread) does not set error in errno instead, they return the error number as a function result, but this return value has the same meaning as a list of symbolic error names defined in errno.
A full list of errno errors could be found in `man 3 errno`

```cpp
#include <errno.h>  //errno
#include <string.h> //strerror
#include <stdio.h>

int main()
{
    // will return NULL if not run as root
    FILE * file = fopen("/proc/1/syscall", "r");
    if(NULL == file)
    {
        if(errno != 0) {
            // error were made or some problem has been made
            char* message = strerror(errno); //char *strerror(int errno)
            
            // print error code description
            printf("Error code is: %s\n", message);

            // calling again now it should goes with success
            file = fopen("./errno.c", "r");

            // if previous call goes with success errno would be changed
            printf("After successfully call errno now is = %d\n", errno);
        }
    }
}
```

More detailed example is in example/errno.c

### Env variables

In C language there are also possible to get env variables as a main argument. (only when running on operating system)

Function to work with env: (more info could be found in man)

- char *getenv(const char*name); - return pointer to env value, return NULL if env do not exists
- int setenv(const char *name, const char*value, int overwrite); - set new env
- int unsetenv(const char *name); - unset env
- int putenv(char *string) - setting new env (string must be type: "NAME=VALUE")

Example:

```cpp
#define SUCCESS 0

int main(int argc, char *argv[], char *env[]) {
    // Print all env
    const char **envp = (const char**)env;
    while(*envp)
        printf("%s\n", *envp++);

    const char * const envVarName = "MY_NAME";
    
    // Reading env
    const char *envVarValue = getenv(envVarName); // more: man getenv, putenv
    if(NULL != envVarValue)
    {
        printf("%s=%s\n", envVarName, envVarValue);
    }

    // Change/set env value
    int status = setenv(envVarName, "Mario", 1); // more: man setenv
    if(SUCCESS == status)
    {
        envVarValue = getenv(envVarName);

    } else
    {
        printf("Changing env var %s failed. Errno: %d\n", envVarName, errno);
    }

    // Unset env, more in man unsetenv
    status = unsetenv(envVarName);
    if(SUCCESS == status)
    {
        printf("Env is unset.\n");
    } else
    {
        printf("Changing env var %s failed. Errno: %d\n", envVarName, errno);
    }
}
```

full code is in examples/env.c

### Effective UID/GID and Real UID/GID

Effective UID/GID - is UID/GID that let some process running on behalf of the other user/group (owner of the file).

```cpp
#include <unistd.h>
#include <sys/types.h>

// Get user effective id.
uid_t geteuid(void);

// Get group effective id.
uid_t getegid(void);
```

Real UID/GID - is the UID/GID of the user that creates a process.

```cpp
#include <unistd.h>
#include <sys/types.h>

// Get user real id.
uid_t getuid(void);

// Get group real id.
uid_t getgid(void);
```

### System command

Scheme: int system(const char *command);
If we call a system(NULL) and return is 0 it means shell is available. If it's smaller than 0 it's not available.

```cpp
#include <stdlib.h>
#include <stdio.h>

int main() {
    int ret = system(NULL); // 0
    if(ret) {
        //8 old bits of status is return code from child process, 8 younger bits is termination reason code.
        ret = system("pwd");
        printf("%d\n", ret); // 0
    }
}
```

### Get pid, ppid and pgrp of actual process

- pid_t getpid(void); - return Process ID (PID)

- pid_t getppid(void); - return process Parent Process ID (PPID)
- int getpgrp(void); - return process Group ID (GID)
  - more man 2 getpgrp

```cpp
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main() {
    printf("%d\n", getpid());
    printf("%d\n", getppid());
    printf("%d\n", getpgrp());
    return 0;
}
```

### Create a new process from the current process

Method:

- pid_t fork(void);

The operation made by fork:

1. Stop process which calls fork()
2. Clone process, which is a perfect copy of the process that calls fork()
   1. Clone the threads that call fork and make it the main thread if it's not already.
   2. Copy parent descriptors
3. Start the child process and parent process.
   1. If the process has multiple threads, then the thread which calls fork is only run. (example/fork_from_thread.c)
4. At this moment both processes work in parallels and are independent.

To remember:

1. Fork always clones all the descriptors in the called process, both processes will try to use external resources at the same time this can leads to corrupting those resources (e.g writing duplicated records to a file).
2. Only a thread, which calls a fork is run after a fork, so this means in the child process the thread which calls fork in the parent process is now the main thread in the child process.
3. Child has a copy of parent data and each other cannot edit others variables.

Return value:

- -1 - error process was not created
- 0 - child process was created and the process which got this value is the child
- '>0' - child process was created and the process which got this value is a parent and the return value is PID of the child process.

```cpp
pid_t forkPid = fork();
if(forkPid == 0) // if true we're in child
{
    printf("Child pid=%d, ppid=%d\n", getpid(),getppid());
}
else if(forkPid > 0) // if true we're in parent
{
    printf("Parent pid=%d, child pid=%d\n", getpid(), forkPid);
}
else // -1
{
    printf("fork() failed: %s\n", strerror(errno));
    return 1;
}
```

Full example is in examples/fork.c
Example with fork called from another thread: example/fork_from_thread.c

### Wait until child process is finished

Wait:

- pid_t wait(int *wstatus); - wait until one of the child processes is finished. (more in man 2 wait)
- pid_t waitpid(pid_t pid, int *wstatus, int options); - wait until the exact child process is finished with the given PID. (more in man 2 waitpid)

Params:

 - wstatus - NULL or pointer to where the return value of the child will be placed.
 - options - describe waiting for behaviour.

Return value:

- pid_t of the finished child process if success
- -1 if failure

```cpp
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{
    int pid = fork();
    if(pid == 0)
    {
        printf("Child %d of parent %d\n",getpid(),getppid());
        return 2;
    } else if(pid > 0)
    {
        printf("Parent %d of child %d\n",getpid(), pid);
        sleep(2);
        int status = 0;
        int child = wait(&status);
        //8 old bits of status is return code from child process, 8 younger bits is termination reason code.
        printf("In parent with pid=%d: child pid=%d, return status=%d\n", getpid(), child, status);
    } else
    {
        printf("fork() failed - %s\n", strerror(errno));
        return 1;
    }
    return 0;
}
```

### Sleep(n) - sleep for n seconds

```cpp
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "usage: %s seconds\n", argv[0]);
        return -1;
    }

    char *endptr;
    int secs = strtol(argv[1], &endptr, 10);
    if(secs <= 0 || *endptr != '\0')
    {
        fprintf(stderr, "argument must be natural integer\n");
        return 1;
    }

    for(int i = 0; i < secs; i++)
    {
        printf("pid=%d, seconds=%d\n", getpid(), secs-i);
        sleep(1);
    }
    printf("finish\n");
    return 0;
}
```

### Exec family functions

Exec family function execute a file and replace the process image with a new process image. (more in man 3 exec and man 2 execve).

```cpp
#define FAILURE -1

int main(void)
{
    char *argv[] = {"ls", "-la"};
    if(FAILURE  == execv("/bin/ls", argv))
    {
        printf("Error on calling execv. err message: %s\n", strerror(errno));
    } else
    {
        // Do not need it because if execv will goes with success this code would never be executed.
    }
}
```

### Spawn new process and change executed code with (fork + exec)

In case we want to handle requests in another process usually it happens in the Linux subsystem by calling fork to spawn a child process and then exec to replace the process with the new image.

Look examples/fork_and_exec and examples/fibonacci.c

### posix_spawn alternative to (fork + exec)

posix_spawn combines fork and exec into one function and it might be also useful in case the fork function is not available like on some embedded platform.

Look examples/posix_spawn
