#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SUCCESS 0

#define PRINT_THREAD_ERROR(message, status) fprintf(stderr, "%s, err code=%d, err message%s\n", message, status, strerror(status))
#define THREAD_ERROR_EXIT(message, status) ({\
        if(SUCCESS != status)\
        {\
            PRINT_THREAD_ERROR(message, status);\
            exit(status);\
        }})

// mutex 1 and 2
pthread_mutex_t mutex[2] = {PTHREAD_MUTEX_INITIALIZER, PTHREAD_MUTEX_INITIALIZER};

void* thread1(void*args)
{
    for(int i = 0; i < 10; i++)
    {
        printf("Thread 1: %d\n", i);
        pthread_mutex_lock(&mutex[0]);
        usleep(200);

        pthread_mutex_lock(&mutex[1]);
        usleep(200);

        pthread_mutex_unlock(&mutex[1]);
        pthread_mutex_unlock(&mutex[0]);
    }
}

void thread2(void)
{
    for(int i = 0; i < 10; i++)
    {
        printf("Thread 2: %d\n", i);
        pthread_mutex_lock(&mutex[1]);
        usleep(200);

        pthread_mutex_lock(&mutex[0]);

        usleep(200);

        pthread_mutex_unlock(&mutex[1]);
        pthread_mutex_unlock(&mutex[0]);
    }
}

int main(void)
{
    int status;
    pthread_t t1;

    // creating thread 1
    status = pthread_create(&t1, NULL, &thread1, NULL);
    THREAD_ERROR_EXIT("error on creating thread1", status);
    
    // calling thread 2 from main thread.
    thread2();

    // join thread 1
    status = pthread_join(t1, NULL);
    THREAD_ERROR_EXIT("error on thread join", status);
    
    // after work done destroy mutex
    status = pthread_mutex_destroy(&mutex[0]);
    THREAD_ERROR_EXIT("error on mutex destroy", status);

    status = pthread_mutex_destroy(&mutex[1]);
    THREAD_ERROR_EXIT("error on mutex destroy", status);
    return SUCCESS;
}