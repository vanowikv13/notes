#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

extern void critical_section_enter(char*) asm("_critical_section_enter");

extern void critical_section_exit(char*) asm("_critical_section_exit");

char lock = 0;

/** look to race_condition.c in example without mutex **/

#define SUCCESS 0

void printThreadErrors(const char * const strErrMessage, int status);

int count_inc = 0;

void *start_routine(void *data)
{
    for(int i = 0; i < 1000; i++)
    {
        // int status = pthread_mutex_lock(&count_inc_mutex);
        // critical section enter
        critical_section_enter(&lock);

        usleep(10); // just add trigger more race conditions
        count_inc = count_inc + 1;


        critical_section_exit(&lock);
        // exit critical section
    }
    return NULL;
}

int main()
{
    pthread_t threads[3];
    for(int i = 0; i < 3; i++)
    {
        pthread_create(threads + i, NULL, start_routine, NULL);
    }
    
    for(int i = 0; i < 3; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Should be count_inc = 3000, and it is = %d", count_inc);
    return 0;
}

void printThreadErrors(const char * const strErrMessage, int status)
{
    printf("%s, err code=%d, err message%s\n", strErrMessage, status, strerror(status));
}