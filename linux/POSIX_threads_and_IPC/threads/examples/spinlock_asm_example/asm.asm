section .text

global _critical_section_enter
global _critical_section_exit

RELEASE equ 0
ACQUIRE equ 1

; Function takes pointer to one byte lock parameter
_critical_section_enter:
    ;prepare stack
    push ebp
    mov ebp, esp
    
    mov edx, [esp+8] ; mov address of lock to edx register

; loop until lock is not free if it's free then lock it with this function
loop_under_lock_is_free:
    mov eax, ACQUIRE ; to use it as exchange register with memory location for xchg

    xchg BYTE [edx], al ; exchange in register al with what is in edx pointer value (lock)

    test al, al ; make and on all to verify if al was after exchange 0 or 1.
    jne loop_under_lock_is_free ; if lock is not free then try again and jump to loop_under_lock_is_free

    ;comeback from where function was called and put stack in order
    pop ebp
    ret

_critical_section_exit:
    ;prepare stack
    push ebp
    mov ebp, esp

    mov edx, [esp+8] ; mov address of lock to edx register
    mov eax, RELEASE ; set eax to value indicating lock is free
    mov [edx], eax ; set lock to RELASE (0)

    ;comeback from where function was called and put stack in order
    pop ebp
    ret