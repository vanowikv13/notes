#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define THREADS_NUM 2

void start_routine(void)
{
    pthread_t own_tid = pthread_self();
    printf("Thread with tid=%ld\n", own_tid);
}

int main()
{
    pthread_t tid[THREADS_NUM];
    int tab[THREADS_NUM];
    for(int i = 0; i < THREADS_NUM; i++)
    {
        tab[i] = i;
        pthread_create(tid + i, NULL, start_routine, (void*)(tab+i));
    }
    
    for(int i = 0; i < THREADS_NUM; i++)
    {
        pthread_join(tid[i], NULL);
        printf("Thread with tid=%ld is finished.\n", tid[i]);
    }
}