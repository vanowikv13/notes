/** look to race_condition.c in example without mutex **/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SUCCESS 0


void printThreadErrors(const char * const strErrMessage, int status);


int count_inc = 0;

// mutex object
pthread_mutex_t count_inc_mutex;

void *writter(void *data)
{
    for(int i = 0; i < 100; i++)
    {
        int status = pthread_mutex_lock(&count_inc_mutex);
        if(SUCCESS == status)
        {
            // critical section enter

            usleep(10); // just add trigger more race conditions
            count_inc = count_inc + 1;
            
            // critical section exit
            
            status = pthread_mutex_unlock(&count_inc_mutex);
            if(SUCCESS != status)
            {
                printThreadErrors("error on mutex unlcok", status);
            }
        } else
        {
            printThreadErrors("error on locking a mutex", status);
        }
    }
    return NULL;
}

int main()
{
    // initialize mutex
    int status = pthread_mutex_init(&count_inc_mutex, NULL);
    if(SUCCESS != status)
    {
        printThreadErrors("error on mutex init", status);
        return status;
    }

    pthread_t threads[3];
    for(int i = 0; i < 3; i++)
    {
        pthread_create(threads + i, NULL, writter, NULL);
    }
    
    for(int i = 0; i < 3; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Should be count_inc = 300, and it is = %d", count_inc);
    
    // after work done destroy mutex
    status = pthread_mutex_destroy(&count_inc_mutex);
    if(SUCCESS != status)
    {
        printThreadErrors("error on mutex destroy", status);
        return status;
    }
    return 0;
}

void printThreadErrors(const char * const strErrMessage, int status)
{
    printf("%s, err code=%d, err message%s\n", strErrMessage, status, strerror(status));
}