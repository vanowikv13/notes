#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

void* startRoutine(void * args)
{
    *(int*)args = 0;
    return args;
}

int main(void)
{
    //pthread_t - thread identifier TID
    pthread_t tid;
    int x = 12;

    //creating new thread
    int status = pthread_create(&tid, NULL,startRoutine, &x);
    if(status != 0)
    {
        // errno error messages used to print pthread error message
        printf("ERROR: %s\n", strerror(status));
        return status;
    }

    sleep(2);
    printf("%d\n", x);
    return 0;
}