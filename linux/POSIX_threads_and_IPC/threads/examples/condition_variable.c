#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SUCCESS 0
#define PRODUCER_BUFFER_LEN 100

#define PRINT_THREAD_ERROR(message, status) fprintf(stderr, "%s, err code=%d, err message%s\n", message, status, strerror(status))
#define THREAD_ERROR_EXIT(message, status) ({\
        if(SUCCESS != status)\
        {\
            PRINT_THREAD_ERROR(message, status);\
            exit(status);\
        }})

// condition variable
pthread_cond_t condition_var = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* thread_routine(void*args)
{
    // mutex should be always locked before calling wait
    int status = pthread_mutex_lock(&mutex);
    puts("locked");
    if(SUCCESS == status)
    {
        int status = pthread_cond_wait(&condition_var, &mutex);
        puts("After condition is waiting\n");
        if(SUCCESS == status)
        {
            // we got notified and entered critical section
            puts("Critical section enter\n");
            // exit critical section
            pthread_mutex_unlock(&mutex);
            THREAD_ERROR_EXIT("error on mutex unlock", status);
        } else
        {
            PRINT_THREAD_ERROR("fail on condition variable wait", status);
        }
    } else
    {
        PRINT_THREAD_ERROR("fail on mutex lock", status);
    }
}

int main(void)
{
    int status;
    pthread_t consumer_id_t;

    // creating consumer thread
    status = pthread_create(&consumer_id_t, NULL, &thread_routine, NULL);
    THREAD_ERROR_EXIT("error on creating consumer thread", status);
    
    sleep(3);
    
    status = pthread_mutex_lock(&mutex);
    THREAD_ERROR_EXIT("error on locking mutex", status);
    
    status = pthread_cond_signal(&condition_var);
    THREAD_ERROR_EXIT("error on notifing with signal", status);

    status = pthread_mutex_unlock(&mutex);
    THREAD_ERROR_EXIT("error on mutex unlock", status);

    // join consumer thread
    status = pthread_join(consumer_id_t, NULL);
    THREAD_ERROR_EXIT("error on thread join", status);
    
    // after work done destroy mutex
    status = pthread_mutex_destroy(&mutex);
    THREAD_ERROR_EXIT("error on mutex destroy", status);

    // after work done destroy condition variable
    status = pthread_cond_destroy(&condition_var);
    THREAD_ERROR_EXIT("error on condition var destroy", status);
    return SUCCESS;
}