/** look to race_condition.c in example without mutex **/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SUCCESS 0


void printThreadErrors(const char * const strErrMessage, int status);


double g_stock_price = 0.0;

char finish = 1;

// rwlock object
pthread_rwlock_t rwlock;

void *stockPriceUpdater(void *data)
{
    for(int i = 0; i < 100; i++)
    {
        int status = pthread_rwlock_wrlock(&rwlock);
        if(SUCCESS == status)
        {
            // critical section enter
            g_stock_price = g_stock_price + 1.0;
            
            // critical section exit
            status = pthread_rwlock_unlock(&rwlock);
            if(SUCCESS != status)
            {
                printThreadErrors("error on rwlock unlock", status);
            }
        } else
        {
            printThreadErrors("error on locking a rwlock", status);
        }

        // 100 ms
        usleep(100000);
    }

    finish = 0;
    return NULL;
}

double getCurrentStockPrice()
{
    double stock_price;
    int status = pthread_rwlock_rdlock(&rwlock);
    if(SUCCESS == status)
    {
        stock_price = g_stock_price;
        status = pthread_rwlock_unlock(&rwlock);
        if(SUCCESS != status)
        {
            printThreadErrors("error on rwlock unlock", status);
        }
    } else
    {
        printThreadErrors("error on locking a mutex", status);
    }

    return stock_price;
}

void* stockPriceSender(void*data)
{
    while(finish)
    {
        double stock_price = getCurrentStockPrice();
        // let's image it's sending it somewhere...
        usleep(1000);
    }
}

void* stockPriceDisplay(void*data)
{
    while(finish)
    {
        double stock_price = getCurrentStockPrice();
        printf("Dispaly %lf\n", stock_price);
        usleep(10000);
    }
}

int main()
{
    clock_t start, end;
    double cpu_time_used;

    start = clock();

    // initialize mutex
    int status = pthread_rwlock_init(&rwlock, NULL);
    if(SUCCESS != status)
    {
        printThreadErrors("error on rwlock init", status);
        return status;
    }

    pthread_t threads[3];
    pthread_create(&threads[0], NULL, stockPriceUpdater, NULL);
    pthread_create(&threads[1], NULL, stockPriceSender, NULL);
    pthread_create(&threads[2], NULL, stockPriceDisplay, NULL);


    for(int i = 0; i < 3; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Should be count_inc = 100, and it is = %lf\n", g_stock_price);

    // after work done destroy rwlock
    status = pthread_rwlock_destroy(&rwlock);
    if(SUCCESS != status)
    {
        printThreadErrors("error on rwlock destroy", status);
        return status;
    }

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time used when using rwlock %lf\n", cpu_time_used);
    return 0;
}

void printThreadErrors(const char * const strErrMessage, int status)
{
    printf("%s, err code=%d, err message%s\n", strErrMessage, status, strerror(status));
}