#include <stdlib.h>
#include <stdio.h>
#include <threads.h> // for thread_local
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#define THREADS_NUMBER 3

thread_local int gi_thread_local = 123;

void* thread_routine(void * args)
{
    for(int i = 0; i < 3; i++)
    {
        gi_thread_local++;
    }
    printf("End from thread with value = %d, TID=%ld and thread_local_adr = 0x%lx\n", gi_thread_local, pthread_self(), &gi_thread_local); 
    return args;
}

int main()
{
    pthread_t tid[THREADS_NUMBER];
    int status;

    for(int i = 0; i < THREADS_NUMBER; i++)
    {
        status = pthread_create(&tid[i], NULL, thread_routine, NULL);
        if(0 != status)
        {
            fprintf(stderr, "ERROR: %s\n", strerror(status));
            return status;
        }
    }


    for(int i = 0; i < THREADS_NUMBER; i++)
    {
        (void)pthread_join(tid[i], NULL);
        printf("Thread with TID = %ld is finish.\n", tid[i]); 
    }
    printf("Thread local in main after join = %d\n", gi_thread_local); 
    return 0;
}
