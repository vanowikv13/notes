#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SUCCESS 0
#define MAX_GLOBAL_VALUE 10000
#define THREADS_NUMBER 4

#define PRINT_THREAD_ERROR(message, status) fprintf(stderr, "%s, err code=%d, err message%s\n", message, status, strerror(status))
#define THREAD_ERROR_EXIT(message, status) ({\
        if(SUCCESS != status)\
        {\
            PRINT_THREAD_ERROR(message, status);\
            exit(status);\
        }})

// spinlock
pthread_spinlock_t spinlock;

int thread_counter[THREADS_NUMBER];
int global_counter;

void* thread_routine(void*args)
{
    int id = *((int*)args);
    for(;;)
    {
        pthread_spin_lock(&spinlock);
        if(global_counter < MAX_GLOBAL_VALUE)
        {
            global_counter++;
            thread_counter[id]++;
        } else
        {
            pthread_spin_unlock(&spinlock);
            break;
        }
        pthread_spin_unlock(&spinlock);
    }
}

int main(void)
{
    int status;
    pthread_t threads[THREADS_NUMBER];
    int thread_index[THREADS_NUMBER];

    status = pthread_spin_init(&spinlock, PTHREAD_PROCESS_PRIVATE);
    THREAD_ERROR_EXIT("error on locking spinlock", status);

    // lock spinlock before running threads
    status =  pthread_spin_lock(&spinlock);
    THREAD_ERROR_EXIT("error on locking spinlock", status);

    for(int i = 0; i < THREADS_NUMBER; i++)
    {
        thread_index[i] = i;
        status = pthread_create(threads + i, NULL, thread_routine, &thread_index[i]);
        THREAD_ERROR_EXIT("error on creating thread", status);
    }

    pthread_spin_unlock(&spinlock);
    
    for(int i = 0; i < THREADS_NUMBER; i++)
    {
        status = pthread_join(threads[i], NULL);
        THREAD_ERROR_EXIT("error on thread join", status);
    }
    
    // after work done destroy spinlock
    status = pthread_spin_destroy(&spinlock);
    THREAD_ERROR_EXIT("error on spinlock destroy", status);
    
    puts("Summary:\n");
    for (int i = 0; i < THREADS_NUMBER; i++)
    {
        printf("Thread with id = %d, count = %d\n", i, thread_counter[i]);
    }
    
    return SUCCESS;
}