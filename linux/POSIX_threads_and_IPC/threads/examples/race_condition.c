#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

int count_inc = 0;

void *writter(void *data)
{
    for(int i = 0; i < 100; i++)
    {
        usleep(10);
        count_inc = count_inc + 1;
    }
    return NULL;
}

int main()
{
    pthread_t threads[3];
    for(int i = 0; i < 3; i++)
    {
        pthread_create(threads + i, NULL, writter, NULL);
    }
    
    for(int i = 0; i < 3; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Should be count_inc = 300, but it is = %d", count_inc);
    
    /*
     What happen here is that each time count_inc is changed it's done this operation (in 0x86):
        0x0000000000001198 <+31>:	mov    eax,DWORD PTR [rip+0x2eb6]        # 0x4054 <count_inc>
        // Fetching value of count_inc into eax register
        
        0x000000000000119e <+37>:	add    eax,0x1
        // Adding to fetched value 1 in register

        0x00000000000011a1 <+40>:	mov    DWORD PTR [rip+0x2ead],eax        # 0x4054 <count_inc>
        // Store value back from eax register to count_inc

     Change of count_inc is handled in 3 steps, so if two or more threads are
     doing the same action at the same time, then it's can leads to race condtions.
     In which for example one thread is at step of fetching a value and other one is 
     changing it, so when first is updating it doesn't have latest value of count_inc.
     This will result in losing some data.
    */
    return 0;
}