#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

// good practice: use mutex when using same global by multiple threads or any shared data between two or more threads
int global = 0;

void* startRoutine(void * args) {
    static int static_th = 0;
    global++;
    static_th += global;
    printf("In thread. tid= %d, i= %d, static= %d, global= %d\n", getpid(), *(int*)args, static_th, global);
    return args;
}

int main(int argc, char *argv[], char **env) {
    
    //pthread_t - thread identifier TIDe
    pthread_t tid;
    int arr[3] = {0};

    for (int i = 0; i < 3; ++i)
    {
        arr[i] = i;
        int status = pthread_create(&tid, NULL, startRoutine, (void*)(&arr[i]));
        if (status != 0)
        {
            printf("ERROR: %s\n", strerror(status));
            return 1;
        }
    }
    sleep(3);
    printf("Global after all=%d\n", global);
    return 0;
}