#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

void* startRoutine(void * args)
{
    *(int*)args = 0;
    sleep(2);
    return args;
}

int main(int argc, char *argv[], char **env)
{
    pthread_t tid;
    int var = 12;

    int status = pthread_create(&tid, NULL,startRoutine, &var);
    if(status != 0)
    {
        fprintf(stderr, "ERROR: %s\n", strerror(status));
        return status;
    }

    status = pthread_join(tid, NULL);
    if(status != 0)
    {
        fprintf(stderr, "ERROR: %s\n", strerror(status));
        return status;
    }

    printf("var=%d\n", var);
    return 0;
}