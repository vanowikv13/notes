#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

void* startRoutine(void * args)
{
    *(int*)args = 0;
    sleep(10);
    return args;
}

#define SUCCESS 0
#define ERRNO_ERR(err_code, err_message) \
    if(SUCCESS != err_code) \
        fprintf(stderr, err_message, strerror(err_code));

int main(int argc, char *argv[], char **env)
{
    pthread_t tid;
    int var = 12;

    int status = pthread_create(&tid, NULL,startRoutine, &var);
    ERRNO_ERR(status, "Error on thread creation: %s\n");

    // Detach a thread.
    status = pthread_detach(tid);
    ERRNO_ERR(status, "Error on detach: %s\n");

    // This line should end-up with error, as it is not acceptable to join a thread that was detached.
    status = pthread_join(tid, NULL);
    ERRNO_ERR(status, "Error on join: %s\n");

    // Also as we do not wait for detached thead, this value should not be changed at this moment.
    printf("var=%d\n", var);
    return 0;
}