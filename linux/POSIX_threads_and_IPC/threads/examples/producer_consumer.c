#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SUCCESS 0
#define PRODUCER_BUFFER_LEN 100

#define PRINT_THREAD_ERROR(message, status) fprintf(stderr, "%s, err code=%d, err message%s\n", message, status, strerror(status))
#define THREAD_ERROR_EXIT(message, status) ({\
        if(SUCCESS != status)\
        {\
            PRINT_THREAD_ERROR(message, status);\
            exit(status);\
        }})

// mutex to use with condidion variable for consumer notification
pthread_mutex_t notify_consumer_mutex = PTHREAD_MUTEX_INITIALIZER;

// condition variable for consumer notificaion
pthread_cond_t notify_consumer = PTHREAD_COND_INITIALIZER;

char message_buffer[PRODUCER_BUFFER_LEN];

void producer(void)
{
    for(;;)
    {
        // before notifying a customer lock a mutex
        int status = pthread_mutex_lock(&notify_consumer_mutex);
        if(SUCCESS == status)
        {
            if(NULL == fgets(message_buffer, PRODUCER_BUFFER_LEN - 1, stdin))
            {
                PRINT_THREAD_ERROR("error on reading from fgets", 0);
                break;
            }

            // notify consumer about new data arrive
            pthread_cond_signal(&notify_consumer);

            // unlock mutex to let consumer use it
            status = pthread_mutex_unlock(&notify_consumer_mutex);
            THREAD_ERROR_EXIT("error on mutex unlock", status);

            // add delay to let consumer get access to critical section 
            // this could be done with another critical variable, but for simplicity sleep is used
            usleep(300);
        } else
        {
            PRINT_THREAD_ERROR("fail on mutex lock", status);
        }

    }
}


void* consumer(void*args)
{
    for(;;)
    {
        // mutex should be always locked before calling wait
        int status = pthread_mutex_lock(&notify_consumer_mutex);
        if(SUCCESS == status)
        {
            int status = pthread_cond_wait(&notify_consumer, &notify_consumer_mutex);
            if(SUCCESS == status)
            {
                // critical section start
                printf("String from input: %s\n", message_buffer);
                // critical section end

                // unlock mutex to let consumer use it
                pthread_mutex_unlock(&notify_consumer_mutex);
                THREAD_ERROR_EXIT("error on mutex unlock", status);
            } else
            {
                PRINT_THREAD_ERROR("fail on condition variable wait", status);
            }
        } else
        {
            PRINT_THREAD_ERROR("fail on mutex lock", status);
        }
    }
}

int main(void)
{
    int status;
    pthread_t consumer_id_t;

    // creating consumer thread
    status = pthread_create(&consumer_id_t, NULL, &consumer, NULL);
    THREAD_ERROR_EXIT("error on creating consumer thread", status);
    
    // wait to start consumer first
    sleep(2);

    // calling producer from main thread.
    producer();

    // join consumer thread
    status = pthread_join(consumer_id_t, NULL);
    THREAD_ERROR_EXIT("error on thread join", status);
    
    // after work done destroy mutex
    status = pthread_mutex_destroy(&notify_consumer_mutex);
    THREAD_ERROR_EXIT("error on mutex destroy", status);

    // after work done destroy condition variable
    status = pthread_cond_destroy(&notify_consumer);
    THREAD_ERROR_EXIT("error on condition var destroy", status);
    return SUCCESS;
}