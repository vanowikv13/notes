# Threads in linux subsystem with C

## Threads

Threads are a way for a program to divide itself into two or more simultaneously (or pseudo-simultaneously) running tasks and all of them are within the scope of a single process.
Compared to processes threads are not independent like processes and share the same:

 1) Code section
 2) Data section
 3) Heap
 4) User and Group ID (UID, GID)
 5) OS resources (file descriptors, signals, cwd ...)

Like processes they have their own:

1) Stack space
2) Registers (PC and others)
3) Thread ID
4) TLS (Thread-local storage) - additional memory space for the thread's own global and static variables. (thread_local)

### Types of threads

* CPU Bound - in simple words means that the thread is only bottlenecked by the CPU. Usually used for threads that make the complicated calculations.

Example:

```cpp
while(true)
{
    printf("Constant using processor time\n");
}
```

* I/O Bound - thread is bottlenecked by the I/O operation like mutex, interrupts, sleep, etc. While they could not operate they usually are sleeping and not using processor time and are working only when the given blocker is released.

Example:

```cpp
while(true)
{
    printf("Constant using processor time\n");
    sleep(10); // not using CPU time during sleep
}
```

In POSIX sleep there is information that sleep() call will cause the calling thread to be suspended from execution. (man 3 thread)

### To use pthreads with linux and C

1) #include <pthread.h>
2) compile with -pthread flag

## POSIX threads - pthread

### Create thread with pthread_create

Declaration:

```cpp
int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);
```

About: The pthread_create() function starts a new thread in the calling process. The new thread starts execution by invoking start_routine(); Arg is passed as the argument to start_routine().

Arguments:

* pthread_t *thread - store thread id in the ptr to var specified.
* const pthread_attr_t *attr - The attr argument points to a pthread_attr_t structure whose contents are used  at thread creation. If attr is NULL then default attr is used.
* void *(*start_routine) (void *) - what new tread should start after creation.
* void *arg - ptr to data that initialize threads. Arg is send exactly to our function start_routing as an argument.

Return status:
Zero on success other number on some error or warnings.

Termination of pthread after creation:

* `void pthread_exit(void *retval);` - terminate a thread and return value via `void *retval`. The value is also available to other thread when calling pthread_join.
* Simply returns from start_routine, which equivalent to pthread_exit.
* Can be cancelled with pthread_cancel (more: man pthread_cancel)
* Any of the threads calls exit(retStatus) or main thread perform return from main(). This cause the termination of all threads.

Example:

```cpp
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

void* startRoutine(void * args)
{
    *(int*)args = 0;
    return args;
}

int main(void)
{
    //pthread_t - thread identifier TID
    pthread_t tid;
    int x = 12;

    //creating new thread
    int status = pthread_create(&tid, NULL,startRoutine, &x);
    if(status != 0)
    {
        // errno error messages used to print pthread error message
        printf("ERROR: %s\n", strerror(status));
        return status;
    }

    printf("%d\n", x);
    sleep(3);
    return 0;
}
```

Threads with static and global

```cpp
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

// good practice: use mutex when using same global by multiple threads or any shared data between two or more threads
int global = 0;

void* startRoutine(void * args) {
    static int static_th = 0;
    global++;
    static_th += global;
    printf("In thread. tid= %d, i= %d, static= %d, global= %d\n", getpid(), *(int*)args, static_th, global);
    return args;
}

int main(int argc, char *argv[], char **env) {
    
    //pthread_t - thread identifier TIDe
    pthread_t tid;
    int arr[3] = {0};

    for (int i = 0; i < 3; ++i)
    {
        arr[i] = i;
        int status = pthread_create(&tid, NULL, startRoutine, (void*)(&arr[i]));
        if (status != 0)
        {
            printf("ERROR: %s\n", strerror(status));
            return 1;
        }
    }
    sleep(3);
    printf("Global after all=%d\n", global);
    return 0;
}
```

The whole file is in examples/threads_with_globals.c

### Waiting for thread with pthread_join

About: Wait for thread to finish it's job.

Scheme:

```cpp
int pthread_join(pthread_t thread, void **retval);
```

Arguments:

* pthread_t thread - tid of a thread we want to wait for.
* void **retval - where we want to store return value. If NULL given it will not set return value for us.

Return status:
Zero on success other number on some error or warnings.

Example:

```cpp
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

void* startRoutine(void * args) {
    *(int*)args = 0;
    sleep(2);
    return args;
}

int main(int argc, char *argv[], char **env) {
    pthread_t tid;
    int var = 12;

    int status = pthread_create(&tid, NULL,startRoutine, &var);
    if(status != 0) {
        printf("ERROR: %s\n", strerror(status));
        return 1;
    }

    int retThreadStatus = pthread_join(tid, NULL);
    if(retThreadStatus != 0) {
        printf("ERROR: %s\n", strerror(retThreadStatus));
        return 2;
    }
    printf("var=%d\n", var);
    return 0;
}
```

### Detach a thread

Set a thread identified by TID as detached. The detached thread when terminates releases all the resource to the system without a need from another thread to join. Once a thread has been detached, it can't be joined by using pthread_join().

```cpp
int pthread_detach(pthread_t thread);
```

Arguments:
    - thread - tid of a thread to detach.
Return value: 0 in case of success other number in case of failure.

Example: [examples/detach.c](examples/detach.c)

### Get own TID with pthread_self

About: return thread id

Scheme:

```cpp
pthread_t pthread_self(void);
```

Return value:

* thread id (TID)

Example:

```cpp
void *start_routine(void *data)
{
    pthread_t own_tid = pthread_self();
    printf("Thread with tid=%ld, i=%d\n", own_tid, *(int*)data);
    return data;
}

int main()
{
    pthread_t tid[2];
    int tab[2] = {1, 2};
    for(int i = 0; i < 2; i++)
    {
        pthread_create(tid + i, NULL, start_routine, (void*)(tab+i));
    }
    
    for(int i = 0; i < 2; i++)
    {
        pthread_join(tid[i], NULL);
        printf("Thread with tid=%ld is finished.\n", tid[i]);
    }
}
```

### Thread local storage (not POSIX convention)

It's uses static or global memory for a thread to not share it between threads, but just have each instance of variable/object for a each threads. Example of thread local storage is errno.

To see example look: examples/thread_local.c

### Race conditions

Data races can occur when two or more threads are accessing data and they trying to change it at the same time. Thread scheduler can schedule threads time differently, so we don't know which thread will access given region first.

Given problem usually exists in the read and write situation.

```cpp
if(2 == index)
{
    // it's possible that index would be changed between reading above and using it here.
    indexVal = index++ * 2;
}
```

In given example index could be changed between reading it and writing. To handle this issues mutex could be lock before first index use in this thread and release after it's last time used. To handle this type of issues mutex, semaphores, critical sections and other could be used.

Look more in examples/race_condition.c

### Atomic

Atomic operation is the one that could be completed in single step/instruction, which means that no other thread can observe the modification half-completed. Usually it's can be used for 1,2,4 bytes type, which could be updated in single instruction. In some cases even if there is assembly level single instruction CPU might split this instruction to two under and it's also is not atomic. One of the example is on 0x86 architecture it's mov instruction, which in case memory address is not correctly aligned (not divisible by 4).

### Mutex (mutual exclusion)

The most used synchronization mechanism. They're used to ensure that only one thread is using protected resource at the time. Mutex are needed when multiple threads want to update the same resource. This is a safe way to ensure that when several threads update the same variable, the final value is the same as what it would be if only one thread performed the update. The variables being updated belong to a "critical section". Mutex protects from data races.
Mutex could be either in one of two states busy and free.

Not like in spinlock when thread is waiting for mutex to be unlocked it's might be asleep (In not all cases it's supported). It's nice way to not waste processor time. But when falling asleep it's also might bring additional delay in compare to constantly checking if mutex resource is available.

To create a mutex:

```cpp
pthread_mutex_t mutex;
```

Mutex functions:

```cpp
// initialize mutex - more: man 3 pthread_mutex_init
pthread_mutex_init(pthread_mutex_t *restrict mutex, const pthread_mutexattr_t *restrict attr);

// destroy mutex - more: man 3 pthread_mutex_destroy
// trying to destroy mutex while it's not initialized will result in UB
pthread_mutex_destroy(pthread_mutex_t *mutex);

// more: man 3 pthread_mutex_lock
// if mutex is not already locked then lock it and let us use resource.
// if mutex is already locked then suspend thread till mutex will be available and then lock it for current caller.
pthread_mutex_lock(pthread_mutex_t *mutex);

// more: man 3 pthread_mutex_trylock
// if mutex is already locked then function return immediately err code EBUSY
// if mutex is not locked then lock a mutex for current caller
pthread_mutex_trylock(pthread_mutex_t *mutex);

// unlock locked mutex - more: man 3 pthread_mutex_unlock
pthread_mutex_unlock(pthread_mutex_t *mutex);

// works the same as pthread_mutex_lock, but when mutex is already locked when calling
// it will wait till given timer expire (abstime). If given timer expire function return EAGAIN.
pthread_mutex_timedlock(pthread_mutex_t *restrict mutex, const struct timespec *restrict abstime);


// given functions in case of success return 0 in case of failure return
// failure error, failure error could be described with errno error values.
```

Code example:

```cpp
/** look to race_condition.c in example without mutex **/

int count_inc = 0;

// mutex object
pthread_mutex_t count_inc_mutex;

void *start_routine(void *data)
{
    for(int i = 0; i < 100; i++)
    {
        pthread_mutex_lock(&count_inc_mutex);
        // critical section enter

        usleep(10);
        count_inc = count_inc + 1;
        
        // critical section exit
        pthread_mutex_unlock(&count_inc_mutex);
    }
    return NULL;
}

int main()
{
    // initialize mutex
    pthread_mutex_init(&count_inc_mutex, NULL);

    pthread_t threads[3];
    for(int i = 0; i < 3; i++)
    {
        pthread_create(threads + i, NULL, start_routine, NULL);
    }
    
    for(int i = 0; i < 3; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Should be count_inc = 300, and it is = %d", count_inc);
    
    // after work done destroy mutex
    pthread_mutex_destroy(&count_inc_mutex);
    return 0;
}
```

look into full code in: examples/mutex.c

### Mutex vs spinlock

In theory if given threads is trying to acquire a mutex, which is already locked then system will put given thread to sleep to let other threads use cpu time. The main difference between mutex and spinlock is that if spinlock would be used in this situation thread will not go to sleep, but will be constantly atomically checking if resource is available, so it will use all the time CPU time, but it will also faster enter critical section.

Assembly implementation of spinlock is in examples/spinlock_asm_example

### Reader and writer lock - rwlock

Issues with synchronization are happen only when given object is modified, while other threads are trying to use th resource. So, there is not such a problem with data racing unless at least one of the threads is not editing shared resource. In such condition where there is many readers and only few writers it's used specific kind of mutex reader-writer lock (rwlock).

Given lock could end-up in two states:

* Reader - in given state multiple threads can use resource only for reading and they do not have to wait.

* Writer - works the same as normal mutex, no one can access resource until given thread using it in this state will not finish its work.

Example of read/write lock is in example/rwlock.c

### Conditional variable

Condition variables are used to lock a thread until specific condition is met (or not). That's why mutex is not enough, but they're just used to keep of track when given critical resource is available not if given condition is met, so threads would continually poll the data and check if the condition is met (wasting with it CPU time).

The case when condition variables are useful is producer and consumers problem.
Consumer in this situation only want access to resource when it's actually produced by producer, so given thread doesn't need to work till that condition is occur.
Producer is producing a shared resource for consumer (which should be still protected with mutex if both threads are accessing given resource) and when it's finish producing it it's notify consumer, so it will start trying accessing given resource.

Condition variables always should be used with mutext etc.. Condition variables also could have only one attribute, which let given condition variable be used by other threads in other processes (by default it's turned off).

Objects and functions:

```cpp
// condition variable object initialized statically with default attributes
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

// more in man 3 pthread_cond_init
// initialize condition variable either this way or like above 
int pthread_cond_init(pthread_cond_t *restrict cond, const pthread_condattr_t *restrict attr);

// more in man 3 pthread_cond_destroy
// destroy condition object 
int pthread_cond_destroy(pthread_cond_t *cond);

// more in man 3 pthread_cond_wait() 
// block calling threads until the specified condition is signaled
// when called with this function second argument mutex should be already locked. This function release the mutex when starts waiting for signal. After signal is received and thread is running, mutex will be automatically lock for use by waiting thread. Then after given thread finish it's work mutex should be unlocked.
int pthread_cond_wait(pthread_cond_t *restrict cond, pthread_mutex_t *restrict mutex);

// more in man 3 pthread_cond_signal()
// used to signal another thread (only one to unlock more use pthread_cond_broadcast), which is waiting on the condition variable. It should be called after mutex is acquire by threads that notify and after it's finish it's work it's must unlock the mutex to let waiting thread to run.
int pthread_cond_signal(pthread_cond_t *cond);

// more in man 3 pthread_cond_broadcast
// works the same like pthread_cond_signal with one main difference that it can unlock more than one waiting threads on condition variable.
int pthread_cond_broadcast(pthread_cond_t *cond);

// given functions in case of success return 0 in case of failure return
// failure error, failure error could be described with errno error values.
```

Important:

* Condition variable always should be used with mutex, because condition variable itself do not protect resource from data races fully.
* Signaling condition usually should be called after other thread is already waiting.
* Remember to always lock a mutex before calling pthread_cond_wait, pthread_cond_signal and pthread_cond_broadcast. Also after those call and other action made mutex should be unlocked.

Example: examples/condition_variable.c

### TODO: Semaphores????

### Deadlock

Is situation where one or more threads are waiting for a locked resource that will never be unlocked. The given issues usually exist because of two things:

1) Programmer mistake to forgot release mutex etc.
2) There are two protected resource X and Y that is used by two threads A and B. If first threads X start to use A resource and other threads start to using B resource. Then when X will try to use B resource while using A it will be blocked until Y unlock B and then if Y will try to use A then it also will be blocked, so both threads will end-up in deadlocks.

To not end-up in deadlocks:

* always use locks in the same order if possible
* if possible do not lock something while using another lock.
* use as less locks as possible
* timed locks request might be useful

Example: example/deadlocks.c
Homework: change given deadlocks code to not have deadlocks anymore.

### Process (thread) starvation

Process starvation is the problem when one of the threads or more could not get access to critical resources to handle the routine because other threads steal its access to the critical section.

Reference:

* <https://simple.wikipedia.org/wiki/Thread_(computer_science>)
* man pthread_create
* <https://computing.llnl.gov/tutorials/pthreads/>


## Exercises

1. Compute the sum of n prime numbers into global variable with m threads. After value is computed print it to the screen. Also 2 * m < n and n, m > 1.