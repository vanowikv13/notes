# Assembly synchronization mechanism

## Intro

Looking from the low-lvl, exactly microprocessor perspective there are three general synchronization mechanism:
    1. Memory barrier (memory fence)
    2. Atomic operations
    3. Interlocks operations

### Memory barriers

There are low-lvl instructions that enforce an ordering of instructions on specific memory region issued before and after the barrier instruction. On x86 architecture there are three instructions, which enable us to do it:
 1. lfence
 2. sfence
 3. mfence

link: https://en.wikipedia.org/wiki/Memory_barrier

## On 0x86 architecture






### Links
https://onedrive.live.com/view.aspx?resid=4E86B0CF20EF15AD!24884&app=WordPdf&authkey=!AMtj_EflYn2507c