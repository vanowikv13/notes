# IPC, Thread programming and Concurrency
## Table of contents
- [IPC, Thread programming and Concurrency](#ipc-thread-programming-and-concurrency)
  - [Table of contents](#table-of-contents)
    - [Process programming](#process-programming)
    - [Threads](#threads)
      - [Process vs Thread performance](#process-vs-thread-performance)
    - [IPC communication](#ipc-communication)
    - [Concurrency](#concurrency)

### Process programming
[Process](process/process.md)

### Threads
[Threads](threads/threads.md)

#### Process vs Thread performance
Compared process performance with threads:
1) Threads are much lightweight (cause they share a lot of data)
2) Threads have faster context switching
3) Threads have better and faster communication
4) Threads have cheaper creation and can be terminated easily.
5) Process can still exist after our main process is finished.

### IPC communication
 - Shared memory
 - Socket
 - D-bus
 - Pipes

### Concurrency
 - example with bare metal