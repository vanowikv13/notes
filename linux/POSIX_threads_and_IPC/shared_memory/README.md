# Shared memory

### mmap
mmap - lets us map part of memory for our usage.

```cpp
#include <stdio.h>    //printf
#include <unistd.h>   //fork, getpagesize()
#include <sys/wait.h> //wait
#include <sys/mman.h> //mmap, flags for mmap

int main() {
    //getpagesize() - return address of regular page size in memory for me it's 4096, to check: getconf PAGESIZE
    //PROT_READ | PROT_WRITE - page may be read and write
    //MAP_SHARED | MAP_ANONYMOUS - memory might be visible to other process and it's not backed to any file
    //-1 - file descriptor we want to backed our file if -1 it's not backed to any file, required by MAP_ANONYMOUS
    //0 - offset of memory
    int *shared = mmap(NULL, getpagesize(), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    *shared = 0;
    int var = 0;

    //creating new process from this process
    if(0 == fork()) {
        //we're in child
        var = 1;
        *shared = 1;
        puts("child");
    } else {
        //we're in parent
        wait(NULL);
        puts("parent");
    }
    printf("Not shared: %d\n", var);
    printf("Shared memory: %d\n", *shared);
    return 0;
}
```

### Shared memory with shmget
[example](examples/x_o_shared_memory.c)