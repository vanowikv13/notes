#include <stdio.h>    //printf
#include <stdlib.h>   //system
#include <errno.h>
#include <sys/shm.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>

void report_and_exit(const char* msg) {
    perror(msg);
    exit(-1);
}

int main(int argc, char* argv[]) {
    if(argc != 2) {
        printf("give filename of shared memory.\n");
    }

    int fd = shm_open(argv[1], O_RDWR | O_CREAT, 0644);
    if(fd < 0)
        report_and_exit("Can't open shared mem segment\n");

    ftruncate(fd, 8);
    caddr_t memptr = (caddr_t)mmap(NULL,       /* let system pick where to put segment */
                          8,   /* how many bytes */
                          PROT_READ | PROT_WRITE, /* access protections */
                          MAP_SHARED, /* mapping visible to other processes */
                          fd,         /* file descriptor */
                          0);         /* offset: start at 1st byte */
    if((caddr_t)-1 == memptr)
        report_and_exit("Can't get segment.\n");

    /* semaphore code to lock the shared mem */
    sem_t* semptr = sem_open("Test", /* name */
                             O_CREAT,       /* create the semaphore */
                             0666,   /* protection perms */
                             0);            /* initial value */
    if (semptr == (void*) -1) report_and_exit("sem_open");

    strcpy(memptr, "123456789\0"); /* copy some ASCII bytes to the segment */

    /* increment the semaphore so that memreader can read */
    if (sem_post(semptr) < 0) report_and_exit("sem_post");


    sleep(4);
    /* clean up */
    munmap(memptr, 8); /* unmap the storage */
    close(fd);
    sem_close(semptr);
    shm_unlink(argv[1]); /* unlink from the backing file, remove shared memory region */
    return 0;
}