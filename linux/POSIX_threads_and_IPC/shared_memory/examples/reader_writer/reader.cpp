#include <stdio.h>    //printf
#include <stdlib.h>   //system
#include <errno.h>
#include <sys/shm.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>

void report_and_exit(const char* msg) {
    perror(msg);
    exit(-1);
}

int main(int argc, char* argv[]) {
    if(argc != 2) {
        printf("give filename of shared memory.\n");
    }

    int fd = shm_open(argv[1], O_RDWR, 0644);
    if(fd < 0)
        report_and_exit("Can't open shared mem segment\n");

    ftruncate(fd, 8);
    caddr_t memptr = (caddr_t)mmap(NULL,       /* let system pick where to put segment */
                          8,   /* how many bytes */
                          PROT_READ | PROT_WRITE, /* access protections */
                          MAP_SHARED, /* mapping visible to other processes */
                          fd,         /* file descriptor */
                          0);         /* offset: start at 1st byte */
    if((caddr_t)-1 == memptr)
        report_and_exit("Can't get segment.\n");

    /* semaphore code to lock the shared mem */
    sem_t* semptr = sem_open("Test", /* name */
                             O_CREAT,       /* create the semaphore */
                             0644,   /* protection perms */
                             0);            /* initial value */
    if (semptr == (void*) -1) report_and_exit("sem_open");


    //if condition true finish reader process
    bool status = true;
    while(*(int*)memptr != 0x00000000) {
        if (!sem_wait(semptr)) {
            //get message from user and write something
            for (int i = 0; i < strlen(memptr); ++i)
                write(STDOUT_FILENO, memptr + i, 1);
            sem_post(semptr);
        } else {
            int* previous = false;
            while(*(int*)memptr != 0xffffffff && status) {
                for (int i = 0; i < strlen(memptr); ++i)
                    write(STDOUT_FILENO, memptr + i, 1);
                status = false;
            }
        }
    }



    /* clean up */
    munmap(memptr, 8); /* unmap the storage */
    close(fd);
    sem_close(semptr);
    unlink(argv[1]);
    return 0;
}