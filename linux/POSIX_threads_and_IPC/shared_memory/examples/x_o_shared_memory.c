#include <stdio.h>    //printf
#include <stdlib.h>   //system
#include <errno.h>
#include <sys/shm.h>
#include <string.h>

int wasNotSet = 0;

int get_shared_block(char* filename, int size) {
    key_t key = ftok(filename, 0);
    if(key == -1)
        return -1;
    int val = shmget(key, size, 0);
    if(errno == ENOENT) {
        val = shmget(key, size,0666 | IPC_CREAT);
        wasNotSet = 1;
    }
    return val;
}

char* attach_memory_block(char*filename, int size) {
    //create shared block if not exist otherwise return existing
    int shared_block_id = get_shared_block(filename, size);

    if(shared_block_id == -1) {
        printf("Wrong shared block id\n");
        return NULL;
    }

    //attach to shared memory block
    char* result = shmat(shared_block_id, NULL, 0);
    if(result == (char*)-1) {
        printf("Could not get address of block. Error in shmat\n");
        return NULL;
    }
    return result;
}

int detach_memory_block(char *block) {
    return shmdt(block) != -1 ? 1 : -1;
}

int destroy_memory_block(char* filename) {
    int shared_block_id = get_shared_block(filename, 0);
    if(shared_block_id == -1)
        return -1;
    return shmctl(shared_block_id, IPC_RMID, NULL) != -1 ? 1 : -1;
}

void setupArea(char* ptr) {
    for(int i = 0; i < 9; i++)
        *(ptr+i) = (char)(49 + i);
}

int isFieldFree(const char* ptr, int fieldNumber) {
    if(fieldNumber < 0 || fieldNumber > 9)
        return 0;

    if(*(ptr + fieldNumber-1) == 'o' || *(ptr + fieldNumber-1) == 'x')
        return 0;
    return 1;
}

void printArea(const char* ptr) {
    printf("%c | %c | %c\n", *ptr, *(ptr+1), *(ptr+2));
    printf("___ ____ ___\n");
    printf("%c | %c | %c\n", *(ptr+3), *(ptr+4), *(ptr+5));
    printf("___ ____ ___\n");
    printf("%c | %c | %c\n", *(ptr+6), *(ptr+7), *(ptr+8));
}

int checkIfWin(const char*ptr) {
    return ((*(ptr) == *(ptr+1) && *(ptr+1) == *(ptr+2)) || (*(ptr+3) == *(ptr+4) && *(ptr+4) == *(ptr+5)) || (*(ptr+6) == *(ptr+7) && *(ptr+7) == *(ptr+8))) ||
    ((*(ptr) == *(ptr+3) && *(ptr+3) == *(ptr+6)) || (*(ptr+1) == *(ptr+4) && *(ptr+4) == *(ptr+7)) || (*(ptr+2) == *(ptr+5) && *(ptr+5) == *(ptr+8))) ||
    ((*(ptr) == *(ptr+4) && *(ptr+4) == *(ptr+8)) || (*(ptr+2) == *(ptr+4) && *(ptr+4) == *(ptr+6)));
}

int checkIfSingle(const char * ptr) {
    return (*ptr == 'o' || *ptr == 'x');
}

int checkIfTie(const char* ptr) {
    for (int i = 0; i < 9; ++i) {
        if(!checkIfSingle((ptr + i)))
            return 0;
    }
    return 1;
}

void finishGameWithWinner(char *ptr, char* fileName, char enemyTurnID, char* message) {
    *ptr = enemyTurnID;
    printf("%s", message);
    detach_memory_block(fileName);
    destroy_memory_block(fileName);
    exit(0);
}

int gameForSign(char* shared_memory, char* fileName, char enemyTurnID, char yourSign) {
    int input = -1;
    system("clear");
    if(checkIfWin(shared_memory+1))
        finishGameWithWinner(shared_memory, fileName, enemyTurnID, "You lose\n");
    if(checkIfTie(shared_memory+1))
        finishGameWithWinner(shared_memory, fileName, enemyTurnID, "Tie\n");
    printArea(shared_memory+1);
    printf("You are %c select field:\n", yourSign);
    scanf("%d", &input);
    while(!isFieldFree(shared_memory+1, input)) {
        system("clear");
        printArea(shared_memory+1);
        printf("Wrong number\nYou are %c select field:\n", yourSign);
        scanf("%d", &input);
    }
    *(shared_memory+input) = yourSign;
    *shared_memory = enemyTurnID;
    if(checkIfWin(shared_memory+1)) {
        printf("You win\n");
        exit(0);
    }
    if(checkIfTie(shared_memory+1)) {
        printf("Tie\n");
        exit(0);
    }
}

int main(int argc, char* argv[]) {
    if(argc != 2) {
        printf("give filename of shared memory.\n");
    }

    char* shared_memory = attach_memory_block(argv[1], 4096);
    if(shared_memory == NULL) {
        printf("Block could not be created\n");
        return -1;
    }

    if(wasNotSet == 0) {
        //O player
        *shared_memory = 0x1; //set enemy turn
        while(*shared_memory != 0x0) {
            if(*shared_memory == 0x2)
                gameForSign(shared_memory, argv[1], 0x1, 'o');
            system("clear");
            printf("X turn.\n");
            printArea(shared_memory+1);
        }
    } else {
        //X player
        setupArea(shared_memory+1);
        *shared_memory = 0x2;
        while(*shared_memory != 0x0) {
            if(*shared_memory == 0x1)
                gameForSign(shared_memory, argv[1], 0x2, 'x');
            system("clear");
            printf("O turn.\n");
            printArea(shared_memory+1);
        }
    }
    detach_memory_block(argv[1]);
    return 0;
}