#!/bin/bash

if [ ! -d "$1" ]
then
	echo "$1 is not directory"
	exit -1
fi

if [ "$#" != 3 ]
then
	echo "Wrong number of parameters"
	echo "Usage: ./srcript directory fromExtension toExtension"
	exit -1
fi

fromExtension=`echo $2 | cut -d '.' -f2`
toExtension=`echo $3 | cut -d '.' -f2`

for i in `ls -a $1`
do
	file="$i"
	extension="${file##*.}"
	filename="${file%.*}"

	if [ "$extension" == "$fromExtension" ]
	then
		mv -v "$1/$file" "$1/$filename.$toExtension"
	fi
done