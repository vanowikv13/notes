#!/bin/bash

read A operation B

case "$operation" in
    '+')
        echo $(($A+$B));;
    '-')
        echo $(($A-$B));;
    '*')
        echo $(($A*$B));;
    '/')
        echo $(($A / $B));;
    *)
        echo "Usage: A operand B (Possible operands: [+ | - | * | /]";;
esac
