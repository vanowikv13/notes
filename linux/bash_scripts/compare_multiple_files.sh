#!/bin/bash

file=$1
shift #move arguments to the left

if [ ! -f "$file" ]
then
    echo "Wrong filename $file!!!"
    exit 1
fi


for i in "$@"
do
    if [ -f "$i" ]
    then
        cmp "$file" "$i"
    else
        echo "File: $i do not exits"
    fi
done