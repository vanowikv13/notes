# to run this: awk -f read_users.awk /etc/passwd
BEGIN { FS=":"; print "Users information: \n" }
{
    print "username: " $1 "\n",
    "UID: "$3"\n",
    "GID: "$4"\n"
    "User ID info: "$5"\n"
}
END { print "---------------------\n" }