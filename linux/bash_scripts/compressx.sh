#!/bin/bash

# This script provides an easy way for user to copmress a directory

echo "Chose compression method:"

cat << COMPRESSX
gzip
tar
zip
compress
COMPRESSX

echo -n "Chouse your method? "
read method

echo -n "What is path to directory to compress? "
read path

echo -n "How would you like to name compressed file? "
read compressedName

case $method in
    gzip)
        gzip -r $path
    ;;
    tar)
        tar -czpf $compressedName $path
    ;;
    zip)
        zip -r $path $compressedName
    ;;
    compress)
        compress -rv $path
    ;;
esac