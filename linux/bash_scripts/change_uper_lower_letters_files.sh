#!/bin/bash

if [ ! -d "$1" ]
then
	echo "$1 is not directory"
	exit -1
fi


for i in `ls -a $1`
do
	file="$i"
	size=${#file}
	str=""
	for j in `seq 0 $size`
	do
		chr="${file:$j:1}"
		if [ $chr != ${chr^^} ]
		then
			str="$str${chr^^}"
		else
			str="$str${chr,,}"
		fi
	done

	if [ $file != $str ]
	then
		mv -v "$1/$file" "$1/$str"
	fi
done