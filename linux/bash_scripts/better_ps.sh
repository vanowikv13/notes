#!/bin/bash

if [ "$1" == "-u" ]
then
	user="$2"
	for pid in `ls /proc/ | egrep -i '^[0-9]' | sort -n`
	do
		uid=`cat /proc/$pid/status 2>/dev/null | grep -i Uid | cut -f 2`
		procUser=`id $uid -un`
		comm=`cat /proc/$pid/comm 2>/dev/null`
		cwd=`readlink -f /proc/$pid/cwd 2>/dev/null`
		if [ -d "/proc/$pid" -a "$procUser" == "$user" ]
		then
			echo -e "$pid\t$comm\t$cwd\n"
		fi
	done
else
	for pid in `ls /proc/ | egrep -i '^[0-9]' | sort -n`
	do
		uid=`cat /proc/$pid/status 2>/dev/null | grep -i Uid | cut -f 2`
		ppid=`cat /proc/$pid/status 2>/dev/null | grep -i PPid | cut -f 2`
		procUser=`id $uid -un`
		comm=`cat /proc/$pid/comm 2>/dev/null`
		if [ -d "/proc/$pid" ]
		then
			echo -e "$pid\t$ppid\t$uid ($procUser)\t$comm"
		fi
	done
fi