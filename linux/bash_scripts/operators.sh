VAR=12
echo $[++VAR] # pre-increment(VAR++ => post-increament)
echo $[VAR**2] # exponentiation
echo $[VAR&3] # bitwise AND
echo $[VAR^4] # bitwise XOR
echo $[VAR|12] # bitwise OR
echo $[VAR&&1] # logical AND
echo $[VAR||0] # logical OR
echo $[VAR > 12 ? 11 : 14] # conditinal evaluation
echo $[VAR<<2] # left bitwise shift (>> right bitwise shift)
VAR=$[~VAR] # bitwise negation
VAR=$[VAR+12] #Adding to VAR 12 (could be also VAR=$((VAR+12)))