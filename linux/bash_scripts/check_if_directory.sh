#!/bin/bash
# Write a script that takes exactly one argument, a directory name.
# If the number of arguments is more or less than one, print a usage message.
# If the argument is not a directory, print another message.
# For the given directory, print the five biggest files and the five files that were most recently modified.

if [[ -d $1 ]]; then
    echo "It's a directory"
    echo "Top 5 bigest files in directory"
    ls "$1" -l | awk ' { print $5, $8 }' | sort -nr | head -5
    echo "Bottom 5 lowest files in directory"
    ls "$1" -l | awk ' { print $5, $8 }' | sort -n | head -5
elif [[ -f $1 ]]; then
    echo "Wrong it's a file"
else
    echo "Wrong it's something else"
fi