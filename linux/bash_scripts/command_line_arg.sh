#!/bin/bash

# checking if we run script with argument like: ./scriptname username
# file name argument index is 0
if [[ $# < 1 ]]; then
    echo "You need to give argument";
    exit 0;
fi

if [[ $1 == $USER ]]; then
    echo "You know your user name"
else
    echo "You do not know your user name"
fi