#!/bin/bash

sum=0
while [ "$#" != 0 ]
do
    sum=$((sum+$1))
    shift
done

echo "Sum: $sum"