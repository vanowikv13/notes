#!/bin/bash

for i in "$@"
do
    if [ -d $i ]
    then
        count=0
        for file in `ls -a $i` 
        do
            if [ ! -r "$i/$file" ]
            then
                count=$((count+1))
            fi
        done
        echo "Directory: $i have files $count that user cannot read"
    else
        echo "File: $i is not a directory"
    fi
done