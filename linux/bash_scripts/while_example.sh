#!/bin/bash

# This script opens N terminal windows.

i="0"

while [ $i -lt $1 ]
do
    xterm &
    i=$[$i+1]
done