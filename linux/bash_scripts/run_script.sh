#!/bin/bash

echo "This script run one of the scripts in this directory"
echo "Enter the number of the file you want to run:"

select FILENAME in *.sh;
do
     echo "You picked $FILENAME ($REPLY), it will be run now."
     bash $FILENAME
done