#!/bin/bash

# in this way could be done arithmetic operations
if (( ($1 >= 35) && ($1 <= 70) )); then # && - and, || - or
    echo "You're and adult and you could be a president."
elif (( $1 >= 18 )); then
    echo "You're and adult."
else
    echo "You're not adult."
fi