$ cat script.sh
#!/bin/bash
# usage: ./script.sh somevalue
echo "Argument for script is: $1."

test ()
{
echo "Reading first argument $1."
RETVALUE=$?
echo "Exit code of function is $RETVALUE"
}

test other_param # usage of function in script