# TTY

When it comes to Linux, TTY is an abstract device in UNIX and Linux. Sometimes it refers to a physical input device such as a serial port, and sometimes it refers to a virtual TTY where it allows users to interact with the system

TTY nowadays is just a piece of software, exactly a kernel subsystem. At the user level of the system, it is a file.

If you type "tty" in the command shell you can see, which TTY is used by the current shell. As it is a file user can write directly to it, and it will be read by the output device.

You can test it by opening two terminal sessions:

```bash
$ tty
# /dev/pts/
```
In the second session:

```bash
echo "aaaa" > /dev/pts/1
```

After that in the first session string "aaaa" should appear.

We can also read from tty with the cat command:

```bash
$ cat /dev/pts/1
```

Now type something in the first shell! As you can see some of the letters appear on the first shell and some on the other.
You can check on your system which users are logged in with "w" command and their tty opened.

Advantages of the TTY:
 - move cursor
 - clear the screen
 - reset the size

To let the user access TTY there is created PTY (pseudo-terminal-slave). When the program needs access to TTY they ask the kernel to create for them PTY, so it always comes in pairs. The slave is given to program and the master is given to the program that asked for it. Both slave and master who are sent to the programs are fd and they can read and write to it. Then the kernel is responsible for copying the content from master PTY to slave PTY and from slave PTY to the master PTY.

terminal emulator - pty master <-- TTY driver( copies stuff from/to) --> pty slave - shell

Reset - reset terminal


Links:
https://www.sobyte.net/post/2022-05/tty/