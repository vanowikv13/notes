# Bash Notes
Bash program should always contain at the beginning of the file below directive. It's information which program should be run this file, here is /bin/bash shell.
```bash 
#!/bin/bash
```
## Shell script
A shell script is a reusable series of commands put in an executable text file.

To make program executable for yourself you need to run command: chmod +x filename.sh.

If you want to make some your script visible in the system you can just add your folder with scripts to PATH with command:
```bash
$ export PATH="$PATH:any_directory"
```
(e.g. any_directory could be ~/Documents/scripts)

## Comments
Remember that comments are really valuable for the scripts readability. So remember to add them.
```bash
# here is a single line comment
: '
multiple
line
comment
'
```
In multiple line comment remember to add space after : and before '

## Debugging script
To run script in debug mode:
```bash
$ bash -xvf you_script.sh
```
* -x => printing command traces before executing command
* -v => prints shell input lines as they are read
* -f => disable file name generation using metacharacters (globing)

This declaration could be also added in a script file with first line of the script:
```bash 
#!/bin/bash -xvf
```

This options could also set for all the commands/scripts with "set" command, works the same for the bash options above.

## Shell initialization file
#### System-wide configuration files
* /etc/profile - here are usually set PATH, USER, MAIL and HOSTNAME.
* /etc/profile.d - directory, which contains files configuring system-wide behavior of specific programs.
* /etc/inputrc - the system-wide Readline initialization file where you can configure the command line bell-style.
* /etc/.bashrc - bash configuration file, that is run when system starts. (there is also this file for user in ~/.bashrc)

#### Individual user configuration files
* ~/.bash_profile - this is the preferred configuration file for user. In this file, users can add extra configuration options or change default settings.
* ~/.bash_login - file contains specific settings that are only executed when user log in to the system.
* ~/.profile - it can hold the same configurations, which are then also accessible by other shells.
* ~/.bashrc - it's keep user setting for all the shells so if you enter new shell window you do not need to set your settings again shell will get it from this file. (it's mostly important cause we do not use anymore login shell)
* ~/.bash_logout - contains specific information for logout procedure.

If any of the changes are made in above files either for individual user or system-wide configuration user have to reconnect to the system again or run a **source** to a changed file.

## Variables
#### Global variables - environmental variables (available in all shells).
To show them run: env
#### Local variables - available in the current shell only.
To show them run: set

#### Creating variable
```bash
VARNAME="value" # set variable (remember to not add spaces around equal sign)
unset VARNAME  # unset variable
export VARNAME # To make variable visible in others child shells (or run export VARNAME="value" to set new value)
```
Putting spaces (in declaration) between equal sign will cause errors. It's good habit to quote content strings when assigning values to variables.

#### Special parameters
```bash
$$ # print process ID of the shell.
$! # print process ID of the most recent background process.
$? # print last command or app exit status.
$0 # print name of the shell or shell script. 
$@ # list of all parameters
$* # all parameters returned as a string
$# # number of parameters
```

#### Quoting characters and not only
```bash
echo /$date # / => ignore $ character just printing everything like it is
echo '$date' # do not recognize variable
echo "$date" # print date variable content
echo `date` # command substitution, it's execute the command date and place it value in this place.

num1=12
num2=11
echo $((num1 + num2)) # expression quotes
echo $(($num1 + $num2)) # same result
echo $[num1 + num2] # old deprecated syntax

echo ${#num1} # get size of num1 as string
```

* Try always place variable in " " in condition to deny errors from not defined variables
```bash
[ $i == 12 ] # wrong
[ "$i" == 12] # good
```

#### Arithmetic operators (like most in C)
[operators](bash_scripts/operators.sh)
```bash
$((EXPRESSION)) # this expression is treated like it were within double quotes.
$[EXPRESSION] # this will only calculate the result without any tests. (deprecated)

var=10
var=$[var+10] # var is now 20
let "var=var+1" # var is now 21
var=$((var+10)) #var is now 31
```
[calculator](bash_scripts/calculator.sh)
[args sum](bash_scripts/args_sum.sh)

## GNU awk
Basic function of awk is to search files for lines or other text units containing one or more patterns. When a line matches one of the patterns, special actions are performed on that line.

Scheme: **awk PROGRAM_INSTRUCTION inputfile(s)**

#### Printing selected field.
When awk reads a line of a file, it divides the line in fields based on the specified input field separator, FS, which is an awk variable. The variables are $1, $2, $3 ..., $N. It's holds values of columns.
```bash
ls -la | awk '{ print $1, $9 }' # print permission and file name (\t tab character)
ls -ldh * | awk '{ print "Size is " $5 " bytes for " $9 }' # printing size of files
```

#### Regular expressions with awk
awk '/EXPRESSION/ {PROGRAM}' file(s)
```bash
df -h | awk '/dev\/hd/ { print $6 "\t: " $5 }'
```

#### Special patterns
BEGIN - It's run before a program. It could be use to introduce the program.
END - It's run after a program.
```bash
ls /etc -l | awk 'BEGIN { print "Files found:\n" } /.*.conf$/ { print $9 } END { print "It is all files" }' 
```

#### Separators (FS) for input
```bash
awk 'BEGIN { FS=":" } { print $1 "\t" $5 }' /etc/passwd
```

#### Separators (OFS - between variables with  ,) (ORS after a line)
```bash
awk 'BEGIN { FS=":"; OFS="\t"; ORS="\n--------------\n" } { print $1, $5 }' /etc/passwd
```

#### User variables
```bash
ls -l | awk '{ total+=$2 } END { print total }'
```

## If statement
Syntax: **if TEST-COMMANDS; then CONSEQUENT-COMMANDS; fi**

The TEST-COMMAND list is executed, and if its return status is zero, the CONSEQUENT-COMMANDS list is executed. The return status is the exit status of the last command executed, or zero if no condition tested true.

#### Comparing options
String comparing
![string compare](img/string_compare_if.png)

![combining expressions](img/combining_expressions_if.png)

![logic expressions](img/logic_expressions_if.png)

* -o - or => [ condition -o condition ]
* -a - and => [ condition -a condition ]

* -f x - check if x is a regular file
* -d x - check if x is a directory
* -e x - check if x exist

#### in this way could be done arithmetic comparing
```bash
if (( ($1 >= 35) && ($1 <= 70) )); then
    echo "You're and adult and you could be a president."
elif (( $1 >= 18 )); then
    echo "You're and adult."
else
    echo "You're not adult."
fi
```
Other example:
```bash
if [ "$1" -ge 35 -a "$1" -le 70 ]
then
    echo "You're and adult and you could be a president."
elif [ "$1" -ge 18 ]
then
    echo "You're and adult."
else
    echo "You're not adult."
fi
```


#### Usage examples
below example in file: [check_vi_op.sh](bash_scripts/check_vi_op.sh)
```bash
if [ -o vi ]; then echo "vi option is set"; fi
```

checking exit status:
```bash
$ grep $USER /etc/passwd
$ if [ $? -eq 0 ]; then echo "user exists"; fi
```

checking if you're root:
```bash
$ if [ "$(whoami)" != 'root' ]; then echo "you're not the root"; else echo "You're root"; fi

# shorten bash version
$ [ "$(whoami)" != 'root' ] && ( echo "you are using a non-privileged account" ) || ( echo "you're a root" )
# "&&" what to do if test goes true
```

if/else:
```bash
gender="male"; if [[ "$gender" == f* ]]; then echo "You're female"; else echo "You're male"; fi;
```

#### Command line arguments
To read arguments use just $0, $1 ... $N, where $0 is a file name.
[script example](bash_scripts/command_line_arg.sh)


#### if/elif/else:
Remember to after elif statement add then.
[if/elif/else example](bash_scripts/if_elif_else.sh)

#### Case statement
syntax: **case EXPRESSION in CASE1) COMMAND-LIST;; CASE2) COMMAND-LIST;; ... CASEN) COMMAND-LIST;; esac**

```bash
#!/bin/bash

case "$1" in
        start)
            echo start
            ;;
         
        stop)
            echo stop
            ;;
         
        status)
            echo status
            ;;
        res*)
            echo stop
            echo start
            ;;
        # regex could be used in case statement
        1[1-8]) # numbers start with 1x (x should be number from 1 to 8)
			echo $1
			;;
        *) # default
            echo "Usage: $0 {start|stop|restart|11 - 18|status}"
            exit 1
esac
```

## Interactive scripts
#### echo command
echo - build-in command outputs its arguments, separated by spaces and terminated with new a newline character. The return status is always zero echo takes a couple of options: 
 * -e - interpret backslash-escaped characters.
 * -n - suppresses the trailing newline.

Some of the escape sequences used by echo command:
* \b - backspace character
* \e - escape character
* \n - new line
* \t - horizontal tab
* \v - vertical tab
* \\\ - back slash

#### read build-in command
syntax: read [options] NAME1 NAME2 ... NAMEN
One line is read from standard input. First word is assigned to NAME1, second to NAME2 and so on. If there is no other variables and there is more words all of them will be in last variable. If no variables is supplied, the line is assigned to REPLY. All variables specified by read are local variables.

Options:
* read -d DELIM => the first character of DELIM is used to terminate the input line, rather that newline.
* read -e => **readline** is used to obtain the line.
* read -n N => read return after reading N characters rather then waiting for complete line of input.
* read -p PROMPT => display PROMPT, without a trailing newline before reading anything from input. It will not be added to variable.
* read -s => Silent mode. If input is coming from terminal, characters are not echoed.
* read -u x => Read input from file descriptor x.

Example: [command_info.sh](bash_scripts/command_info.sh)

#### Redirecting and file descriptors
File descriptors - stores information for what resource/file has been opened by process (e.g stdin (input), stdout(output), stderr(error)).

/proc/[PID]/fd - This is a subdirectory containing one entry for each file which the process has open, named by  its  file  descriptor,  and which is a symbolic link to the actual file (inode table).

```bash
ls -l /dev/std* # to check file descriptors numbers
ls -l /proc/self/fd/[0-2] # to see to which device it's connect
```

When excuting a given command, the following steps are excuted, in order:
* If the standard output of a previous command is being piped to the standard input of the current command, then /proc/<current_process_ID>/fd/0 is updated to target the same anonymous pipe as /proc/<previous_process_ID/fd/1.

* If the standard output of the current command is being piped to the standard input of the next command, then /proc/<current_process_ID>/fd/1 is updated to target another anonymous pipe.

* Redirection for the current command is processed from left to right.

* Redirection "N>&M" or "N<&M" after a command has the effect of creating or updating the symbolic link /proc/self/fd/N with the same target as the symbolic link /proc/self/fd/M.

* The redirections "N> file" and "N< file" have the effect of creating or updating the symbolic link /proc/self/fd/N with the target file.

* File descriptor closure "N>&-" has the effect of deleting the symbolic link /proc/self/fd/N.

* Only now is the current command executed.

To redirect both standard input and error to a file use: command &>FILE (same as: command > FILE 2>&1)

Redirect stdout and stderr to different files:
```bash
$ command > output 2> errors
```

## For loop
syntax: **for NAME [in LIST ]; do COMMANDS; done**

* The return status is the exit status of last command executed
* NAME could be any variable name

Example:
```bash
for i in `ls`; do echo "$i: " file "$i"; echo; done # print file name and file information from ls command

for i in *.tmp; do rm -r $i; done # delete all files with that end with .tmp

for i in 1 2 3; do echo $i; done # printing 1 2 3

for i in `seq 1 3`; do echo $i; done # printing 1 2 3
```

[compare multiple files](bash_scripts/compare_multiple_files.sh)

## While loop
syntax: **while CONTROL-COMMAND; do CONSEQUENT-COMMANDS; done**
```bash
while true; do echo "I'm infinite" done # infinite example

# here condition could also be: [ "$#" != "0" ]
while [ -n "$1" ]; do echo $1; shift; done #print all arguments one by one
```

[example](bash_scripts/while_example.sh)

## Break and continue
Break cancel loop and continue the loop from new iteration.
[break_continue example](bash_scripts/break_continue.sh)


## Select
syntax: **select WORD [in LIST]; do RESPECTIVE-COMMANDS; done**
* [make private example](bash_scripts/make_private.sh)
* [run script](bash_scripts/run_script.sh)


## Variables types
To decide which type our variable should get we can use a **declare** statement.
syntax: **declare OPTION(s) VARIABLE=value**

Options:
![variable options](img/variable_options.png)

Important to notice that Bash has an option to declare a numeric value, but none for declaring string values. This is because, by default, if no specifications are given, a variable can hold any type of data. As soon as you restrict assignment of values to a variable, it can only hold that type of data.

```bash
declare -i integer=1 # integer variable
declare -p integer # display attribute and value of integer variable
```

#### Constant - readonly
Readonly let you create a constant.

syntax: **readonly OPTION VARIABLES(s)**
```bash
readonly AGE=21
AGE=123 # this will cause a error
```

#### Arrays
* Indirect declaration: **ARRNAME[INDEXNR]=value**
* Explicit declaration by using **declare**: declare -a ARRAYNAME
* By using compound assignments: ARRAYNAME=(val1 val2 ... valN)
* Array can hold only one type of data

Adding or editing records of array are done by: ARRAYNAME[INDEXNR]=value

```bash
PETS=(CAT DOG LION) # DECLARING ARRAY
#PETS=() # Declaring an empty array

echo ${PETS[*]} # printing all record from array
echo ${PETS[1]} # printing "DOG"
PETS[3]=MOUSE # adding new record to array

echo ${#PETS[*]} #print array size
```

To delete array use command **unset** like in example.
```bash
unset ARRAY[1] # to delete a single record with index 1
unset ARRAY # to delete the whole array
```

#### Operations on variables
* To count variable(string) or array length use: **${#VARNAME}**
* If you want to get default value when VARNAME is not set use: **${VARNAME:-value}**
* If you want to set value when VARNAME is not set use: **${VARNAME:=value}**

Some bash examples usage:
```bash
echo ${#HOME} # count HOME var string length
echo ${TEST:-test} # if TEST is not defined or null expression will return test otherwise $TEST value.
echo ${TEST:=test} # if TEST is not set it will be set with test
echo ${TEST:?test} # if TEST is not set test will be printed and script will exit
```

* Getting substring: **${VARNAME:OFFSET:LENGTH}** (It's not change original var)
  * OFFSET - from where start getting substring
  * LENGTH - How much of string returns (if we don't give this value it will get the rest of string)
* Getting substring with: **${VARNAME:WORD}** (delete first matching patter WORD, but do not change original string)
```bash
export ALPHABET="ABCDEFGHIJKLM"
echo ${ALPHABET:3:4} # OUTPUT: DEFG
echo ${ALPHABET#ABCD} # OUTPUT: EFGHIJKLM

# This also works for arrays
NUMBERS=(One Two Three Four Five)
echo ${NUMBERS[*]##O*} # Two Three Four Five (remove matching characters)
echo ${NUMBERS[*]#O*} # ne Two Three Four Five (remove matching elements)
echo ${NUMBERS[*]:1:3} # Two Three Four (getting part of array)
```

* replace first match PATTERN with STRING: **${VAR/PATTERN/STRING}**
* replace all matching PATTERNS with STRING: **${VAR//PATTERN/STRING}**
```bash
export ALPHABET="ABCDEFGHIJKLM"
ALPHABET=${ALPHABET/GHI/ABC} # SET ALPHABET to: ABCDEFABCJKLM
echo ${ALPHABET/ABC/X} # OUTPUT: XDEFXJKLM
```

## Functions
To define a function use:
* **function FUNCTIONNAME { COMMANDS; }**
* **FUNCTIONNAME() { COMMANDS; }**

```bash
$ cat script.sh
#!/bin/bash
# usage: ./script.sh somevalue
echo "Argument for script is: $1."

test ()
{
    echo "Reading first argument $1."
    RETVALUE=$?
    echo "Exit code of function is $RETVALUE"
}

test other_param # usage of function in script
```

## Signals
Signals are really important for process, cause they can determine the process actions. (e.g close, stop ... process). They could be send via shortcuts or kill command.

For example SIGHUP by default exits a shell. An interactive shell will send a SIGHUP to all jobs, running or stopped; 

To see all possible signals name run: kill -l
To read more about signals run: man 7 signal

Signals that can be used in bash shell:
![shell signals](img/shell_signals.png)

#### Usage of signals with kill:
![kill signals](img/kill_signals.png)

Kill command send the TERM signal if none is given.
```bash
kill -9 1234 # send kill signal to process with ID 1234
```

## Trap
Term let you disable for user exiting with specific signals:

Syntax: trap [COMMANDS] [SIGNALS]
* SIGNALS - signals to be ignored
But still commands like kill cannot be disabled.
```bash
#!/bin/bash
# traptest.sh

trap "echo Booh!" SIGINT SIGTERM SIGTSTP
echo "pid is $$"

while :			# same as "while true".
do
    sleep 60 # sleep for 60 seconds
done
```

To kill process use: kill -9 [pid]

Some process might ignore signals. The only signal that can't be ignore is KILL signal.

## Common shell features
![common shell features](img/common_shell_features.png)


## Bash script examples
[check if user can read files in directory](bash_scripts/check_if_user_can_read_files_in_directory.sh)

[change in directory filenames from e.g aAbB to AaBb](bash_scripts/change_uper_lower_letters_files.sh)

[change extensions of a files in directory](bash_scripts/change_extension_in_directory.sh)

[better ps](bash_scripts/better_ps.sh)