## Regular Expression (regex)

### Meta sign and theirs usage:
#### . - match a single sign
  * bash == bas. (true)

#### ^ - match not a sing but the place before first sign/s
 * ^bash == bash
 * ^bash != special bash
 * '^\.[gd]' - match all string started with . and has latter letter g or d (e.g .gdb, .directory)

#### $ - match not a sing but the place before last sing/s
* bash == sh$ - true - checking if line ends with sh
* bash == as$ - false - checking if line ends with as but it's end with sh

#### [] - list of signs that can fit in word
* Matching last sing that is in bracket: bash == bas[hx] && basx == bas[hx]
* Getting false if last is one of in bracket: bash != bas[^hx] && basx != bas[^hx]
* Checking signs in range:
  * 1[1 - 5] - true for 11 ..., 15  (false for others)
  * 0x[0-9a-f] - true for all 0x0 - 0xf
* Character classes
![POSIX CHARACTER CLASSES](img/POSIX_character_classes.png)
Example: 
  * 0x[^[:digit:]] == 0x[^0-9]

* Important to notice in ASCII control characters are from 0x00 to 0x1f

#### \<...\> - find sentence that are between first and last words also including them. But cannot be part of word
```bash
'bash is cool' == '\<is\>' #match sentence (true)
'bash is cool' == '\<co..\>' #match sentence (true)
```

#### ? - mean last element before the ? sign in the pattern is optional (0 or 1 sign)
```bash
'bashx' == 'bash?' #after a x one sign is optional (true)
'bash' == 'bash?' #and here we do not have sign (true)
```

#### * - element before a * sign may not appear or appear multiple times (0 or more times)
```bash
'basssh' == 'bas*h' #s appears multiple times (true)
'bah' == 'bas*h' #s do not appear (true)
```

#### + - element before a + sign may not appear or appear multiple times (1 or more times)
```bash
'basssh' == 'bas+h' #s appears multiple times (true)
'bah' == 'bas+h' #s do not appear (false)
'bash' == 'bas+h' #s do not appear (true)
```

#### {n} - previous element in the pattern appears exactly n times
```bash
'12345' == '[[:digit:]]{5}' #digit appears at least 5 times (true)
'aaa' == 'a{3}' # (true)
```

#### {n, m} - previous element in the pattern appears at least n times and less or equal m
```bash
'12345' == '[[:digit:]]{4,5}' #(true)
```

#### {n, } - previous element in the pattern appears at least n times
```bash
'123' == '[[:digit:]]{4,}' #(false)
```

#### | - alternative operator - element on the left or right of the | sign have to exist in text
```bash
'0xEF' == '0xA|B|C|D|E|F[A-F]' #(true) cause after 0x must be either A, B, C, D, E, F and second from A to F
```

Examples:
```bash
'^*[^0-9]*$' #Lines that do not contains number
'\<[0-9]{4}-[0-9]{2}-[0-9]{2}\>' #format ISO 8601

```

To use metasign as literal use it with (single) \ before a sing: \\[]

Special signs:
 * \n - end of line
 * \r - return sign
 * \t - tab sign
 * \e - escape char

Special class of signs for regex:
 * \s - whitespace
 * \S - not whitespace sign
 * \w - sign in the word (letter, number, ...)
 * \W - sign that is not in word
 * \b - place on border in the word