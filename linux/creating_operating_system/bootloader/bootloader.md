# Bootloader

When you turn on the computer the first one it does, it looks for operating system in the boot sector. The boot sector it usually the first sector of a bootable drive. It's usually 512 bytes long binary code that ends with 0x55 0xAA bytes.

## The first program
### 512 binary code that will end with 0x55AA (boot.asm)

```asm

jmp $ ;jump to the current line (make system infinity)

times 510 - ($ - $$) db 0 ;this will fill the rest of empty bootloader space with 0.
                          ;$$ - address of the section
                          ;$ - address of current instruction 


dw 0xaa55 ;store the end of the first boot sector mark
```


### To compile and run binary file

```bash
$ nasm -f bin boot.asm -o boot.bin
$ qemu-system-x86_64 boot.bin
```

To exit qemu press: Ctrl-Alt-2 and write `quit` -> Enter.

## Bootloader memory layout

![memory layout](images/boot-memory-layout.png)


## BIOS - basic input/output system

BIOS calls examples: https://riptutorial.com/x86/example/23463/bios-calls

[printing loop](print_loop.asm)

### Reading and writing on screen

[full file](example/bootloader.s)


### Using stack

[stack example](example/reading_chars_stack.asm)

### Segments

In real mode there is only 16 bit addressing, which lets to access address
only up to 2^16 memory locations = 65536 bytes = 64 KiB.

To unlock larger part of the memory there is something created a segmentation. Segmentation divides memory into segments each up to 64 KiB.

The segments we mostly care about are data, code and stack segments and each of this segments have appropriate register:

 - data - ds
 - code - cs
 - stack - ss

To represent for example memory segment of data (ds):
 - example ph. address: 75200 
   75200 = 16 * ds + offset = ds : offset



References:
 - https://en.wikipedia.org/wiki/Bootloader
 - https://de-engineer.github.io/Understanding-booting-process-and-writing-own-os/
 - https://raw.githubusercontent.com/tuhdo/os01/master/Operating_Systems_From_0_to_1.pdf