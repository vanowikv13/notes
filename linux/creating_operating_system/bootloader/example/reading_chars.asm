;[org 0x7c00] ; Directive hint to set base address equal to 0x7c00.

mov bx, buffer
mov cx, 0

reading:
xor ah, ah
int 0x16 ; Keyboard related handler.
mov [bx], al

cmp al, 13 ; Check if enter is pressed
je printing
inc bx

cmp cx, 10 ; Check if buffer_size is reached
je printing
inc cx

jmp reading

printing:
mov bx, buffer
mov ah, 0x0e ; Write character to screen.

printing_loop:
mov al, [bx]
cmp al, 0
je exit
cmp al, 13
je exit
int 0x10
inc bx
jmp printing_loop

buffer_size equ 10
buffer:
    times buffer_size db 0

exit:
jmp $
times 510-($-$$) db 0
db 0x55, 0xaa