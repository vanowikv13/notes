mov ah, 0x0e
mov al, 'A'

label:
    ; Check condition and exit if al == 'Z' + 1
    cmp al, 'Z' + 1
    je exit

    ; make interrupt and increment char
    int 0x10
    inc al

    ; loop again
    jmp label

exit:
jmp $
times 510-($-$$) db 0
db 0x55, 0xaa
