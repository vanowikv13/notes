[org 0x7c00] ; Directive hint to set base address equal to 0x7c00.

mov bp, 0xffff ; Set to address that is not part of memory layout.
               ; This is needed to not override by accident some important memory region.
mov sp, bp ; Copy 0xffff from bp to sp.
cli ; Clear interupt flag.

call set_video_mode
call get_char_input

hlt ; Halt the system until next extern interupt is fired.

set_video_mode:
    ; https://en.wikipedia.org/wiki/INT_10H
	mov ah, 0x00 ; Set video mode.
	mov al, 0x03 ; Set text mode.
	int 0x10 ; Screen/console related handler.
	ret

get_char_input:
    ; https://en.wikipedia.org/wiki/INT_16H
	xor ah, ah ; Set ah to 0x00 = Read key press.
	int 0x16 ; Keyboard related handler.

	mov ah, 0x0e ; Write character to screen.
	int 0x10 ; Screen/console related handler.

	jmp get_char_input

; Bootloader size should be 512 bytes.
times 510-($-$$) db 0		    ; 510 this will fill the rest of empty bootloader space with 0
								; to keep it size exactly at 512 bytes.
dw 0xaa55 						; The bootloader magic number (2 bytes).

