;[org 0x7c00] ; Directive hint to set base address equal to 0x7c00.
bits 16


;mov bp, 0x8000 ;example: stack position of the base
;mov sp, bp

readstr:
xor ah, ah
int 0x16 ; Keyboard related handler.

cmp al, 0x0D ; Check if enter is pressed
je printstr
push ax

jmp readstr

printstr:
mov ah, 0x0e ; Write character to screen.

printing_loop:
pop ax
cmp al, 0
je exit
cmp al, 13
je exit
mov ah, 0x0e
int 0x10
jmp printing_loop

exit:
jmp $
times 510-($-$$) db 0
db 0x55, 0xaa