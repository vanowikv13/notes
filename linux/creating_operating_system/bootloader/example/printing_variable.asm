[org 0x7c00] ; Directive hint to set base address equal to 0x7c00.

mov bx, variableName
mov ah, 0x0e

loop:
mov al, [bx]
cmp al, 0
je exit
int 0x10
inc bx
jmp loop


variableName:
    db "Some random string", 0

exit:
jmp $
times 510-($-$$) db 0
db 0x55, 0xaa