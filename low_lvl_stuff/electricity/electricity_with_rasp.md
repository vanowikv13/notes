# Electricity with Rasp

### Pins
![pins](img/pins.png)

On the board there're two 5V pins and two 3.3V pins, there are also 8 ground pins 0V, all of them are unconfigurable. Rest of them are configurable pins on general purpose and output of them is set to 3V3 and input is 3V3 tolerant. Maximum current draw of raspberry pi gpio pin is 16mA up to 50mA with 3.3V power supply pin.

Output of gpio pins can be either a 3V3 (high) or 0V (low)

Input of gpio pins can be read as 3V3 (high) or 0V (low).

It's important that when using components to check hom much voltage they can get. And If our raspberry is giving to much power use special resistor.

### Basic electricity knowledge
* Electric current (I) - is an amount of stream of charges moving towards ground per time, it's not itself flow of energy. Usually described as letter I, measured in A(mper) which actually measure the rate of flow at specific point.

    1A = 1000mA

* Voltage (V) - electric potential difference, electric pressure - measured between two points, between positive and negative end of battery and between a wire and a ground. Represented by V(olt). It's whats make electric charges move and since moving charges is current, voltage can cause a current but it's not itself flow or transfer of energy.

    1V = 1000mV

* Resistance (Ω) - Explains how difficult it is to transfer electrical charges. Here electricity energy is transformed into heat.

In simple words all of this: Voltage is a force that moves electric charge. Current is the velocity of electric charge. Resistance is how easy it is to move the supports.

### Ohm's law
I = V / R

### How to specify resistor for element?
To make this we need to use a Ohm's law:
R = V / I

V = (Power supply voltage) - (Forward drop voltage)
I = Max Continuous forward current
R = Resistance we want to get

Now fully it's look:
```
((Power supply voltage) - (Forward drop voltage)) / (Max Continuous forward current) = resistor value in Ohms
```

Example with LED
```
LED Spec:
 1. LED voltage drop: 1.7V
 2. Max Continuous Forward Current: 25mA - 30mA

Pi spec:
 1. Voltage from pin: 3.3V

* Remember it's always safer to lower a little Max current of LED lamp

Solve:
V = 3.3V - 1.7V = 1.6V
I = 25mA
R = V / I = 1.6V / 25mA = 1.6V / 0.025A = 64Ω

For LED lamp we should use resistor with resistance more or equal then 64Ω.
```

To make it with multiple LED in parallel - the electrical current may flow along multiple paths before returning to the power source. The voltage is the same across all LEDs but the current is divided between all of LEDs.

Formula:
```
((Power supply voltage) - (Forward drop voltage)) / (Max Continuous forward current) = resistor value in Ohms
```

With multiple LED formula for resistance doesn't change, but we need to know if our power supply can transfer enough power. For 2 LEDs it's twice much and so on.


To make it with multiple LED strung together (series) - In a series circuit, the same amount of current from the power source flows through each LEDs. The Voltage in a series circuit is divided up across all of the LEDs. If one of the LED will be not able to flow current the rest will also stop working.

Formula:
```
((Power supply voltage) - (Forward drop voltage * Number of LEDs)) / (Max Continuous forward current) = resistor value in Ohms
```
Example:
```
LED Spec:
 1. LED voltage drop: 1.6V
 2. Max Continuous Forward Current: 25mA - 30mA

2 LED spec:
 1. Voltage: 1.6V * 2 = 3.2V

Pi spec:
 1. Voltage from pin: 3.3V

Solve:
 V = 3.3V - 3.2V = 0.1V
 I = 25mA
 R = V / I = 0.1V / 25mA = 0.1V / 0.025A = 4Ω
```

### Power supply transferring from AC to DC
* AC - Altering current - this kind of current is supplied to our homes and everywhere else. And it's voltage is changing over time in sinusoidal wave shape.
* DC - Direct current - this type of current do not varies over time, but stays on one level. In computer voltage of DC power could represent for example on 2.5V logical 1 and 0.0V logical 0.
* AC/DC Power supply - converting from AC to DC
![](img/power_supply.png)


### Reference
* https://www.raspberrypi.org/documentation/usage/gpio/
* https://en.wikipedia.org/wiki/Electric_current
* https://simple.wikipedia.org/wiki/Voltage
* https://www.evilmadscientist.com/2012/resistors-for-LEDs/
* https://simple.wikipedia.org/wiki/Series_and_parallel_circuits#:~:text=In%20a%20series%20circuit%2C%20the,flow%20around%20a%20single%20path.&text=In%20a%20series%20circuit%2C%20the%20same%20amount%20of%20amperage%20from,across%20all%20of%20the%20loads.
* http://bob.cs.sonoma.edu/IntroCompOrg-RPi/sec-elec.html