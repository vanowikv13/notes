# Low lvl theory

## Binary numbers

When we have binary number like 10110110

- first bit is called MSB (most significant bit)
- the last is called LSB (less significant bit)

Binary addition:

- 0 + 0 = 0
- 0 + 1 = 1
- 1 + 0 = 1
- 1 + 1 = 10

If number is not unsigned first bit of a number is a sign bit
1101 - negative - (-1)*2^3 + 2^2 + 2^0 = -8 + 4 = -4
0101 - positive - 2^2 + 2^0 = 5

To reverse a number (e.g from 1 to -1 or from -10 to 10):
- our num: 0001 => 1

1. Reverse the bits, out num: 1110 => -2
2. Add one to num: our num: 1110 + 0001 => 1111 => -1

Subtraction:
![](sub.png)

### Floating point numbers in binary

![](img/fpu_from_bin_to_dec.jpg)

![](img/fpu_from_dec_to_bin.jpg)

## Computer architecture

### Von-Neumann vs Harvard architecture

![](img/Von_Neumann_vs_Harvard.png)

### Microcomputer

![microcomputer](img/microcomputer.png)
- The clock synchronizes the internal operations of the CPU with other system components.
- The control unit (CU) coordinates the sequencing of steps involved in executing machine
instructions.
- The arithmetic logic unit (ALU) performs arithmetic operations such as addition and subtraction and logical operations such as AND, OR, and NOT. (ALU can make only one call to memory per instruction)
- Memory storage unit is where instructions and data are hold.
- A bus is a wires to transfer data from one part of the computer to another.
- Data bus - transfers instructions and data between CPU and memory.
- The I/O bus transfers data between the CPU and the system input/output devices.
- The control bus synchronize actions of all devices attached to the system bus.
- Clock each operation between CPU and the system bus is synchronized by an internal clock pulsing at constant rate. The basic unit of the time is machine cycle (clock cycle) measured in Hz

    ![clock_cycle](img/clock_cycle.png)

### Instruction execution cycle

1. First, the CPU has to fetch the instruction from an area of memory called the instruction
queue. Right after doing this, it increments the instruction pointer.
2. Next, the CPU decodes the instruction by looking at its binary bit pattern. This bit pattern
might reveal that the instruction has operands (input values).
3. If operands are involved, the CPU fetches the operands from registers and memory. Sometimes, this involves address calculations.
4. Next, the CPU executes the instruction, using any operand values it fetched during the earlier
step. It also updates a few status flags, such as Zero, Carry, and Overflow.
5. Finally, if an output operand was part of the instruction, the CPU stores the result of its execution in the operand.

We usually simplify this complicated-sounding process to three basic steps: Fetch, Decode, and Execute. (each step takes one clock cycle)

### Short summary definition

The central processor unit (CPU) is where calculations and logic processing occur. It contains a limited number of storage locations called registers, a high-frequency clock to synchronize its operations, a control unit, and an arithmetic logic unit. The memory storage unit is where instructions and data are held while a computer program is running. A bus is a series of parallel wires that transmit data among various parts of the computer. The execution of a single machine instruction can be divided into a sequence of individual operations called the instruction execution cycle. The three primary operations are fetch, decode, and execute. Each step in the instruction cycle takes at least one tick of the system clock, called a clock cycle. The load and execute sequence describes how a program is located by the operating system, loaded into memory, and executed by the operating system. x86 processors have three basic modes of operation: protected mode, real-address mode, and system management mode. In addition, virtual-8086 mode is a special case of protected mode. Intel64 processors have two basic modes of operation: compatibility mode and 64-bit mode. In compatibility mode they can run 16-bit and 32-bit applications.
Registers are named locations within the CPU that can be accessed much more quickly than
conventional memory. Following are brief descriptions of register types:
• The general-purpose registers are primarily used for arithmetic, data movement, and logical
operations.
• The segment registers are used as base locations for preassigned memory areas called
segments.
• The instruction pointer register contains the address of the next instruction to be executed.
• The flags register consists of individual binary bits that control the operation of the CPU and reflect the outcome of ALU operations. The x86 has a floating-point unit (FPU) expressly used for the execution of high-speed floatingpoint instructions. The heart of any microcomputer is its motherboard, holding the computer’s CPU, supporting processors, main memory, input–output connectors, power supply connectors, and expansion slots. The PCI (Peripheral Component Interconnect) bus provides a convenient upgrade path for Pentium processors. Most motherboards contain an integrated set of several microprocessors and controllers, called a chipset. The chipset largely determines the capabilities of the computer. Several basic types of memory are used in PCs: ROM, EPROM, Dynamic RAM (DRAM), Static RAM (SRAM), Video RAM (VRAM), and CMOS RAM. Input–output is accomplished via different access levels, similar to the virtual machine concept. Library functions are at the highest level, and the operating system is at the next level below. The BIOS (basic input–output system) is a collection of functions that communicate directly with hardware devices. Programs can also directly access input–output devices.

## HEAP

It's place in memory for dynamic allocation, usually called heap or free store. In C, the library function malloc is used to allocate a block of memory on the heap and free deallocate allocated memory on heap.

## Integers in programming

Converting signed integer for example from 4 bytes to 8 bytes:
![](img/signed_integer_conversion.jpg)

In assembly to convert from one size integer to another use:

```assembly
movzx ... ; move with zero extend (for unsigned)
movsx ... ; move with sign-extend (for signed)
```

Border Integer values (unsigned):
![](img/max_integer_unsigned.jpg)

Border Integer values (signed):
![](img/max_integer_signed.jpg)

## Floating point numbers in programming

![floating points precisions](img/floating_points_precisions.jpg)
![special values floating points numbers](img/special_fpn_values.jpg)

## Little endian and big endian

This is how values are encoded in memory.
![little big endian](img/little_big_endian.jpg)

Another example:
string: ABCD (0x41424344) in little endian in memory will be: 0x44434241 in big endian nothing will change.

## DMA - direct memory access

It's a module, which has direct access to memory (RAM), because of that DMA can do some work for the CPU by transferring data between memory and peripheral. This feature is mostly used to now waste CPU time, because some peripherals transfer data much slower than the CPU, so a lot of time would be wasted just waiting. The CPU controls DMA and tells it from where to which place data should be saved.

Reference:
- <https://web.sonoma.edu/users/f/farahman/sonoma/courses/es310/310_arm/lectures/Chapter_3-and-1_ARM.pdf>
