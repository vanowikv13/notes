# BLE - Bluetooth Low Energy

To initiate a connection, a central device picks up the connectable advertising packets
from a peripheral and then sends a request to the peripheral to establish an exclusive
connection between the two devices. Once the connection is established, the peripheral
stops advertising and the two devices can begin exchanging data in both directions

Advertising and Scanning
BLE has only one packet format and two types of packets (advertising and data packets),
which simplifies the protocol stack implementation immensely. Advertising packets
serve two purposes:
• To broadcast data for applications that do not need the overhead of a full connection
establishment
• To discover slaves and to connect to them

Avertising Packet Types (APT)
```
APT              Con    Scan Directed GAP Name
ADV_IND          Yes    Yes  No       Connectable Undirected Advertising
ADV_DIRECT_IND   Yes    No   Yes      Connectable Directed Advertising
ADV_NONCONN_IND  No     No   No       Non-connectable Undirected Advertising
ADV_SCAN_IND     No     Yes  No       Scannable Undirected Advertising
```

Connections
To establish a connection, a master first starts scanning to look for advertisers that are
currently accepting connection requests. he advertising packets can be filtered by
Bluetooth Address or based in the advertising data itself. When a suitable advertising
slave is detected, the master sends a connection request packet to the slave and, provided
the slave responds, establishes a connection. The connection request packet includes
the frequency hop increment, which determines the hopping sequence that both the
master and the slave will follow during the lifetime of the connection.


The following three connection parameters are another set of key variables communi‐
cated by the master during the establishment of a connection:

* Connection interval - The time between the beginning of two consecutive connection events. This value
ranges from 7.5 ms (high throughput) to 4 s (lowest possible throughput but also
least power hungry).

* Slave latency - The number of connection events that a slave can choose to skip without risking a
disconnection.

* Connection supervision timeout - The maximum time between two received valid data packets before a connection
is considered lost


ATT - Attribute Protocol is a simple client/server stateless protocol based on at‐
tributes presented by a device. In BLE, each device is a client, a server, or both, irre‐
spective of whether it’s a master or slave. A client requests data from a server, and a
server sends data to clients.

Each server contains data organized in the form of attributes, each of which is assigned
a 16-bit attribute handle, a universally unique identifier (UUID), a set of permissions,
and finally, of course, a value. Theattribute handle is simply an identifier used to access
an attribute value. The UUID specifies the type and nature of the data contained in the
value.

When a client wants to read or write attribute values from or to a server, it issues a read
or write request to the server with the handle. The server will respond with the attribute
value or an acknowledgement. In the case of a read operation, it is up to the client to
parse the value and understand the data type based on the UUID of the attribute. On
the other hand, during a write operation, the client is expected to provide data that is
consistent with the attribute type and the server is free to reject the operation if that is
not the case.

### ATT Operations:

Error Response - sending response to the request with error.
Exchange MTU request/response - Exchange between client and server Maximum Transmission Units (MTU or maximum packet size)
Find information Request/Response. - Obtain a list of all attributes in a particular handle range.
Read operations - all kind of reads with request/response.
Write command - Write Request/response (write data and expect response form server)
    - Write command - write data without any acknowledgment or response.
    - Signed Write Command - Similar to "Write Command", but uses signature from ("Security Manager (SM)")



## GATT - Generic Attribute Profile

GATT establishes in detail how to exchange all profile and user data over a BLE connection.
In contrast with GAP, which define low-level interactions with devices, 
GATT deal only with actual data transfer procedures and formats.

All standard BLE pro‐
files are therefore based on GATT and must comply with it to operate correctly. This
makes GATT a key section of the BLE specification, because every single item of data
relevant to applications and users must be formatted, packed, and sent according to its
rules.

GATT uses the Attribute Protocol as its transport protocol to exchange data between devices. This data is organized hi‐
erarchically in sections called services, which group conceptually related pieces of user
data called characteristics. This determines many fundamental aspects of GATT dis‐
cussed in this chapter.

GATT Roles:
    - Client - corresponds to the ATT client. It sends requests to a server and receives responses (and server-initiated updates) from it. Client don't know anything about server attributes, so it must firstly performs service discovery. After performing discovery then it can then start reading and writhing attributes found in the server.
    - Serer - corresponds to the ATT server. It receives request from a client and sends responses back.

UUIDs - A universally unique identifier (UUID) is a 128-bit (16 bytes) number that is guaranteed (or has a high probability) to be globally unique. UUIDs. For efficiency, and because 16 bytes would take a large chunk of the 27-byte data payload length of the Link Layer, the BLE specification adds two additional UUID formats: 16-bit and 32-bit UUIDs. These shortened formats can be used only with UUIDs that are defined in the Bluetooth specification

### Attributes

Attributes are the smallest data entity defined by GATT (and ATT). They are address‐able pieces of information that can contain relevant user data (or metadata) about the structure and grouping of the different attributes contained within the server. Both GATT and ATT can work only with attributes, so for clients and servers to interact, all information must be organized in this form

#### Handle

The attribute handle is a unique 16-bit identifier for each attribute on a particular GATT server. Handles are used to identify the association of each Read, Write, Notification or Indication with a particular attribute. Attribute handles are the primary key identifiers for the data transferred


# BLE stack architecture

![BLE stack architecture](images/ble_stack.png)

## Link Layer

Responsible for advertising, scanning, maintaining and creating connection.

Link layer states:

1. Standby - no action is performed.
2. Advertising - necessary to be discovered or connectable.
3. Scanning - passive or active, the device is scanning for advertise package.
   1. Passive scanning only receives advertising packages.
   2. In active scanning scanner request to advertising devices to obtain some data.
4. Initialing - It sends auto-connection request (the device that sends request becomes a master) to the advertiser.
5. Connection - The device that sends connection request is a master and the one that receives request is slave.

Depend if device is master or slave it might loop over different types of states. For example slave is firstly advertising, while master is scanning and initiating connection.

Packets strucutre:

1. Advertising packages are one way and are usually understandable by all receiving devices.
2. Data packages are understandable by only two devices, master and a slave.

Link layer sends advertising packages and after advertising package
is sent, there is always some delay between between another advertising packages to let master during this time send connection request.

### L2CAP - Logical Link Control and Adaption Protocol

For Bluetooth Low Energy, the L2CAP layer is in charge or routing two main protocols:
the Attribute Protocol (ATT) and the Security Manager Protocol (SMP).


## Attribute Protocol

Defines how a client can find and access the attributes on a server.

Operations

1. Request
2. Response
3. Command
4. Indication
5. Confirmation
6. Notification

Attribute - peace of label of labeled addressable data

1. Label - UUID
2. Handle, type and value.
   1. Handle - uniquely identify attribute on the server, allowing the client to reference it.
   2. Type - specifies what attributes represents, defined by UUID.
   3. Value - is the state data, the actual data.
3. Permissions: like READ and/or WRITE


## GATT - Generic Attribute Profile

Defines a hierarchical grouping of attributes and procedures for discovery and access.

Nice links:
 - https://learn.adafruit.com/introduction-to-bluetooth-low-energy/gatt
 - https://www.youtube.com/watch?v=BZwOrQ6zkzE
 - https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/