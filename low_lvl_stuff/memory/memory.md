# How memory is working in programs x64

Usually when we create a process it has an access only to virtual memory but not the global one. To global one has access only kernel.

Everything written in memory is in usually in little-endian format so it means that bytes are reversed.


## Basic data sections
* .data - initialized global data section, mostly used in C for initialized globals and static with values.
* .bss - not initialized global data section, guaranteed to be all 0 values. Used for uninitialized globals and static variables.
* .rodata - read only global data section

When we compile below code and run in the e.g IDA
```cpp
#include <cstdio>

//.data section
int global_var = 0x41414141;

//.bss
int not_initialized_global;

//.rodata section
const int global_const = 0x42424242; 

// placed at .bss from the begin and then func() is called from the:
// _start->__libc_start_main... ->__static_initialization_and_destruction_0
int global_run_time = func(); // not available for C leads to compiler error: function call is not allowed in a constant expression

int main() {
    //all the static variables are global variables (lifetime of static var is lifetime of a program)
    //static variables works similar to globals on where they're placed in memory
    //.data section, cause we allocate it if we do not allocate it then we will have .bss
    static int static_arr[0x10] = {0x43434343};
    
    //.rodata section
    puts("Some text");
}
```

On clicking Ctrl + S IDA will open all the sections.
From IDA we can check sections that are written in the comments.

## Heap - dynamic allocation
System allocate for the program blocks of data always 4KB. Even if we need one byte system will give use 4KB block. Functions:
* VirtualAlloc (windows)
* mmap (linux)

Then function like malloc and other works with parts of this blocks and give pointer to free space when needed.

## Memory block on linux:
Memory address block of a single process:
```bash
cat /proc/pid/maps
```

Memory io block of a system:
```bash
sudo cat /proc/iomem
```

#### Terminology
Stackcrash - happen when heap start allocating data on stack frame.