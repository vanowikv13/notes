# ARM assembly

## Types for 32-bit

* Byte - 8 bits
* Halfword - 16 bits
* Word - 32 bits
* Doubleword - 64 bits

C/C++ types:

* int, long - word
* short - halfword
* char - byte
* void* - word
* bool - byte
* float - word
* double - doubleword

## Types for 64-bit

64-bit also have all 32-bit types but add up some:

* Quadword - 128 bits

types for C/C++ are also the same with one difference:

* pointers - doubleword

## ARM core Architecture 32-bit

![arm_architecture](img/arm_architecture.png)

Instruction pipes - holds instructions to be executed.

The ARM has a pipeline technique for instructions. It means we can process one stage of the second instruction while processing the other stage of the first instruction. For example, in the first instruction, we're in process of decoding so we can for the second instruction start fetching. Each instruction is done in a single clock cycle.

![fetch_decode_execute](img/fetch_decode_execute.png)

## CPU cache

It's internal CPU memory to speed-up some memory operations. It's achieved by loading memory pages from RAM/flash etc. into CPU cache when they're accessed by the code, then those values stays in cache to use it latter.

## Execute operations could go two ways a register-register or register-immediate, immediate is in example below a value #1.

register register:

```assembly
ADD R1, R2, R3 ;write to R1 sum of R2 and R3
```

register immediate:

```assembly
ADD R1, R2, #1 ;write to R1 sum of R2 and 1
```

![](img/execute.png)

## Registers

AARCH32 have 16 32-bits registers:

* r0 - r10 - general purpose registers
* r11 (fp) - frame pointer (ebp)
* r12 (ip) - intra procedural call
* r13 (sp) - stack pointer (esp)
* r14 (lr) - link register (next instruction after end of function call)
* r15 (pc) - program counter (eip)

* r0 like eax or rax is return value of function.
* r0 - r3 - usually are used as arguments for functions. If function have more arguments than 4 then rest of them are placed on stack. First argument is r0 and so on. On stack arguments are placed ordered right under frame pointer.

AARCH64 have 31 64-bits registers:

* r0 - r30 - general purpose registers
* x0 - x30 - same general purpose registers but in 64-bit
* w0 - w30 - like above but 32-bit part of register
* x30 register is used as link register
* sp (31 register) - stack pointer

CPSR - 32-bit flags register - The Current Program Status Register:

* N - bit 31 - Negative flag
* Z - bit 30 - Zero flag
* C - bit 29 - Carry flag
* V - bit 28 - Overflow flag
* ...

## Instruction execution cycle

![insrtuction_execution_cycle](img/insrtuction_execution_cycle.png)

wfi - wait for interrupt - instruction places the CPU in the idle state, where it remains until an I/O device sends an interrupt signal to the CPU.

## How compilers produce code

Here we will be using gcc example:

1) Preprocessing - resolving macros (#include, #define, ...), to get result after this process use -E whit gcc
2) Compilation - translate source code into assembly code, to stop gcc after this and get result use: -S. You should got *.s file
3) Assembly - translate previous produced assembly into machine code, to stop process after this step use: -c. You should got *.o file
4) Linking - combining our object file with other standard C libraries and other machine codes modules. When used with -o we should got executable binary.

### Compiling on not arm architecture

to install:

```bash
# ARM compilers/assemblers.
arm-linux-gnueabi-gcc

# ARM emulator or if you have use real device, which is ARM based.
qemu-arm
```

to run:

```bash
# Create object file from asm file.
arm-linux-gnueabi-as hello.asm -o hello.o

# Link all the stuff and create executable.
arm-linux-gnueabi-ld hello.o -o hello

# Run executable with qemu-arm.
qemu-arm ./hello
```

## Program example

```C
int main(void)
{
  return 0;
}
```

after we run: gcc program.c -S, we should got file main.s, which should give us result similar to this:

```assembly
@... for now we ignore this messages
@ Program code
        @ compiler generated labels, cause they start with '.'
        .text   @ section for store machine code assembly instructions
        .align  2   @ ensure that lowest two bits of starting address of generated code are zero.
        .global main    @ make main known globally
        .arch armv6
        .type   main, %function
main:
        str     fp, [sp, #-4]!  @ saving caller frame pointer (similar to saving ebp)
        add     fp, sp, #0 @ setting our frame pointer
        mov     r3, #0  @ return 0
        mov     r0, r3  @ return of a function main is in r0
        add     sp, fp, #0  @ restore stack pointer
        ldr     fp, [sp], #4 @ restore caller frame pointer (similar to restoring ebp)
        bx      lr  @ return to next instruction
        .size   main, .-main    @ numbers of bytes in the code
        .ident  "GCC: (Raspbian 8.3.0-6+rpi1) 8.3.0" @ version of the compiler
        .section        .note.GNU-stack,"",%progbits @ information for memory used in stack.
```

## ELF

Above code follows ELF format (Executable and Linking Format). ELF format store code inside sections like .text section is used for assembly code instructions. To read more about the format use **man elf** to read more about elf of binary use **readelf**

![elf](img/elf.png)

## Segments of memory

Text segment (code segment) - program instructions and constant data are stored. The operating system prevents a program from changing anything stored in the text segment, treating it as read-only memory during program execution.

Data segment - where global variables and static local variables are stored. Both read-only and read-write data segments can occur in a program. It remains in place for the duration of program execution.

Stack segment - where automatic local variables and the data that links functions are stored. It is read-write memory that is allocated and de-allocated dynamically as the program executes.

Heap segment - the pool of memory available when a C program calls the malloc function (or C++ calls new). It is read-write memory that is allocated and de-allocated by the program.

## ARM Instructions

Syntax of ARM assembly is called U(nified) A(ssembly) L(anguage).

Instruction scheme:
```
Instruction{s}{condition} {destinationRegister}, Operand1, Operand2
```

* Instruction - name of instruction (like. mov, add ...)
* {s} - An optional suffix. If S is specified, the condition flags are updated on the result of the operation.
* {condition}  - Condition that is needed to be met in order for the instruction to be executed.
* {destinationRegister} - Register (destination) for storing the result of the instruction.
* Operand1 - first operand. Either a register or an immediate value. 
* Operand2 - second (flexible) operand. Can be an immediate value (number) or a register with an optional shift.


In AARCH32 ARM most instructions have an option that allows you to specify that it will be executed only if specific condition of flag exists:
![condition_codes](img/condition_codes.png)

### Shift instruction

As you can see operand2 could be a register with optional shift, here we will list them:

* LSL n - logical shift left by n bits
* LSR n - logical shift right by n bits
* ASL n - arithmetic shift left by n bits
* ASR n - arithmetic shift right by n bits
* ROR n - rotate right n bits
* RRX - rotate right one bit with extend

```assembly
mov r0, 1
mov r1, 0
add r2, r1, r0, lsl 1 @ this will store in r2 2, cause 1*2 = 2

add r3, r1, r0, lsl r2 @ this will store in r3 4, cause 1*2*2 = 4
```

### Instruction set

{} - optional parameters

1. MOV - copies values into a register
```assembly
mov{s}{conditions} destinationRegister, #constantValue
mov r1, #12 @ copy 12 into r1

mov{s}{conditions} destinationRegister, sourceRegister
mov r1, r2 @ copy value of r2 into r1

mov{s}{conditions}   destinationRegister, sourceRegister{, shiftOperand n}
mov r2, 1
mov r1, r2, lsl #2 @ stores inside r1 1 << 2 => 4

mov{s}{conditions}   destinationRegister, sourceRegister, shiftOperand shiftAmountRegister
mov r2, 1
mov r1, r2, lsl r2 @ stores inside r1 1 << 1 => 2
```

2. MVN - copies bitwise NOT of a value into a register

```assembly
mvn{s}{conditions}   destinationRegister, #constantValue
mvn r1, #0 @ stores -1 in r1, cause when we bitwise not on 0 we will got -1 => ~(0000b) => 1111b = -1

mvn{s}{conditions}   destinationRegister, sourceRegister{, shiftOperand n}
mov r2, 1
mvn r1, r2, lsl #2 @ stores inside r1 ~(1 << 2) => -5

mvn{s}{conditions}   destinationRegister, sourceRegister, shiftOperand shiftAmountRegister
mov r2, 1
mvn r1, r2, lsl r2 @ stores inside r1 ~(1 << 1) => -3 
```

1. ADD - adds two integers
```assembly
add{s}{conditions}   destinationRegister, {sourceRegister,} {#constantValue} @ only one of them is optional
mov r1, #2
mov r2, #1
add r1, r2, #2 @ stores 3 in r1

mvn r2, 0 @ stores inside r2 -1
adds r2, #1 @ update r2 by #1 now it's 3

add{s}{conditions}   destinationRegister, sourceRegister1, {sourceRegister2, shiftOperand n}
mov r1, #1
mvn r2, #0 @ stores inside r2 -1
adds r2, r1, lsl #2 @ r2 = -1 + (1 << 2) = 3

add{s}{conditions}   destinationRegister, sourceRegister, {shiftOperand shiftAmountRegister}
mov r1, #1
mvn r2, #0 @ stores inside r2 -1
adds r2, r1, lsl r1 @ r2 = r2 + (r1 << r1) = -1 + (1 << 1) = 1
```

4. SUB - subtract two integers
```assembly
sub{s}{conditions}   destinationRegister, {sourceRegister,} {#constantValue} @ only one of them is optional
mov r1, #2
mov r2, #1
sub r1, r2, #2 @ stores 1 in r1

mvn r1, 0 @ stores inside r1 -1
sub r1, #1 @ r1 is now -2

sub{s}{conditions}   destinationRegister, sourceRegister1, {sourceRegister2{, shiftOperand n}}
mov r1, #1
mvn r2, #0 @ stores inside r2 -1
sub r2, r1, lsl #2 @ r2 -= (r1 << 2) => r2 = -1 - (1 << 2) = -1 - 4 = -5

sub{s} {conditions}   destinationRegister, sourceRegister, {shiftOperand shiftAmountRegister}
mov r1, #1
mvn r2, #0 @ stores inside r2 -1
sub r2, r1, lsl r1 @ r2 = r2 - (r1 << r1) = -1 - (1 << 1) = -3
```

5. BX - Branch to another location.
```assembly
BX{conditions} - is only if address is in register
B{conditions} - is for labels (like main, func etc.)

BLX{conditions} - same as BX but additionally copy the address of the next instruction to r14 (LR)
BX{conditions} - same as B but additionally copy the address of the next instruction to r14 (LR)

mov r1, 0x00000000 @ address
bx r1 @ jump to address in bx register

main:
        b infinite_loop

infinite_loop:
        b infinite_loop
```

6. LDR - loads a word from memory into register
```
LDR{conditions} destinationRegister, label - store address of label into a destinationRegister (label could be memory address)

test:
        mov r1, #0
        @...

main:
        @ use =label to get real address of label
        ldr r1, =test @ store test address inside r1
        bx r1   @ jump to test label
        @...

element here could be a #constValue, register or shifted register by constant (like [r1, r2, lsl #2]).

LDR{conditions} destinationRegister, [sourceRegister{, element}] - store value from updated sourceRegister address inside destinationRegister but withouts changing sourceRegister address in the end.
LDR{conditions} destinationRegister, [sourceRegister, element]! - address of sourceRegister is changed to expression (like 0x00 + 0x1 = 0x01) and destinationRegister (now we get value from [0x01]) is updated with value from new address.
LDR{conditions} destinationRegister, [sourceRegister], element - first load value from sourceRegister address into destinationRegister then update sourceRegister with constValue.
```

7. STR - stores a value from a register into a memory
```
element here could be a #constValue, register or shifted register by constant (for example: [r1, r2, lsl #2] for second case).

STR{conditions} destinationRegister, label - store value from destinationRegister into a label address of memory.
STR{conditions} destinationRegister, [sourceRegister{, element}] - store value from destinationRegister into updated sourceRegister address withouts changing sourceRegister address in the end.
STR{conditions} destinationRegister, [sourceRegister, element]! - address of sourceRegister is changed to expression (like 0x00 + 0x1 = 0x01) and now we transfer value from destinationRegister to address of memory in updated sourceRegister, sourceRegister address is also changed.
STR{conditions} destinationRegister, [sourceRegister], element - first load value from destinationRegister into address of sourceRegister then update sourceRegister address with constValue.
```

8. PUSH - 

Simple code with calling function:
```
.global main
.text

test:
    str fp, [sp, #-4]!
    add fp, sp, #0
    mov r0, #1
    add sp, fp, #0
    ldr fp, [sp], #4
    bx lr
main:
    str lr, [sp, #-4]!
    bl test
    ldr lr, [sp], #4
    bx lr
```

9. ADR - load address of element
```
ADR{conditions} destinationRegister, label {+/- constant}
* ADR cannot be PC

adr r0, main+4 @ store second instruction in main, cause one instruction have 4 bytes and main is first instruction.
```

10. POP - pop value from stack into register (only 32-bit)
```
pop{conditions} registers... - store values from stack into register (updates sp).

pop {r0, r1} - pop 8 bytes from stack first 4 bytes into r0 second 4 bytes into r1.
```

11. PUSH - store value from registers into a stack (only 32-bit)
```
push{conditions} registers... - store values from registers into a stack (updates sp).

push {r0, r1} - push 8 bytes from registers first 4 bytes from r0 and second 4 bytes from r1.
```

12. LDM - load multiple from stack (new POP for 64-bit and 32-bit)
```
ldm{operand}{conditions} sourceAddress, registers.. - load from source address into register

ldm sp, {r0, r1} - load next two values from sourceAddress address into r0 and r1.

ldm sp, {r0-r3} - like above but load registers: r0, r1, r2, r3
```

13. STM - store multiple values to stack (new PUSH for 64-bit and 32-bit)
```
stm{operand}{conditions} sourceAddress, registers.. - store from registers under an sourceAddress address.

stm sp, {r0, r1} - store next two values from registers r0 and r1 under sourceAddress. (r0 will be loaded into sp address and r1 will be loaded into sp + 4 address)

stm sp, {r0-r3} - same but store registers: r0, r1, r2, r3
```

Operands for STM i LDM:

* IA (increase after) (LDM == LDMIA i STM == STMIA) - increase pointer after each load. Store and load to registers in given order.
* IB (increase before) - first increase address then load values. Store and load to registers in given order.
* DA (decrease after) - decrease pointer after each load. Store and load to registers in reverse order.
* DB (decrease before) - first decrease address then load values. Store and load to registers in reverse order.

### Create assembly function and run it with C

`func.c`:

```assembly
.global func1
.global func2

.text
func1:
    str fp, [sp, #-4]!
    add fp, sp, #0
    mov r0, #1
    add sp, fp, #0
    ldr fp, [sp], #4
    bx lr

func2:
    str fp, [sp, #-4]!
    add fp, sp, #0
    mov r0, #2
    add sp, fp, #0
    ldr fp, [sp], #4
    bx lr
```

`main.c`:

```cpp
#include <stdio.h>

extern int func1();
extern int func2();

int main(void) {
    printf("first=%d, second=%d\n", func1(), func2()); 
}
```

Compile with:
```bash
gcc func.s main.c -o main
```

## Functions calls

```assembly
.global main
.text

@ example shows how to call test function
test:
    str lr, [sp, #-4]! @ decrement a sp and store lr into a stack
    str fp, [sp, #-4]! @ decrement a sp and store fp into a stack
    add fp, sp, #0 @ update fp with sp
    @... use here arguments operate on sp and stack
    mov r0, #0 @ return value of function
    add sp, fp, #0 @ update stack to the state from beginning of test+4
    ldr fp, [sp], #4 @ load previous stored fp from stack into fp register
    ldr lr, [sp], #4 @ load previous stored lr from stack into lr register
    bx lr @ call next instruction that should be called after end of this function.

@ main usually differ then the one bellow, but we make it this way for simplicity.
main:
    str lr, [sp #-4]! @ store next instruction into a stack
    bl test @ call a function and store next instruction after this into lr register
    ldr lr, [sp], #4 @ load previous lr value from stack
    bx lr @ call next instruction that should be called after end of main.
```

## Hello world example

[Linux ARM 32-bit system call table](https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md#arm-32_bit_EABI)

```assembly
.global _start
.section .text


_start:
        # System call write.
        mov r7, #0x04
        mov r0, #0x01
        ldr r1, =message
        ldr r2, =len
        swi 0

        # Exit with success.
        mov r7, #0x01
        mov r0, #0x00
        swi 0

.section .data
message:
        .ascii "Hello World\n"
len = .-message
```


reference:

* https://en.wikichip.org/wiki/acorn/microarchitectures/arm1
* http://bob.cs.sonoma.edu/IntroCompOrg-RPi/frontmatter-1.html
* https://en.wikipedia.org/wiki/Executable_and_Linkable_Format