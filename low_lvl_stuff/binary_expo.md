## Stuff important for binary exploitation

## Commands
* strace program - writing out all system calls for a program
  * strace program 2>&1 | less - less a program (it's cause strace sends to stderr not stdout)
* ltrace program - writing out all library calls for a program
* objdump program - display information form program files
  * objdump -d program - disassemble
  * objdump -x program - headers
  * objdump -t program - name and address of a function, variables etc.


## Radare2
main tutorial: https://radare.gitbooks.io/radare2book/content/first_steps/intro.html
r2 program - run radare with program

#### Basic r2 commands
Important to know each character in radare mean something
* aaa - automatically analyse and autoname functions
* a - keyword stand for analyzee
* afl - print all functions that radare2 found
  * a - analyze
  * f - function
  * l - list (print)
* command? - will print help about a command
* s(eek) - print current address
  * s address - set current address to address (expressions also available like: main + 0x13)
    * s++/-- move over: (0x100)
* s sys.main - change address to main
* i - info keywords
* pdf - print disassembly of a current function
* VV - visual mode of disassembly function
  * shift + tab - change between blocks
  * P - change representation of the graph
* V! - mode with stack and registers
* fs imports; f - flag imports used by binary
* iz - search for strings

#### Debugging with r2
to start debugging run: r2 -d program

Commands:
* db address - place breakpoint
* :dc - run program
* shift + s - next line (without going nested to blocks or functions)
* s - next line (with nested lines in function)
* dr - show registers
  * dr eip=0x123 - set eip to some value

#### rbin2
Part of r2 allows extracting information from binary files including sections, headers, imports, strings, entrypoints, etc.

*rabin2 program args:
  * -I binary info


## Python
Python2:
* int('1111', 2) - convert binary to decimal
* int('AF', 16) - convert hex to decimal
* hex(123) - convert bin to dec

Encode/decode with ASCII character:
```python
# Python3
  # works only for single characters
ord('A') # ASCII to dec => 65
hex(ord('A')) # ASCII to hex => 0x41
format(ord('A'), x) # ASCII to hex => 41
chr(65) # dec to ascii => 'A'
chr(0x41) # hex to ascii => 'A'

# from ascii hex to ascii string python2
'61626364'.decode('hex') # abcd

# from string to hex
import binascii
binascii.hexlify('abcd'.encode()) # b'61626364'
```

Terminal pipe python command to something else:
```bash
echo -e "$(python -c "print('A'*21)")\n" | some_program
python -c "print('A'*21)" | some_program
```

To write pure bytes in python3 use:
```python
# from string to bytes
import sys

sys.stdout.buffer.write(bytes.fromhex('ff')) # write on buffer: \xff (in bytes)
# or
sys.stdout.buffer.write(b'\xff')
```

Python3 byte decode and encode:
```python
import struct
#integer to bytes
print(struct.pack('<I', a))
#bytes to integer
print(struct.unpack('<I', b'\x07\x00\x00\x00'))

# pack little-endian 64 bytes
print(struct.pack('<Q', 0x0000000000000000))

#convert string to byte
s = 'ABCD'
p = bytes (s, encoding ='raw_unicode_escape') + b'\x00'
print(p)
```

## Linking libraries in binary and others
Dynamic linked - means that library are linked to the binary at a run time. (It's sometimes let's us overwrite some function in the system to exploit binary). What is good about this solution is that our binary has much more lower weight than Static linked binary.

Static linked - means our libraries are already included into binary.

Striped binary - means that all names of function, variables etc. in binary are changed to make them not recognized what they might do.

PIE - position independent executable - executables that can be place anywhere in memory affected by ASLR. It means anytime you run a binary, every instruction address differ of some offset.

ASLR usually keep end of the address same but change the front of it. e.g Instruction has address: 0x123456789, after we run it again the same instruction has address 0x555555789. We can see that end of instructions stayed the same.

Offset - the amount of distance by which something is out of line.

### Loader
Main responsibilities is to prepare program in the way from disk and place it in memory that it can be run.

Steps in Linux (with execve()):
1. Validation (permissions, memory requirements etc.);
2. Copying the program image from the disk into main memory;
3. Copying the command-line arguments on the stack;
4. Initializing registers (e.g., the stack pointer);
5. Jumping to the program entry point (_start).

### Symbol table - table where all the symbols are defined like functions, assembly sections etc.

Can be check with: objdump -t file.o (or executable file)
```
main.o:     file format elf64-x86-64

SYMBOL TABLE:
0000000000000000 l    df *ABS*  0000000000000000 main.c
0000000000000000 l    d  .text  0000000000000000 .text
0000000000000000 l    d  .data  0000000000000000 .data
0000000000000000 l    d  .bss   0000000000000000 .bss
0000000000000000 l    d  .rodata        0000000000000000 .rodata
0000000000000000 l    d  .note.GNU-stack        0000000000000000 .note.GNU-stack
0000000000000000 l    d  .note.gnu.property     0000000000000000 .note.gnu.property
0000000000000000 l    d  .eh_frame      0000000000000000 .eh_frame
0000000000000000 l    d  .comment       0000000000000000 .comment
0000000000000000 g     F .text  000000000000002c main
0000000000000000         *UND*  0000000000000000 _GLOBAL_OFFSET_TABLE_
0000000000000000         *UND*  0000000000000000 getId
0000000000000000         *UND*  0000000000000000 printf
```

UND - means that symbols address is not know at this stage it might be latter resolve by linker (if we link other file with function declaration) or latter at run-time (if it's for example shared library function).


### Objdump - represent binary in hex
* -d  - disassemble partly
* -D - disassemble fully
* -t - display content of the symbolic table


### Size - give size of data and section of a file