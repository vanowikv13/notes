# GDB - GNU Debugger

In order to use GDB with additional options, you need to compile program debugged program with -ggdb flag.

```bash
# for C:
gcc [other flag] -g <source files> -o <output file>
gcc -g program.c -o program
# for nasm 32-bit, i386:
nasm -f elf32 -g basic_example.asm -o basic_example.o
ld -m elf_i386 basic_example.o -o basic_example
```

## Help

```bash
(gdb) help [command]
```

## Basic commands

### Running - r[un]

```bash
(gdb) run
```

### Breakpoints

#### Adding breakpoints - b[reak]

```bash
# At specific function (for example main).
(gdb) break *main

# At specific instruction address.
(gdb) break *0x000004004

# At specific line in file.
(gdb) break file.c:6
```

#### Deleting breakpoint - d[elete] b[reak] {num}

```bash
(gdb) delete break number
```

#### Viewing breakpoints - i[nfo] b[reak]

```bash
(gdb) i[nfo] b[reak]
```


#### Continuing - c[ontinue] - when program stops on breakpoints to continue the program execution

```bash
(gdb) continue
```

#### Step (si) - execute the next line of code

```bash
(gdb) step
```

#### Next (ni) - execute the next portion of code (not stopping line by line)

```bash
(gdb) next
```

#### Watches

```bash
# Set watch on variable.
(gdb) watch program_var

# Set watch on register.
(gdb) watch $eax
```

### Set variables to value - set var = expr

```bash
(gdb) set $eax = 2
(gdb) set program_var = program_var +2
(gdb) set {int}0x56557010=0xd
```

## Printing

### Print - p[rint][/format] expr

Print program variable in hex:

```bash
(gdb) print/x program_var
```

If you want to print field of:

* Class use obj.value
* Pointer to class use obj->value
* Value of Pointer *obj

Possible `/format` args:

* d - dec
* x - hex
* t - bin
* c - character
* o - oct

Examples:

```bash
# Print as 5 array with each element of one byte with hex.
(gdb) print /x (char[5])&string_buffer 
```

### Print dump from memory - x

```bash
(gdb) x /options <addr>
```

Other `options`:

* o(octal)
* x(hex)
* d(decimal)
* u(unsigned decimal),
* t(binary),
* f(float),
* a(address),
* i(instruction),
* c(char)
* s(string)
* w - address

Example:

```bash
(gdb) x /s 0x8048500 ;print string from address
(gdb) x /24wx $esp   ;print address of esp and value and 23 cells after below esp.
(gdb) x/2i $eip      ;print actual address instruction and instruction name and one more instruction after
(gdb) x/wx $esp+0x5c ;reading value and address of a value from memory with esp register
(gdb) x /32wi ;reading address of esp and instruction for next 31 cells
```

### Printf

scheme: ```(gdb) printf "format", var1, va2 ...```

```bash
# print decimal variable from address
(gdb) printf "%d\n", *(0xffffce60) #works also with print *(0xffffce00)
 
#print unsigned decimal from address
(gdb) printf "%u\n", *(0xffffce00) # works also: print *(unsigned int*)(0xffffce74)

#print char* 
(gdb) printf "%s", 0xffffce68 #works also: print (char*)0xffffce68
```

### Info about variables

```bash
# Print all locals and their values.
(gdb) info locals

# Print all args and their addresses.
(gdb) info args

# Print all globals/static.
(gdb) info variables

# Print all globals/static that starts with "start" (regex apply).
(gdb) info variables start*

# Prints information about value address, could be function, hex address, variable etc.
(gdb) info address value
```

### Assembly program - disas(semble)

```bash
# Set intel syntax.
(gdb) set disassembly-flavor intel

# Disassemble function main.
(gdb) disassemble main (or layout asm for more details view)
```

### Print registers values

```bash
(gdb) info registers
```

### Function calls trace

```bash
# Print function calls.
(gdb) backtrace
```

### Jump to line - jump

```bash
# Jump to instruction at specific address.
(gdb) jump *0x000055555555515a
```

### Print the list of signals

```bash
(gdb) info signals
```

### Show memory segments and permissions

```bash
(gdb) info proc map (or vmmap)
```

### Threads

#### List all threads

```bash
(gdb) info threads
```

#### To switch into thread with 1 id

```bash
(gdb) thread 1
```

#### Print backtrace for all threads

```bash
# Print backtrace of all threads in shortcut (taa bt)
(gdb) thread apply all bt
```

### Forks and child/parent process

#### To set which process when fork is called follow call

```bash
(gdb) set follow-fork-mode mode
```

Where mode could be parent or child, which tells which one to follow.

#### To debug both parent and child process

```bash
(gdb) set detach-on-fork mode
```

Where mode could be off to debug both process or on (debug parent or child process depend of follow-fork-mode). By default detach on fork is turned on.

### Information about file, like segments, etc.

```bash
(gdb) info file
```

### Output GDB stdout to a file

```bash
# Save GDB output into gdb.txt file.
(gdb) set logging on
```

### Quit from GDB - q(uit)

```bash
(gdb) quit
```

### To make config for gdb

1. Place a .gdbinit file in your user home directory
2. Write a commands inside .gdbinit file that you want to run at the start of the program.

Nice links:
* https://cheatography.com/fristle/cheat-sheets/closed-source-debugging-with-gdb/