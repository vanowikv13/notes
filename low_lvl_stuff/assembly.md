## Introduction to low level programming for x86

Register - It's fixed size, quickly accessible location (variable) available to processor. Registers are measured for a number of bits they can hold, for example 32-bit or 64-bit. Some registers have specific hardware functions, and may be read-only or write-only. Each register have it's own address. There are two group of register general purpose and special purpose.

Program Counter (special purpose register) - PC tells the CPU which instruction we're executing next. Every time we execute an instruction, we advance the program counter. This name could differ depend of the architecture for example (RIP, EIP, IP)

Important to note is that what doesn't fit in register is stored in memory.

When we transfer value from Register to memory it will reverse bytes.
e.g 07 25 -> 25 07 (on the other way works this same)

Stack (LIFO data structure) is area of the bottom of our memory. You have also a special register that points to top of the stack esp (e.g. SP, RSP). 
* push eax - decreasing ESP and adding value to stack
* pop eax - getting value from stack and increasing ESP

Stack usually grow from top to down, so when we add new value to stack it's esp will decrease.

Control flow is done via GOTOs --- jumps, branches, or calls. The 
effect of these instructions is to alter the program counter directly.
jmp 5 -> set PC (IP) to 2 (jump is unconditional GOTO).

Many instructions involve comparisons and mathematical calculations and change the status of the flags and some other conditional instructions test the value of these status flags to take the control flow to other location.

EFLAGS Register (or just Flags) consists of individual binary bits that control the operation of the CPU or reflect the outcome of some CPU operation.
* The Carry flag (CF) - (**unsigned**) is set when:
   1. Unsigned arithmetic operation is to large to fit into destination. For example when output of our operation is 9 bits but our register is 8 then CF flag is set. Like: 1111 + 0001 = 0000 (CF is set here)
   2. Subtract happen from smaller a larger number, because we need to borrow one (non exist) bit value. For example: 0000 - 0001 = 1111
* The Overflow flag (OF) (only for **signed**) is set when:
   1. Result of **signed** arithmetic operation of two positive numbers is negative. For example: 0100 + 0100 = 1000 (4 + 4 = -8)
   2. Result of **signed** arithmetic operation of two negative numbers is positive. For example: 1000 + 1000 = 0001 (-8 + -8 = 0)
In other word whenever the output leftmost bit is different than two numbers that operations has occur in that cases overflow flag is turned on.
* The Sign flag (SF) is set when the result of an arithmetic or logical operation generates a negative result.
* The Zero flag (ZF) is set when the result of an arithmetic or logical operation generates a result of zero.
A flag is set when it equals 1; it is clear (or reset) when it equals 0.
2.2 32-Bit x86 Processors 41
* The Auxiliary Carry flag (AC) is set when an arithmetic operation causes a carry from bit 3 to bit 4 in an 8-bit operand.
* The Parity flag (PF) is set if the least-significant byte in the result contains an even number of 1 bits. Otherwise, PF is clear. In general, it is used for error checking when there is a possibility that data might be altered or corrupted.

[More about CF and OF](http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt)

je eax (jump equal uses this flag) -> jump to eax if zero flag is set.

A CALL is just an unconditional GOTO that jump to call location and pushes the next address on the stack, so a RET instruction can later pop it off and keep going where the CALL left off it.

#### Basic registers
Registers are high-speed storage locations directly inside the CPU, designed to be accessed at much higher speed than conventional memory
![general purpose registers](img/gp_registers.png)

How to get part value of a some registers.

![gpr](img/gpr.png)

This works same also for other registers.

![part registers](img/part_registers.png)

Floating points unit registers
![floating points unit registers](img/fpu_registers.png)

Index registers

![Index Registers](img/index_registers.png)

#### Assembly basic instructions
```assembly
mov eax, 0x5    ; set on register value 0x5
add eax, 0x3    ; add to register eax 0x3 
sub eax, ebx    ; eax \ ebx -> and place that in eax
inc ecx         ; ecx += 1 (dec => -= 1)
lea eax, eip  ; fetch the address of eip into eax (works like mov but move copy value under address but not address itself)
mov eax, [0x14]   ; move from register eax to memory with address of 0x14
push eax    ; put a eax on a stack
pop eax     ; get top element from stack
$ - refer to address of actual line
call procedure  ;call a some section or function
$$ - refer to the address of the our section
(how far in the section you're) - $ - $$
```

Remember that ALU can make only one call to memory.
```assembly
; add [0x1], [0x2] ;invalid instruction
  mov eax, [0x1] ;0x1 memory address of one cell
  add eax, [0x2] ;0x2 memory address of second cell
```

other example:
```assembly
mov eax, 1  ; 1 - exit calls
mov ebx, 21 ; exit status of our program
int 0x80 ;  ; interrupt calling kernel
```
int - interrupt, processor is transfer control into interrupt handler that we specify with value, here we use 0x80 which is interrupt for system calls. The system call that it make is determined by eax register, 1 in eax means we're making a exit calls and that signal end of our program. The value is storing ebx it will be exit status for our program.


#### Assembly language statements
```assembly
[label]   mnemonic   [operands]   [;comment]
```

#### Assembly sections
* section.data - declaring initialized data or constants. This data does not change at runtime. You can declare various constant values, file names, or buffer size, etc., in this section.
* section.bss - declaring variables.
* section.text - keeping the actual code. This section must begin with the declaration global _start, which tells the kernel where the program execution begins.
```assembly
section.text
   global _start
_start:
```

#### Arithmetic operations
```assembly
 add ebx, edx ; ebx += edx
 sub ebx, edx ; ebx -= edx
 mul ebx ; eax *= ebx
 imul eax, ebx, 5 ;ebx = eax * 5
 div ebx ; eax /= ebx 
```

#### Shifting
```assembly
shl eax, 2  ;shift to the left (e.g. 1010 << 2 => 101000)
shr eax, 3  ;shift to the right (e.g. 10100 >> 3 => 00010)
```

#### Logical operations
```assembly
and eax, 5  ;eax = eax & 5
not eax     ;reverse all bits in eax
or eax, 4   ;eax = eax | 4
xor eax, 3  ;eax = eax ^ 3
```

#### Hello World example
```assembly
section	.text
   global _start     ;must be declared for linker (ld)
	
_start:	            ;tells linker entry point
   mov	edx, len     ;message length
   mov	ecx, msg     ;message to write (holds the string pointer)
   mov	ebx, 1       ;file descriptor (stdout)
   mov	eax, 4       ;system call number (sys_write call)
   int	0x80        ;call kernel (interrupt to make system calls to kernel)
	
   mov	eax, 1       ;system call number (sys_exit)
   mov   ebx, 0      ;0 is our exit status of program
   int	0x80        ;call kernel

section	.data
msg db 'Hello, world!', 0x0a  ;string to be printed
len equ $ - msg     ;length of the string
```
* 0x0a => new line character in hex
* [file descriptor options](https://www.computerhope.com/jargon/f/file-descriptor.htm#:~:text=A%20file%20descriptor%20is%20a,Grants%20access.)

#### Nasm and ld compilation (x86)
To compile asm program with nasm and link it use:
```bash
nasm -f elf32 x.asm -o x.o
ld -m elf_i386 -o x x.o
#to correctly link with ld your program use _start: section and make it global (from this section your execution will start)
```

#### Control flow
```assembly
cmp eax, ebx ;cmp doesn't change a value of eax (but SUB does)
jmp label ;(just jump to label without any condition)
je label ;(jump when equal)
jne label ;(jump when not equal)
jz label ;(jump when last result was zero)
jg label ;(jump when greater than)
jge label ;(jump when greater than or equal to)
jl label ;(jump when less than)
jle label ;(jump when less than or equal to)

test eax, ebx ;doest eax and ebx operation but doesn't change value of eax (like and does)

```

#### Jump example (jmp)
jmp set eip to specific address
```assembly
global _start

section .text

_start:
   mov ebx, 0 ;exit status set to 0
   mov eax, 1  ;sys_exit system call
   jmp skip    ;jump to "skip" label
   mov ebx, 13 ;exit to status 13
skip:
   int 0x80

```
exit status should be 0, cause we jump the line with setting exit status to 13

#### jump if less example (jl - jump less, jg - jump greater, je - jump equal)
```assembly
global _start

section .text

_start:
   mov ecx, 23 ;set ecx to 23
   mov ebx, 0 ;exit status set to 0
   mov eax, 1  ;sys_exit system call
   cmp ecx, 20 ;compare ecx with 20
   jl skip    ;jump if less than 22
   mov ebx, 13 ;exit to status 13
skip:
   int 0x80

```
the exit status: 13  (cause 23 >= 20 so condition is false and it didn't make a jump).
if we will set cmp ecx, 30, exit status will be 0

#### looping examples
* [inc loop](asm_code_examples/inc_loop.asm)
* [dec loop](asm_code_examples/dec_loop.asm)

#### Changing data in string
```assembly
global _start
section .data
   addr db "yellow"  ;label to some memory address and pointer points to string that contain words "yellow

section .text
_start:
   mov [addr], byte 'H' ;we're moving at the begin of array 'H' (byte keyword is important to tell language how much of data we're moving)
   mov [addr + 5], byte '!'
   mov eax, 4  ;sys_write system call
   mov ebx, 1  ;stdout file descriptor (we're writing to stdout)
   mov ecx, addr  ;bytes to write
   mov edx, 6  ;number of bytes we want to write
   int 0x80 ;perform a system call
   mov eax, 1  ;sys_exit system call
   mov ebx, 0  ;exit status is 0
   int 0x80 ;exit program
```

[other example](asm_code_examples/change_data_in_str_arr.asm)

#### Assembly datatype
```assembly
section .data
   ;byte is a single byte datatype
   ;db is 1 byte
   name1 db "string"
   name2 db 0xff
   name3 db 100
   ;dw is 2 bytes
   name4 dw 1000
   ;dd is 4 bytes
   name5 dd 100000
   ;dq is 8 bytes integer
   ;dt is 10 bytes integer
   ;to get value from variable [variable], all vars are pointers.
```

not reserved datatype (that need to be declared in .bss)
```assembly
buffer:         resb    64              ; reserve 64 bytes 
wordvar:        resw    ?               ; reserve a word 
realarray       resq    10              ; array of ten reals 
ymmval:         resy    ?               ; one YMM register 
zmmvals:        resz    32              ; 32 ZMM registers
```

Like in example resx is for reserve x data and dx is for define x data.
x could be:
 - b(yte) - 1 byte
 - w(ord) - 2 bytes
 - d(ouble) word - 4 bytes
 - q(quad word) - 8 bytes
 - t(en) bytes - 10

#### Stack usage
esp - its stack pointer. It's points to what is on top, esp is not decreased by 1 but by 4 bytes or 8 depends of architecture
stack is on bottom of memory, and esp is on the last cells in memory (at beginning)
```assembly
push 1234   ;push value on top of stack, esp -= 1
push 4321   ;esp -= 1
push 133    ;esp -= 1
;alternative push
sub esp, 4  ;it's will be 8 in x64 architecture, we're making subtraction, cause when we push on stack the esp is decreasing
mov [esp], dword 357 ;putting value on stack, (dword - means what size of a value we're putting)

pop eax  ;place 133 at eax, esp += 1
mov eax, dword [esp]
add esp, 4  ;in x64 architecture we will add 8
```

#### Using call function
call:
* pushes EIP to stack
* perform a jump in location we specify
* this give us opportunity to jump to some location and then jump back
```assembly
global _start
_start:
   call func   ;jumping to func and pushing on stack address after function call
   mov eax, 1  ;setting sys_exit
   int 0x80    ;calling kernel and exit

func:
   mov ebx, 42 ;setting ebx to 42 (exit_status)
   pop eax  ;setting eax to address of a second instruction in _start
   jmp eax  ;jumping to address of eax value
```
instead of "pop eax" and "jmp eax" we could use just a simple "ret"

#### Using call and stack inside of call (func)
```assembly
global _start
_start:
   call func
   mov eax, 1
   mov ebx, 0
   int 0x80

func: 
   mov ebp, esp 
   sub esp, 2  ;move pointer to make a space for values
   mov [esp], byte 'H'
   mov [esp+1], byte 'i'
   mov eax, 4  ;sys_write system call
   mov ebx, 1  ;stdout file descriptor
   mov ecx, esp   ;bytes to write (start of array)
   mov edx, 2  ;number of bytes to write out
   int 0x80 ;perform system call
   mov esp, ebp   ;free space and put the stack on the right track
   ret
```

### Correct usage of functions (template) using _cdecl
```assembly
global _start
_start:
   push 3   ;arg 3
   push 2   ;arg 2
   push 1   ;arg 1
   call func   ;return value is in eax
   add esp 12 ;cause we insert 3 var of 4 bytes and we need to clean space for arguments
   mov ebx, eax   ;moving return value to ebx
   mov eax, 1
   int 0x80

   ;-----Stack at function call--------;
   ; esp->  Func argument1             ;
   ;        // func locals             ;
   ; ebp->  previous EBP               ;
   ;        previous return address    ;
   ;        argument 1                 ;
   ;        argument 2                 ;
   ;        argument 3                 ;
   ;        rest of stack...           ;
   ;-----------------------------------;

func:
   push ebp
   mov ebp, esp
   xor eax, eax   ;set eax to 0
   mov eax, [esp+16] ;move argument 1 value 
   add eax, [esp+20] ;add argument 2
   add eax, [esp+24] ;add argument 3
   sub, esp 4 ;declaring 4 bytes
   mov [esp], eax ;move eax to local function variable
   add esp, 4 ;clearing 4 bytes
   pop ebp
   ret
```

#### Times2 example
```assembly
_start:
   push 21
   call times2
   mov ebx, eax 
   mox eax, 1
   int 0x80
times2:
   push ebp
   mov ebp, esp
   mov eax, [ebp+8]
   add eax, eax
   mov esp, ebp
   pop ebp
   ret

```

#### User input and linux system calls
All the system calls are listed in /usr/include/asm/unistd.h
![system calls](img/system_calls.png)

[user input example](asm_code_examples/user_input_example.asm)

#### Arrays
1. Specified in .data section
[arrays](asm_code_examples/arrays.asm)

2. Other examples
* CPP: [arrays_more.cpp](asm_code_examples/arrays_mode.cpp)
* ASM: [arrays_more.asm](asm_code_examples/arrays_more.asm)

#### Struct from C++ to assembly
```cpp
//#pragma pack(1) //with pragma pack we delete unused padding but it will cost us performance
typedef struct {
    unsigned char a; //1 byte
    int b; //4 bytes
    long c; //4 bytes
}TestStruct;

sizeof(TestStruct); // 12
/*
 If some variable is less than a 4 bytes or we cannot get sum of sizes
 of variable to 4 bytes there is added a padding for performance reasons.
*/

TestStruct testStruct;
testStruct.a = 0x1;
testStruct.b = 0x22222222;
testStruct.c = 0x33333333;
//In memory: 01 XX XX XX 22 22 22 22 33 33 33 33

//From this we can see that struct variables are stored one right after another and if one variable is less than 4 bytes and second + first is more than 4 bytes around the first would be add a padding.
```

There is also a way to define a structure in assembly. Here is [link](https://www.csee.umbc.edu/courses/undergraduate/313/spring05/burt_katz/lectures/Lect10/structuresInAsm.html)

#### Using printf function from C
Steps to use example:
* nasm -f elf32 program.asm -o program.o
* gcc -m32 program.o -o program
* ./program
```assembly
global main ;required by gcc
extern printf  ;to tell we want to use external func

section .data
    msg db "Testing %i...", 0x0a, 0x00 ;0x0a = '\n', 0x00 = '\0' = null
    ;we're making 0x00 to tell where is end of the string
    ;%i format specifier that expect a integer

main:
    push ebp 
    mov ebp, esp
    push 123   ;integer we want to print
    push msg   ;message we want to print
    call printf   ;calling printf with message and 123 integer
    mov eax, 0 ;exit status in our program
    mov esp, ebp
    pop ebp
    ret

```

### Inserting asm code into C/C++
Compile and run: g++ -m32 -masm=intel -o filename filename.cpp && ./filename
```cpp
#include <iostream>

int main() {
    int x = 12;
    int y = 13;
    
    /*
    Extended Assembly:
    asm ( "assembly codei;"
           : output operands                  optional
           : input operands                   optional
           : list of clobbered registers      optional
    );

    Registers key specified in input and output for variables.
    +---+--------------------+
    | r |       Random       |
    +---+--------------------+
    | a |   %eax, %ax, %al   |
    | b |   %ebx, %bx, %bl   |
    | c |   %ecx, %cx, %cl   |
    | d |   %edx, %dx, %dl   |
    | S |   %esi, %si        |
    | D |   %edi, %di        |
    +---+--------------------+*/
    
    asm(
    //copy value from y into eax
    "mov eax, %1;"
    //copy value from eax into x (after an instruction)
    "mov %0, eax;"
        //= - output operand required for write mode
        : "=r" (x) //output
        : "r" (y) //input
        : "eax" //used registers
    );


    std::cout << x << std::endl;
    std::cout << y << std::endl; 
    
    
    //other example:
    std::cout << std::endl;
    int sum, i = 1, j = 2;
    //volatile - protect from moving code for optimality
    asm volatile ("add eax, ebx;"
        : "=r" (sum)
        : "a" (i), "b" (j)
    );

    std::cout << sum << std::endl << std::endl;    

    //3. Multiply a*b in loop
    int a = 6, b = 6, mul = 0;
    asm (
        "mov eax, 0;"
        "Loop: cmp ecx, 0;"
            "jz Done;"
            "add eax, ebx;"
            "sub ecx, 1;"
            "jmp Loop;"
        "Done: mov %0, eax;"
        : "=a" (mul)
        : "b" (a), "c" (b)
    ); 

    
    std::cout << mul << std::endl;
    return 0;
}

/* References:
    - https://www.codeproject.com/Articles/15971/Using-Inline-Assembly-in-C-C
    - https://gcc.gnu.org/onlinedocs/gcc/Extended-Asm.html
*/
```


### Including asm code into C++ code
C++ code
```cpp
#include <iostream>

extern int fun(int*, int*) asm("_fun");

int main() {
    int x = 12, y = 13;
    std::cout << fun(&x, &y) << std::endl;
    return 0;
}
```
Assembly 
```assembly
section .text

global _fun

;multiply two values and return it
_fun:
    push ebp
    mov ebp, esp
    mov edx, [esp+8] 
    mov eax, [edx]
    mov edx, [esp+12]
    mov edx, [edx]
    mul edx

    pop ebp
    ret
```

Compile, link and run (x86): nasm -f elf32 y.asm -o y.o && g++ -m32 x.cpp y.o -o x && ./x
