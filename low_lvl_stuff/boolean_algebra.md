# Boolean Algebra
### Operators
* NOT - "`"
* AND - "·"
* OR - "+"
* XOR - "⊕"

### Most important equations
```
x·y = y·x
x+y = y+x

x·x = x
x+x = x

x·(y·z) = (x·y)·z
x+(y+z) = (x+y)+z

x·x` = 0
x+x` = 1

x·(y+z) = x·y+x·z
x+y·z = (x+y)·(x+z)

x⊕y = x·y` + x`·y
```

DeMorgan's Law:
```
(x·y)` = x` + y`
(x+y)` = x`·y`
```

Multiplexer - Conditional statement:
```
if(condition == 1)
    return x;
else 
    return y;

Boolean Algebra Multiplexer: (x·z)+(y·z`)
```