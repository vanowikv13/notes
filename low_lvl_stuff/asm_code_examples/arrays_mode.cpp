#include <stdio.h>
#include <cstdint>

extern int8_t fun() asm("_fun");
//with pointer to array
extern int8_t withPtr(int8_t*) asm("_withPtr");

//with pointer to array and looping
extern int8_t withPtrLoopA(int8_t*) asm("_withPtrLoopA");

//with pointer to array and looping
extern int8_t withPtrLoopB(int8_t*) asm("_withPtrLoopB");

void pointerAddressing() {
    volatile int8_t tab[5];
    for(int8_t i = 0; i !=5; i++)
    //compile will change i to appropriate number of bytes
        *(tab+i) = i;
}

void indexAddressing() {
    volatile int8_t tab[5];
    for(int8_t i = 0; i !=5; i++)
        tab[i] = i;
}

 int8_t globalTab[5] = {5, 4, 3, 2, 1};

int main() {
    //pointerAddressing();
    //indexAddressing();

    printf("%hhu\n",fun());

    int8_t tab[5];
    printf("%hhu\n",withPtr(tab));
    printf("%hhu\n", tab[4]);

    printf("tab[4] = %hhu\n", globalTab[4]);
    printf("Global last = %hhu\n",withPtr(globalTab));
    printf("tab[4] = %hhu\n", globalTab[4]);

    int8_t tab1[5];
    printf("Last of A: %hhu\n", withPtrLoopA(tab1));
    printf("tab[2] = %hhu\n", tab1[2]);

    int8_t tab2[5];
    printf("Last of B: %hhu\n", withPtrLoopB(tab2));
    printf("tab[4] = %hhu\n", tab2[4]);
    return 0;
}
