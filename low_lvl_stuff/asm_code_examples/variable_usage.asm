global _start
section .text
_start:   
    mov eax, 1
    add [x], byte 12 ;adding to the x var 12, (we need to use [ ] to call value not address)
    mov ebx, [x]     ;placing value of x into ebx
    int 0x80         ;call kernel
section   .data
    x dd 1 
