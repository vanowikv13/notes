section .text

global _fun

_fun:
    push ebp
    mov ebp, esp

    mov ecx, [esp+8] 
    mov bl, [ecx] ;a
    
    mov cl, 0x01   ;index wil go from lsb to msb
    mov al, 0  ;y

    jmp _loop_reverse

_loop_reverse:
    test bl, cl
    jnz _loop
    add al, 1

    jmp _loop

_loop:
    test cl, 0x80
    jnz _ret_value

    shl cl, 1 ;cl+=1
    jmp _loop_reverse

_ret_value: 
    ;return value is by default in eax

    ;uncoment if you want set pointer value to reverse value
    ;mov ecx, [esp+8]
    ;mov [ecx], al
    
    pop ebp
    ret
