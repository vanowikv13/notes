global _start
section .data
    arr db "xdddd"          ;our text
    len equ $ - arr         ;length of arr

_start:
    mov [arr], byte 'd'     ;placing at the begin of the ary 'd'
    mov [arr+4], byte 0x0a  ;placing at the end of the arr new line

    ;sending data to stdout
    mov eax, 4              ;sys_write call
    mov ebx, 1              ;file descriptor (stdout)
    mov ecx, arr            ;address to start of the message    
    mov edx, 5              ;amount of bytes to write out    
    int 0x80                ;calling kernel for stdout

    ;exit program
    mov eax, 1
    mov ebx, 0
    int 0x80
