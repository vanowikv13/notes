global main
extern atoi

section .text
main:
    call start_mes
    call user_input
    call output_mes
    call exit

start_mes:
    mov eax, 4
    mov ebx, 1
    mov ecx, askAgeMessage
    mov edx, askAgeMesSize
    int 0x80
    ret

user_input:
    mov eax, 3
    mov ebx, 1
    mov ecx, age
    mov edx, 4
    int 0x80
    ret

output_mes:
    push age            ;pushing string age for atoi
    call atoi           ;calling function to convert input characters into integer
    cmp eax, 18         ;we're comparing eax cause output of atoi is stored in eax
    mov eax, 4
    mov ebx, 1
    jl not_adult        ;if age < 18
    jge adult           ;else age >= 18
    
not_adult:
    mov ecx, notAdultMessage
    mov edx, notAdultMesSize
    int 0x80

adult:
    mov ecx, adultMessage
    mov edx, adultMesSize
    int 0x80

exit:
    mov eax, 1
    mov ebx, 0
    int 0x80

section .data
    askAgeMessage dq 'What is your age?', 0x0a
    askAgeMesSize equ $ - askAgeMessage
    adultMessage dq "you're an adult.", 0x0a
    adultMesSize equ $ - adultMessage    
    notAdultMessage dq "you're not an adult yet", 0x0a
    notAdultMesSize equ $ - notAdultMessage    

section .bss
    age resb 4
