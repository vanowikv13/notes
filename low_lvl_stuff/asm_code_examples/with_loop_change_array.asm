global _start
section .data
    arr dq "xddd", 0x0a     ;our text
    len equ $ - arr         ;length of arr

_start:
    mov ebx, 0              ;loop index start 
    mov ecx, 4              ;interation amount
    push 'x'                ;push argument on stack
    call change_arr_data
    pop ebx                 ;pop 'x' from the stack 
    
    ;sending data to stdout
    mov eax, 4              ;sys_write call
    mov ebx, 1              ;file descriptor (stdout)
    mov ecx, arr            ;address to start of the message    
    mov edx, len            ;amount of bytes to write out    
    int 0x80                ;calling kernel for stdout

    ;exit program
    mov eax, 1
    mov ebx, 0
    int 0x80

change_arr_data:
    mov eax, [esp+4]        ;move argument to eax
    mov [arr+ebx], eax      ;change arr value to byte x
    inc ebx                 ;ebx+=1
    cmp ebx, ecx            ;compare eax with ebx
    jl change_arr_data      ;jump if eax < ebx to change_arr_data
    ret
