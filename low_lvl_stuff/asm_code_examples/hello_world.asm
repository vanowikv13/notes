section .text
    global _start:  ;must be declared for linker (ld)

_start:
    mov edx, len    ;message length (amount of bytes)
    mov ecx, msg    ;message pointer to start
    mov ebx, 1      ;file descriptor (stdout)
    mov eax, 4      ;system call number (system_write call)
    int 0x80        ;call kernel (interupt to make system call to kernel)
    
    ;exit program with status 0
    mov eax, 1      ;system call number (sys_exit)
    mov ebx, 0      ;0 our exit status of program
    int 0x80        ;call kernel

section .data 
msg db 'Hello World!', 0x0a ;string to be printed, 0x0a - '\n'
len equ $ - msg    ;length of the string
