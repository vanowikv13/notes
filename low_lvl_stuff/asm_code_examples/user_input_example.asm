section .bss           ;Uninitialized data
   num resb 5
	
section .text          ;Code Segment
   global _start
	
_start:                ;User prompt
   ;Read and store the user input
   mov eax, 3           ;reading options
   mov ebx, 2           ;stdin
   mov ecx, num         ;where write user input
   mov edx, 5           ;5 bytes (numeric, 1 for sign) of that information
   int 80h
	
   ;Output the number entered
   mov eax, 4           ;writing options
   mov ebx, 1           ;stdout
   mov ecx, num         ;what to output
   mov edx, 5           ;amount of bytes to write out
   int 80h
    
   ; Exit code
   mov eax, 1
   mov ebx, 0
   int 80h
