#include <iostream>
#include <cstdint>
#include <cstring>

//https://stackoverflow.com/questions/3318410/pragma-pack-effect
//#pragma pack(1) //make packing data nad not adding padding if not add
// size of structure would be 9 but it's 12
typedef struct {
    int8_t a; //1 byte + 3 bytes padding
    int b; //4 bytes
    long c; //4 bytes
}MTestStruct; //total size 12

#pragma pack(1)
typedef struct {
    int8_t a; //1 byte
    int b; //4 bytes
    long c; //4 bytes
}MTestStructPack; //total size 9

extern MTestStructPack* A() asm("_A");
extern MTestStruct* B() asm("_B");

template<class T>
void printStructureBytes(T test, size_t n) {
    uint8_t arr[sizeof(T)];
    memcpy(&arr, &test, sizeof(T));
    for(size_t i = 0; i < n; i++) {
        printf("%x ", *(arr+i));
    }
    printf("\n");
}

void func() {
    //std::cout << sizeof(MTestStruct) << std::endl;
    MTestStruct testStruct;
    testStruct.a = 0x1;
    testStruct.b = 0x22222222;
    testStruct.c = 0x33333333;
    //In memory: 0xffb16807: 0x22222201	0x33333322	0xXXXXXX33	0xXXXXXXXX
    //    +1   : 0xffb16808: 0x22222222	0x33333333	0xXXXXXXXX	0xXXXXXXXX
    //    +5   : 0xffb1680c: 0x33333333	0xXXXXXXXX	0xXXXXXXXX	0xXXXXXXXX

    printStructureBytes(testStruct, 12);
}

void sovleB() {
    //setting in little endian
    uint8_t structPack[12] = { 0x41, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x3, 0x0, 0x0, 0x0};

    MTestStruct test;
    memcpy(&test, &structPack, sizeof(MTestStruct));
    std::cout << test.a << ", " << test.b << ", " << test.c  << std::endl;

    printStructureBytes(test, 12);
}

void solveA() {
    //setting in little endian
    uint8_t structPack[9] = { 0x41, 0x2, 0x0, 0x0, 0x0, 0x3, 0x0, 0x0, 0x0};

    MTestStructPack test;
    memcpy(&test, &structPack, sizeof(MTestStructPack));
    std::cout << test.a << ", " << test.b << ", " << test.c  << std::endl;

    printStructureBytes(test, 9);
}

void sovleB1() {
    //setting in little endian
    uint8_t a = 0x41; //A
    int b = 2;
    long c = 3;

    uint8_t structPack[12];
    memcpy(structPack, &a, sizeof(uint8_t));
    memcpy((structPack+4) , &b, sizeof(int));
    memcpy((structPack+8) , &c, sizeof(long));

    for(size_t i = 0; i < 12; i++)
        printf("%x ", *(structPack+i));
    printf("\n");

    MTestStruct test;
    memcpy(&test, &structPack, sizeof(MTestStruct));
    std::cout << test.a << ", " << test.b << ", " << test.c  << std::endl;
    printf("\n");
}

int main() {
    //func();
    std::cout << "A in Cpp:" << std::endl;
    solveA();

    std::cout << "Assembly solve A: " << std::endl;
    MTestStructPack testPack = *(A());
    printStructureBytes(testPack, sizeof(MTestStructPack));
    std::cout << testPack.a << ", " << testPack.b << ", " << testPack.c  << std::endl;

    std::cout << std::endl << "B in Cpp : " << std::endl;
    sovleB1();

    std::cout << "Assembly solve B: " << std::endl;
    MTestStruct test = *(B());
    printStructureBytes(test, sizeof(MTestStruct));
    std::cout << test.a << ", " << test.b << ", " << test.c  << std::endl;
    return 0;
}