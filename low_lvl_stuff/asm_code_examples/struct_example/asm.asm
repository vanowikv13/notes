
section .text
global _A
global _B

_B:
    mov [XB], BYTE 0x41
    mov [XB+4], BYTE 0x2
    mov [XB+8], BYTE 0x3
    lea eax, XB
    ret

_A:
    mov [XA], BYTE 0x41
    mov [XA+1], BYTE 0x2
    mov [XA+5], BYTE 0x3
    lea eax, XA
    ret

section .bss
    XB: resb 12 ;space reserved in bss has default values 0
    XA: resb 9 ;space reserved in bss has default values 0