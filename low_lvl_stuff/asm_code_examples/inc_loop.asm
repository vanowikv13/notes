global _start

section .text
_start:
    mov ebx, 1      ;start ebx at one
    mov ecx, 1      ;start index
    mov edx, 4      ;number of interaton -1
interation:
    add ebx, ebx    ;ebx += ebx
    inc ecx         ;ecx += 1
    cmp ecx, edx    ;ecx - edx = 0
    jl interation   ;jump to the interation if less (ecx < edx)
    mov eax, 1      ;sys_exit call
    int 0x80        ;call kernel
