section .data
x:                  ;array of 3 elements
    db 1
    db 2
    db 3
sum db 0

section .text
global _start
_start:
    mov eax, 0       ;index of array
    mov ebx, 3       ;array size
    mov ecx, 0       ;string sum
    call sum_arr
    add ecx, '0'     ;convert digit to corresponding character digit
    mov [sum], ecx
    call display
    mov eax, 1
    mov ebx, 0
    int 0x80

sum_arr:
    add ecx, [x+eax]
    inc eax
    cmp eax, ebx
    jl sum_arr
    ret

display:
    mov eax, 4
    mov ebx, 1
    mov ecx, sum
    mov edx, 1
    int 0x80
    ret
