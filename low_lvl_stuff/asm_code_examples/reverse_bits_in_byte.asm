section .text

global _fun

_fun:
    push ebp
    mov ebp, esp
    mov ecx, [esp+8] 
    mov bl, [ecx] ;a
    
    mov cl, 0x01   ;from lsb to msb
    mov dl, 0x80  ;from msb to lsb

    mov al, 0  ;r

    jmp _loop_reverse

_loop_reverse:
    test bl, cl

    jz _loop
    or al, dl   ;setting right in r var

    jmp _loop

_loop:
    test cl, 0x80
    jnz _ret_value

    test dl, 0
    jnz _ret_value

    shl cl, 1 ;cl+=1
    shr dl, 1 ;dl-=1

    jmp _loop_reverse
    

_ret_value: 
    ;return value is by default in eax

    ;uncoment if you want set pointer value to reverse value
    ;mov ecx, [esp+8]
    ;mov [ecx], al
    
    pop ebp
    ret

