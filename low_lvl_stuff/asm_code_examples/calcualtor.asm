global main
extern atoi         ;for converting str to int
extern printf       ;for printing integer

section .text:
main:
    call getting_data_and_printing_messages
    call make_operation
    call print_result
    call exit_program

;number1\n operation\n number2\n
getting_data_and_printing_messages:
    ;asking for a number1
    mov eax, 4
    mov ebx, 1
    mov ecx, askFirstNum
    mov edx, askFirstNumSize
    int 0x80    

    ;number1 input
    mov eax, 3
    mov ebx, 2
    mov ecx, firstNum
    mov edx, 32
    int 0x80

    ;asking for operation
    mov eax, 4
    mov ebx, 1
    mov ecx, aksForOperation
    mov edx, aksForOperationSize
    int 0x80    

    ;operation input
    mov eax, 3
    mov ebx, 2
    mov ecx, operation
    mov edx, 2    
    int 0x80

    ;asking for a number2
    mov eax, 4
    mov ebx, 1
    mov ecx, askSecondNum
    mov edx, askSecondNumSize
    int 0x80

    ;number2 input
    mov eax, 3
    mov ebx, 2
    mov ecx, secondNum
    mov edx, 32
    int 0x80
    ret

make_operation:
    push firstNum
    call atoi
    mov ebx, eax
    pop eax
    push secondNum
    call atoi
    pop ecx

    push ebx
    push eax
    
    cmp byte [operation], '+'
    je add_num
    cmp byte [operation], '-'
    je sub_num
    cmp byte [operation], '*'
    je mul_num
    cmp byte [operation], '/'
    je div_num

;operations
add_num:        ;+
    pop ebx
    pop eax
    add eax, ebx
    mov [resultNum], eax
    ret

sub_num:        ;-       
    pop ebx
    pop eax
    sub eax, ebx
    mov [resultNum], eax
    ret

mul_num:        ;*
    pop ebx
    pop eax
    mul ebx
    mov [resultNum], eax
    ret

div_num:        ;/
    pop ebx
    pop eax
    div ebx
    mov [resultNum], eax
    ret

print_result:
    push dword [resultNum]
    push resultMessage
    call printf
    add esp, 12
    ret

exit_program:
    mov eax, 1
    mov ebx, 0
    int 0x80

section .data
    askFirstNum dq 'Give first number.', 0x0a
    askFirstNumSize equ $ - askFirstNum
    askSecondNum dq "Give second number.", 0x0a
    askSecondNumSize equ $ - askSecondNum    
    aksForOperation dq "What operation? (+, -, /, *)", 0x0a
    aksForOperationSize equ $ - aksForOperation
    resultMessage db "Result is %i", 0x0a, 0x00
    resultNum dw 0.0

section .bss
    firstNum resb 32
    secondNum resb 32
    operation resb 1