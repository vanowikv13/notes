#include "TemperatureSensor.h"
#include <algorithm>
#include <unistd.h>
#include <iostream>
#include "pthread.h"


void TemperatureSensor::temperatureSensorsReadInterval() {
    int i = 0;
    sleep(1);
    while(i < this->maxReads) {
        this->temperature = -10. + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (35. - 10.)));
        this->notify();
        i++;
        usleep(this->microsecondsInterval);
    }
}

TemperatureSensor::TemperatureSensor() {}

void TemperatureSensor::startIntervalOnThread() {
    pthread_mutex_init(&(this->temperatureMutex), nullptr);
    int status = pthread_create(&threadId, NULL, TemperatureSensor::temperatureSensorsReadIntervalWrapper, this);
    if (status != 0) {
        std::cerr << "Could not create a thread" << std::endl;
        return exit(1);
    }
}

void TemperatureSensor::add(IObserver * observer) {
    this->observers.push_back(observer);
}

void TemperatureSensor::remove(IObserver * observer) {
    std::remove(this->observers.begin(), this->observers.end(), observer);
}

void TemperatureSensor::notify() {
    for (auto &x : observers)
        x->update();
}

float TemperatureSensor::getTemperature() {
    return this->temperature;
}

void* TemperatureSensor::temperatureSensorsReadIntervalWrapper(void * object) {
    reinterpret_cast<TemperatureSensor*>(object)->temperatureSensorsReadInterval();
    return object;
}

void TemperatureSensor::joinThread() {
    int retThreadStatus = pthread_join(this->threadId, nullptr);
    if(retThreadStatus != 0) {
        std::cerr << "Could not join thread." << std::endl;
        return exit(2);
    }
}
