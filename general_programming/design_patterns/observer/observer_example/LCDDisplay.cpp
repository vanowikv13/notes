#include "LCDDisplay.h"
#include <iostream>

void LCDDisplay::print() {
    std::cout << this->title << TS->getTemperature() << std::endl;
}

LCDDisplay::LCDDisplay(TemperatureSensor * ts, std::string title) {
    this->TS = ts;
    this->title = title;
}

void LCDDisplay::update() {
    this->print();
}