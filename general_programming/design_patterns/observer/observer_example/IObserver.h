#ifndef OBSERVER_IOBSERVER_H
#define OBSERVER_IOBSERVER_H


class IObserver {
public:
    virtual void update() = 0;
};


#endif //OBSERVER_IOBSERVER_H
