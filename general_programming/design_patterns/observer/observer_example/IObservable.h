#ifndef OBSERVER_IOBSERVABLE_H
#define OBSERVER_IOBSERVABLE_H

#include "IObserver.h"

class IObservable {
public:
    IObservable(){}
    virtual void add(IObserver *) = 0;
    virtual void remove(IObserver *) = 0;
    virtual void notify() = 0;
};

#endif //OBSERVER_IOBSERVABLE_H
