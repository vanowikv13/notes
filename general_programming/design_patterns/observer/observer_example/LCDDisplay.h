#ifndef OBSERVER_LCDDISPLAY_H
#define OBSERVER_LCDDISPLAY_H

#include <string>

#include "TemperatureSensor.h"
#include "IObserver.h"

class LCDDisplay : public IObserver {
    TemperatureSensor * TS;
    std::string title;

    //not required
    void print();
public:
    //required
    LCDDisplay(TemperatureSensor*, std::string title);
    void update() override;
};


#endif //OBSERVER_LCDDISPLAY_H
