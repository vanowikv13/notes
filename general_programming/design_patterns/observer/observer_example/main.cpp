#include <iostream>

//files are in example/ folder
#include "IObserver.h"
#include "IObservable.h"
#include "TemperatureSensor.h"
#include "LCDDisplay.h"

int main() {
    srand (time(NULL));

    TemperatureSensor * observable = new TemperatureSensor();
    IObserver * observer = new LCDDisplay(observable, "Temperature1 = ");
    observable->add(observer);
    IObserver * observer2 = new LCDDisplay(observable, "Temperature2 = ");

    observable->startIntervalOnThread();
    observable->add(observer2);
    observable->joinThread();
    return 0;
}
