#ifndef OBSERVER_TEMPERATURESENSOR_H
#define OBSERVER_TEMPERATURESENSOR_H

#include <vector>
#include <pthread.h>

#include "IObservable.h"

class TemperatureSensor : public IObservable {
    std::vector<IObserver*> observers;
    float temperature;
    int microsecondsInterval = 2000000; //2 seconds
    int maxReads = 2;

    //threads required objects
    pthread_mutex_t temperatureMutex;
    pthread_t threadId;

    //not required method by each observer
    void temperatureSensorsReadInterval();

    //pthread_create requires static function, so we wrap temperatureSensorsReadInterval with it
    static void* temperatureSensorsReadIntervalWrapper(void *);
public:


    TemperatureSensor();

    //required methods
    void add(IObserver *) override;
    void remove(IObserver *) override;
    void notify() override;

    //function used by observers to get temperature
    float getTemperature();

    //client is required to start a thread and wait for it
    void startIntervalOnThread();
    void joinThread();
};


#endif //OBSERVER_TEMPERATURESENSOR_H
