# Observer Design pattern
The idea behind this pattern is that we have two object one have some state (Observable object) and seconds (single or many) are the one that are interest when this state changes. To not making interested (Observers) in interval ask about state change but instead the observable object inform about state changes.


![](img/img3.jpg)
IObservable - interface for observable object
* add(IObserver) - adding new observer
* remove(IObserver) - remove observer
* notify() - notify observers

IObservable - interface for observer object
* update() - method that calls notify in IObservable to inform our observer about state changes.

Zero/One to many relationships means that each Observable can have zero or more observers.


![](img/img2.jpg)
The things that really differ from interface is that ConcreteObserver have access to ConcreteObservable object (It could be send to ConcreteObserver probably with constructor) this is because it's do not get any data from ConcreteObserver when update is call. So when Observable call notify -> update. Observer can call getState() to get actual value. This is solution let us define state object in concrete implementation without need for using any interface.

If we remove method getState() and remove relation from ConcreteObserver to ConcreteObservable and send our state in update method as an argument. We need to specify what kind of type it would be. In C/C++ languages argument in interface could be void* to make it more extendable.


Example:
![](img/img1.jpg)

Implementation in IObserver have pointer to the object that they observe to so when the TemperatureSensor object call notify they can call getTemperature() and get new state of value.


[Example project in C++](observer_example/)