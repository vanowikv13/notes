# Structures
* remember that struct in C++ and in C very differ
* struct in C do not have constructors and destructors and could not have methods
* declaration of structures and definition also differ
* in C struct we cannot have static variables (in C++ we can)

Here are some rules with use case for struct in C:
```cpp
    // basic definition with 2 variables
    struct name {
        int x;
    } var1, var2;

    // if we define struct in the way above we need to always define variable in a way below.
    struct name var3; 

    // definition with typedef name to String to not use in definition each time struct keyword
    typedef struct {
        char * str;
        size_t size;
        char c;
    } String;

    // from definition above we can define in the way below
    String string;

    // string initialization that can be done only at object creation not latter on.
    String string2 = {NULL, 0};
    // string2 = {NULL, 1}; // compiler error in C

    // wrong: str=0 and size=NULL
    String string3 = {0, NULL};

    // wrong: str=0 and size='c'
    String strin4 = {0, 'c'};

    // from above examples we can see that in this we we cannot initialize some variables without initializing ones before
    // But there is other way we can do this:
    String string1 = {.size=0, .c='c', .str=NULL};

    // In this case .c and .str is initialized with values defied and .size is set by default to 0 as all other not initialized field during this kind of initialization. This works the same as array initialization. Remember that padding is be only zeroed for .size field.
    String string5 = {.c='c', .str=NULL};

    // struct can latter only be reinitialized in this way
    String string5 = (String){.c='a', .str="ABCD"};

    // If you want to have compleatly zeroed string use either:
    String zero = {0};
    //or use: 
    String zero0;
    memset(&zero0, sizeof(zero0), 0x00);

    // if struct object definition is not defined in globals then it could have wrong values. To prevent it we can initialize our variables like above or initialize everything to zero with, here it initialize first field to 0 and the rest as default also to zero.
    String zero = {0};
```

### define structure from pointer
```cpp
    //to allocate previous defined structure on heap
    String * ptr = malloc(sizeof(String));
    *ptr = (String){.str=NULL, .size=0}; // in this case we could initialize after allocation (not like in stack memory)

    //if you want to just define a structure with with 0 you can use instruction below instead of this above.
    String * ptr = calloc(sizeof(String));
```

### comparison of structures
```cpp
String s1 = {.str="ABCD", .size=0, .c='A'};
String s2 = {.str="ABCD", .size=0, .c='A'};

// Line below will end-up with compile error, struct cannot be compare this way only field by field
// if(s1 == s2) puts("They're equal");
```
* struct should not also be compared with memcmp if struct have any padding (is not packed), because padding is usually not the same, when values in fields might be. So, it's better to compare struct field by field if struct is not packed.