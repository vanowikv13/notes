#include <stdio.h>
#include <stdlib.h>

void f1()
{
    printf("text1");
}

void f2()
{
    printf("text2");
}

#define HELPER do{f1(); f2();}while(0)
#define HELPER2 {f1(); f2();}

int main()
{
    // so do{...}while(0); enable us this kind of construction with defines
    // in C language in C++ this could be made in different ways and this way should be avoided.
    if(1)
        HELPER;
    else
        printf("ahaha");

    /**
    if(1)
        HELPER2;
    else // here will be error syntax
        printf("hahaha");

    **/
    return 0;
}
