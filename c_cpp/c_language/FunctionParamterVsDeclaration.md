#### Function with prototype and without prototype in C language
For example let's have a declaration:
```cpp
void f();
```

This declaration tell compiler there will be available somewhere latter on function with name f
Given function takes unspecified amount of parameters. Because of that compiler let us call it with any number of arguments and will not protect us from this.
This is in simple words function without prototype, which just do not have function parameter information specified.

Another example:
```cpp
void f(); // function declaration (without prototype)
void f1()
{
    printf("f1");
}

int main()
{
    f(1); // no compile error - one argument is placed
    f1(1, 2); // no compile error - two arguments are placed
}

void f(int x)
{
    printf("%d ", x);
}
```

Both function are gets called without any compile or runtime errors.
Output will be: 1 f1

So, compiler will not protect us with how many arguments function is called and if given arguments are correct. Lets illustrate it with another example:

```cpp
void f(); // function declaration (without prototype)

int main()
{
    f(1); // no compile error
}

void f(int x, int y)
{
    // also no compile error
    printf("%d\n", x, y); // trying to read second argument even if it's not put there will print random stack value.
}
```
The above code will compile and execute without any compiler errors.
So, it's always better do declare function with prototype in declaration, to prevent this kind of issues in the code and make it more secure.
Function declaration with prototype tell how many compiler can suspect amount of arguments to that function palaced in call.

```cpp
void f(void); // function declaration with prototype - amount of parameters specified
int main()
{
    f(1); // now it will give compiler error - expect non arguments
}

void f(int x) // also compile error - expect definition of function f without parameters
{
    printf("%d\n", x);
}
```

##### This scenario in C++
In C++ things are a little bit different there declaration shall always match the definition, so compiler always expect them to be equivalent.