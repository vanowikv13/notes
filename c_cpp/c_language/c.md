## C language good practices and random language topics notes

### Standard types
In C it's better to use types with **#include <stdint.h>** instead of standard integers.

What types are good for:
```cpp
int8_t, int16_t, int32_t, int64_t // for signed int
uint8_t, uint16_t, uint32_t, uint64_t // for unsigned int
float // 32 bits floating points number
double // 64 bits floating points number

int8_t //is perfect for single byte, which is not ascii char

char // single character variables
```
If you really can't use standard types above then you can use other types.

If you want to use maximum size integer variable in your system use:
```cpp
intmax_t var = 0; // depend of architecture could be different size
uintmax_t uvar = 0; // same as intmax_t but for unsigned int
```

### Pointer arithmetic and operations
* intptr_t (uintptr_t) is for holding int pointer depend of the platform could be 32-bit or 64-bit
* ptrdiff_t could be use to store pointers and making arithmetic on them.
* It's also better to use intptr_t for integer of system dependent size instead of long/int.

```cpp
#include <stdint.h>
#include <stddef.h>

int32_t arr[10];
int32_t * ptr = arr;
for (int32_t i = 0; i < 10; ++i)
    arr[i] = i+1;

ptr = (arr + 8); // move pointer to 9 element of arr
printf("%d\n", *ptr);

ptrdiff_t diff = (uintptr_t)ptr - (uintptr_t)arr;
// could also be: (void*)ptr - (void*)arr;
printf("%td\n", diff); // difference by bytes

diff = ptr - arr;
printf("%td\n", diff); //difference by types
uintptr_t p1 = 
```

### Max values
```cpp
// max int values
printf("%d\n", INT8_MAX); // to get min use INT8_MIN
printf("%d\n", INT16_MAX); // to get min use INT8_MIN
printf("%d\n", INT32_MAX); // to get min use INT8_MIN
printf("%ld\n", INT64_MAX); // to get min use INT8_MIN

// max uint values
printf("%u\n", UINT8_MAX);
printf("%u\n", UINT16_MAX);
printf("%u\n", UINT32_MAX);
printf("%lu\n", UINT64_MAX);
```

### Size and indexing
Instead of long and int we should be using for indexing arrays and keeping size of structure **size_t** which is uint64_t for x64. For signed size there is ssize_t which is int64_t for x64. Both size types on x86 we will be 32 bits size.

```cpp
size_t counter = 10; // good
int counter = 10; // bad

ssize_t getStatus(); // good
int getStatus(); // bad
```

### Static initialization of auto-allocated arrays
```cpp
// Bad
int64_t arr[64];
memset(arr, 0, sizeof(arr));

// Good
int64_t arr[64] = {0};
```

### Static initialization of auto-allocated structs
* Bad:
```cpp
struct thing {
    uint64_t index;
    uint32_t counter;
};

struct thing localThing;

void initThing(void) {
    memset(&localThing, 0, sizeof(localThing));
}
```

* Good:
```cpp
struct thing {
    uint64_t index;
    uint32_t counter;
};

// If you have padding like here this method will not 0 this padding
struct thing localThing = {0};
```

### Re-initialize of previous allocated struct
```cpp
struct thing {
    uint64_t index;
    uint32_t counter;
};

//you need to create variable with struct if struct were defined without typedef
static const struct thing zero = {0};

int main() {
    struct thing localThing = {.counter = 3};

    // re-initialize with zero
    localThing = zero;
    return 0;
}
```

You can make it better with:
```cpp
struct thing {
    uint64_t index;
    uint32_t counter;
};

int main() {
    struct thing localThing = {.counter = 3};

    // re-initialize with 0
    localThing = (struct thing){0};
    // or
    localThing = (struct thing){.index = 0, .counter = 0};
    return 0;
}
```

### Parameter pointer types
```cpp
// Bad
void processAddBytesOverflow(uint8_t *bytes, size_t len) {
    // S...
}

// Good - it's better case we can send char* and other types
// make it more flexible for programmer.
void processAddBytesOverflow(void *bytes, size_t len) {
    uint8_t *val = bytes;
    // S...
}
```

### Calloc instead of malloc
The main difference between malloc and callos is that calloc sets chunk of memory to zero-initializer buffer.

```cpp
int64_t * arr;

// Bad
arr = (int64_t*)malloc(10 * sizeof(int64_t));

// Good
arr = (int64_t*)calloc(10, sizeof(int64_t));
```

You can still use malloc if you initialize your buffer before you use it.

Remember that if you realloc after calloc. Use memset() to set values to 0. Usually it's better practice to not use memset and use it if you really do not have any better way.

Also instead of realloc you could use reallocarray which let's you prevent integer-overflow error If such an overflow occurs on multiplication,  reallocarray() returns NULL, sets errno to ENOMEM, and leaves the original block of memory unchanged.


### Implicit and explicit conversion
Implicit appears when we convert same size type or from smaller to larger types.

Explicit arrives when we convert from larger to smaller. Implicit type casting are doing automated if there is possibility. If explicit types visible conversion is needed.

### const pointers and pointer to const data
```cpp
char x = 'a';

// const pointer
// which means we cannot edit pointer value but 
// we can change where pointer is pointing to.
char* const constDataPtr = &x;

// pointer to const data
// which means we can edit pointer value and
// we can't change where pointer is pointing to.
const char* constDataPtr = &x;

// const pointer to const data
// which means we cannot edit pointer value and
// we can't change where pointer is pointing to.
const char* const constPointerToConstData = &x;
```

### extern
extern specify that variable declared with this keyword is outside the file. and we want to use it from there.

other.c
```cpp
int value = 5;

void changeValue() {
    value = 10;
}
```

main.c
```cpp
#include <stdio.h>

extern void changeValue();

int main() {
    //variables with extern could not be initialize
    extern int value;
    printf("%d\n", value); // 5
    changeValue();
    printf("%d\n", value); // 10
}
```

It's good practice to declare extern function in .h files that are defined in some .c files to latter easily linked them. If we use it with variable than all the file will be able to use this variable defined somewhere else (but it's important that this variable need to be defined.)

### strings
Funcs:
* strcpy(char* s1, char* s2); - Copies string s2 into string s1.
* strcat(char* s1, char* s2); - Concatenate string s2 onto the end of s1
* strlen(char* s1); - return the length of s1
* strcmp(char* s1, char* s2); - Returns 0 if s1 and s2 are the same; less than 0 if s1<s2; greater than 0 if s1>s2.
* strchr(char* s1, char c); - Return the pointer to first occurrence char c in s1
* strstr(char* s1, char* s2); - Return the pinter to first occurrence of string s2 in string s1


```cpp
int main() {
    // 10 chars initialized nothing is set (not even a null byte)
    char arr1[10];
    // 10 chars exactly, initialized, and there values and null bytes
    char arr2[10] = { 'a', 'b', 'c' };
    // there bytes only a, b and c not null byte
    char arr3[] = { 'a', 'b', 'c' };
    // four bytes with null byte initialized
    char arr4[] = "abc";
}
```

### Linux standard calls
```cpp
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
  char *buff = malloc(sizeof(char)*5);
  //0 = STDIN
  //1 = STDOUT
  //2 = STDERR
  char * s = "Enter 5 characters.";
  write(1, s, strlen(s));
  read(0, buff, 5);
  write(1, buff, 5);

  free(buff);
  return 0;
}
```

### Macros
Macros are executed before a compilation, here are some of them:
* #define x y - replace in code x with y (not giving result only replace)
* #undef - undefine a processor macro
* #ifdef - return true is this macro is defined
* #ifndef - return true is this macro is not defined
* #if - test if condition on compilation is true
* #else - else of if
* #elif - else if
* #endif - end of if
* #error - prints error message on stderr


Special macros
```cpp
#include <stdio.h>

int main() {
   printf("File :%s\n", __FILE__ );
   printf("Date :%s\n", __DATE__ );
   printf("Time :%s\n", __TIME__ );
   printf("Line :%d\n", __LINE__ );
   printf("ANSI :%d\n", __STDC__ );
}
```

Defines macros:
```cpp
#define mult(a, b) a*b
printf("%d\n", mult(12, 13));

// # convert variable to string
#define  pair(a, b)  printf("("#x ", " #y ")\n")
pair(First, SECOND);


// ## - token parsing operation
#define parsing(i) printf("var" #i " = %d\n", var##i)
int var12 = 123;
parsing(12);

//output:
// var12 = 123

// defined() - checking if expression is defined using #define
#if !defined (AGE)
   #define AGE 50
#endif
printf("Age: %d\n", AGE);

//define giving us MAX of two values
#define MAX(x,y) ((x) > (y) ? (x) : (y))
x = MAX(1, 2) => x = ((1) > (2) ? (1) : (2))
```

### static
Where static is used and how:
* static in function keep it's value between a calls but we cannot access it outside of function and duration of it will be till end of a program.
```cpp
int counter() {
    //static variable is  initialized only once
    //so line bellow will be only once executed.
    //this variable will be in .bss section in asm
    static int x = 0;
    x++;
    return x;
}
```

* A static global variable or a function is "seen" only in the file it's declared in (not visible to linker). Duration of it will be till end of a program. Global constant also have static linkage, so to use them in other files declare this variable with those files with extern keyword.

* Global static function or variable in header file, after include to source file gets individual copy of static variable, function and it's only visible there, where included.

C++ static:
* A static variable in class in initialized once outside of class and is accessible across all objects of class. The lifetime of it is program life.

* A static function in class could be called without creating a object.

Reference: https://stackoverflow.com/questions/15235526/the-static-keyword-and-its-various-uses-in-c

### Difference between function() and function(void) in C
The difference is pretty devastating:
```cpp
void fun() {  }  
int main(void) 
{ 
    //this doesn't throw any errors and compile correctly
    fun(10, "GfG", "GQ"); 
    return 0; 
} 
```

```cpp
void fun(void) {  }  
int main(void) 
{ 
    //here we will get ERROR: too many arguments to function ‘fun’
    fun(10, "GfG", "GQ"); 
    return 0; 
} 
```

In C++ this will work after as it have void inside.

### Swap program standard file descriptor with own file
```cpp
//standard out stream is closed and it's now associated with output file
FILE * output = freopen("output", "w", stdout);

//standard input stream is closed and it's now associated with input file
FILE * input = freopen("input", "r", stdin);
int age;
scanf("%d", &age);
printf("Your age is: %d\n", age);
fclose(output);
fclose(input);
```

Input file:
```
12
```

After run in output file should be our program output

### How to deal with lose of precision in float/doubles
```cpp
double x = 0.3 + 0.6 + 0.1;
printf("%.20f\n", x); //output: 0.99999999999999988898
//to check actually if the number is 1 instead of == operator we should use:
if(abs(x - 1.0) < 1e-9)
    printf("We're correct\n");

//where 1e-9 is 1^(-9)
```

### String initialization
By default when creating string literal compiler will add to the given string a '\0' null byte. Which is character representing string end. Function like strlen() from standard uses this character to count for example string length.

#### char* array vs char array[]
```cpp
// Create a string literal (mostly in .ro section),
// then create pointer strPtr and assign to it address of that string literal
char * strPtr = "Hello World!"; // in this case pointer should be always const char * because content under should not be ever modified.

// When array created in local scope:
// Create a string literal (either in .ro or in .text section)
// Create array of string literal size
// Copy all bytes from string literal position to our strArr

// When array created in global scope:
// If above definition will land in global scope, then there will be only one copy of code in .data section (in most cases)
char strArr[] = "Hello World!"
```

#### Creating array without null byte
```cpp
// This will contain null byte, because we're assigning to ptr a string literal, which by default have null byte set.
const char * strPtr = "Hello World!"; // string literals should be of const type always

// This will contain null byte, because like above string literal have null byte set, and if array size is not specified, then whole string is copied to it (which include also null byte).
char strArr[] = "Hello World!"

// This will not contain null byte, because in global scope//
// there is created buffer of size 5 and only 5 bytes are copied from string literal
char strArr[5] = "ABCDEF";

// This will contain null byte, not because full string literal could be copied.
char strArr[6] = "ABCDEF";
```

#### Array missing elements initialization
```cpp
int arr[4] = {1, 2};
// If any array elements initialization is missed during initialization with {} it will
// set missing values to 0, So this {1, 2} is in reality {1, 2, 0, 0}
// This mechanism happen for all type of initialization with {} even for strings literals
// assigned to arrays ptr with size specified if size is larger or struct etc.
```