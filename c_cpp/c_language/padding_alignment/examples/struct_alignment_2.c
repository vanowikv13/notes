#include <stdio.h>

typedef struct x
{
    char a; // 1 byte
    char b; // 1 byte
    int c; // 4 bytes
} X;

// Type your code here, or load an example.
int main(int num) {
    X xobj;
    xobj.a = 0x41; // 'A'
    xobj.b = 0x42; // 'B'
    xobj.c = 0x43444500; // "CDE\0" (in little endian "\0EDC")
    printf("Sizeof of X: %d\n", sizeof(X)); // on x86 output should be 8 bytes
}