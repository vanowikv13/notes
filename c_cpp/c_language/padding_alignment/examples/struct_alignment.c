#include <stdio.h>

typedef struct x
{
    char a;
    int b;
    char c;
} X;

// Type your code here, or load an example.
int main(int num) {
    X xobj = {0};
    xobj.a = 1;
    xobj.b = 0x41424344;
    xobj.c = 2;
    printf("Sizeof of X: %d\n", sizeof(X));
}