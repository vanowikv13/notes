#include <stdio.h>

typedef struct __attribute__((__packed__)) x
{
    char a; // 1 byte
    int b; // 4 bytes
    char c; // 1 byte
} X; // with packed given struct size is 8

// Type your code here, or load an example.
int main(int num) {
    X xobj;
    xobj.a = 0x41; // 'A'
    xobj.b = 0x42; // 'B'
    xobj.c = 0x43444500; // "CDE\0" (in little endian "\0EDC")
    printf("Sizeof of X: %d\n", sizeof(X)); // on x86 output should be 8 bytes
}