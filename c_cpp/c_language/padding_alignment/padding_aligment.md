# Padding in C/C++ language

In C/C++ by default all struct members are aligned to 4 bytes (address % mod 4 == 0),
given alignment additional space is called **padding**. In most computer architecture,
processor could fetch values from memory in one instruction if they're aligned.
In 0x86 bytes should be aligned to 4 bytes, while in 0x64 it should be to 8 bytes.
If members are not aligned it can take process to fetch data from memory more than one instruction.

### Example with memory analysis
```cpp
#include <stdio.h>

typedef struct x
{
    char a; // 1 byte
    int b; // 4 bytes
    char c; // 1 byte
} X;

// Type your code here, or load an example.
int main(int num) {
    X xobj = {0};
    xobj.a = 0x41; // 'A'
    xobj.b = 0x42434400; // "BCD"
    xobj.c = 0x45; // 'A'
    printf("Sizeof of X: %d\n", sizeof(X)); // on x86 output should be 12 bytes
}
```

If you run given example output for it will be "Size of X: 12".
This is how it's look in the memory:

```
    -------------------------------------------------
    |aaa|xxx|xxx|xxx|ccc|ccc|ccc|ccc|bbb|xxx|xxx|xxx|
    
    aaa - is a memory cell for a variable in memory
    ccc - is memory cells for c variable in memory
    bbb - is memory cell for b variable in memory
    xxx - is padding in memory
```

From given example padding is added to align struct members, so they could be fetched with single instruction.

In reality this struct is:
```cpp
typedef struct x
{
    char a; // 1 byte
    char padding_a[3]; // inserted by compiler for alignment
    int b; // 4 bytes
    char c; // 1 byte
    char padding_b[3]; // inserted by compiler for alignment 
} X;
```


### Another example with GDB analysis
```cpp
#include <stdio.h>

typedef struct x
{
    char a; // 1 byte
    char b; // 1 byte
    int c; // 4 bytes
} X;

// Type your code here, or load an example.
int main(int num) {
    X xobj;
    xobj.a = 0x41; // 'A'
    xobj.b = 0x42; // 'B'
    xobj.c = 0x43444500; // "CDE\0" (in little endian "\0EDC")
    printf("Sizeof of X: %d\n", sizeof(X)); // on x86 output should be 8 bytes
}
```

At first xobj on stack look like:
```bash
(gdb) x /2x &xobj
0xffffcd78:     0xffffce4c      0x56556221
```

After all fields are set to values:
```bash
(gdb) x /2x &xobj
0xffffcd78:     0xffff4541      0x42434400
```

Bytes at each addresses, when address of xobj is 0xffffcd78:
```bash
0xffffcd78: 0x41
0xffffcd79: 0x42
0xffffcd7a: 0xff
0xffffcd7b: 0xff
0xffffcd7c: 0x00
0xffffcd7d: 0x45
0xffffcd7e: 0x44
0xffffcd7f: 0x43
```

Struct in memory is written in little endian and padding in this case is visible it's both 0xFFFF bytes, which is complete random value and it could be any value padding set to if struct obj is made on stack.

Also address of int and chars is aligned.
0xffffcd78 % 4 == 0 and 0xffffcd7c % 4 == 0

Usually members are not aligned if their size and their neighbors is in 4 bytes range. (on 0x86 on 0x64 this might differ). Here struct size is 8 bytes, because two chars are next to each other and they're less than 4 bytes. Next field c (int) is 4 bytes, so this is the reason why padding is added.

The main rule behind it is int should be aligned to address divisible by 4, because we want to read it at once, that is why padding is added to chars.

In reality this struct looks like:
```cpp
typedef struct x
{
    char a; // 1 byte
    char b; // 1 byte
    char padding[2]; // inserted by compiler for alignment
    int c; // 4 bytes
} X;
```

In first example the same struct have 12 bytes this is because compiler could not reorder fields, so firstly it see a (char), then b (int), which a + b size is larger than 4. Padding is added to a, then padding need to be added to c.

```cpp
typedef struct x
{
    char a; // 1 byte
    char padding_a[3]; // inserted by compiler for alignment 
    int b; // 4 bytes
    char c; // 1 byte
    char padding_c[3]; // inserted by compiler for alignment
} X;
```

Here rule about int also apply, so that is why padding_a is added.Padding for c is added, because struct size should be divisible by the largest individual fields, which in this example is b (int) -> 4 bytes (12 % 4 == 0)

This is mostly useful in case there is array of structs, because adding this padding to have size of struct divisible by the largest individual field, let us in case of array have all the members at correct offset.

```cpp
typedef struct x
{
    char a; // 1 byte
    char padding_a[3]; // inserted by compiler for alignment 
    int b; // 4 bytes
    char c; // 1 byte
    char padding_c[3]; // inserted by compiler for alignment
} X;

X xarr[2];
xarr[0].b = 0; // address of this field is divisible by 4
xarr[1].b = 0; // and because padding is added to c, address of this is divisible by 4
```

### The rules:
Alignment is created to let processor fetch and store data types in single instruction.

Types and alignment:
```
byte - do not matter at what address is started (int8_t)
word - should start at even address (int16_t)
dword - should start at address divisible by 4 (int32_t)
qword - should start at address divisible by 8 (float, int64_t)
```

Remember that not only padding is added to struct, but it might be added also to unions, globals and etc.

### struct packing
If you do not want to have your struct to have padding, then you could inform compiler about it with pack option.

```cpp
#include <stdio.h>

typedef struct __attribute__((__packed__)) x
{
    char a; // 1 byte
    int b; // 4 bytes
    char c; // 1 byte
} X; // with packed given struct size is 8

// Type your code here, or load an example.
int main(int num) {
    X xobj;
    xobj.a = 0x41; // 'A'
    xobj.b = 0x42; // 'B'
    xobj.c = 0x43444500; // "CDE\0" (in little endian "\0EDC")
    printf("Sizeof of X: %d\n", sizeof(X)); // on x86 output should be 8 bytes
}
```

References:
* https://stackoverflow.com/questions/45463701/why-is-the-compiler-adding-padding-to-a-struct-thats-already-4-byte-aligned
* http://www.catb.org/esr/structure-packing/