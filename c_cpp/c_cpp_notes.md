# Cpp languages features

## Mutable keyword

```cpp

class Animal {
public:
    mutable int age;
    std::string name;

    Animal(int a, std::string s): age(a), name(std::move(s)) {}

    virtual void hello() const {
        std::cout << "Hello, I'm Animal, and my name is " << name << std::endl;
    }
};

int main() {
    const Animal an(12, "Name");
    //without mutable we could not do this
    an.age = 13;
}
```

## Try catch
```cpp
#include <iostream>
#include <exception>
#include <cstdint>


try {
    const size_t size = INT32_MAX;
    int* arr= new int[size];
} catch(std::exception &e) {
    std::cout << "Error: " << e.what() << std::endl;
} catch(...) {
    std::cout << "Other exceptions" << std::endl;
}
```

* Own exception:
```cpp
#include <iostream>
#include <exception>
#include <cstdint>

class myexception: public exception
{
  virtual const char* what() const throw()
  {
    return "My exception happened";
  }
};


try {
    throw myexception();
} catch(myexception &e) {
    std::cout << "Error: " << e.what() << std::endl;
} catch(...) {
    std::cout << "Other exceptions" << std::endl;
}
```


## In which way constructors and destructors works??
```cpp
class Animal {
public:
    mutable int age;
    std::string name;

    Animal(int a, std::string s): age(a), name(std::move(s)) {
        std::cout << "Animal constructor" << std::endl;
    }

    ~Animal() {
        std::cout << "Animal destructor" << std::endl;
    }
};

class Tiger : public Animal {
public:
    Tiger(int a, std::string s) : Animal(a, std::move(s)) {
        std::cout << "Tiger constructor" << std::endl;
    }
    ~Tiger() {
        std::cout << "Tiger destructor" << std::endl;
    }
};

int main() {
    Tiger tn(12, "Name");
    /* OUTPUT:
        Animal constructor
        Tiger constructor
        Tiger destructor
        Animal destructor
    */
}

```

From example above we can see that destructor starts in reverse order comparing to constructors. It's also important to remember if class have multiple parent constructors will start in order from definition in class.
```cpp
//constructors will start Predator, Animal, Tiger
//destructors will start Tiger, Animal, Predator
class Tiger : public Predator, Animal
//...
```

## final class
Final class could not have any children.
```cpp
class Class final {
public:
    Class(){}
};

//error 
//class Deriv : public Class {};
```

## assignment operator vs copy constructor
A copy constructor is used to initialize a previously uninitialized object from some other object's data.
```cpp
Animal(const Animal& animal) : age(animal.age) {}
```

An assignment operator is used to replace the data of a previously initialized object with some other object's data.
```cpp
Animal& operator=(const Animal& animal) {
    age = animal.age;
    return *this;
}
```

## shred_ptr - new smart pointers
This pointer let have multiple owners and when last people delete it then object is deleted.

```cpp
#include <memory>

std::shared_ptr<int> sp(new int(12));
auto sp2 = sp;
//count how many owners pointer have
std::cout << sp2.use_count() << std::endl;
//delete this owner
sp2.reset();
if(sp2 == nullptr)
    std::cout << "nullptr" << std::endl;
std::cout << sp2.use_count() << std::endl;
std::cout << *sp << std::endl;
```

## unique_ptr
This pointer like shared_ptr can't have multiple owners, but only one.

```cpp
#include <memory>

std::unique_ptr<int> up (new int (10));
std::cout << *up << std::endl;
auto &up2 = up;
up.reset();
if(up2 == nullptr)
    std::cout << "nullptr" << std::endl;

//to get unique_ptr from function make it this way
std::unique_ptr<int> getUniqInt() {
    return std::make_unique<int>(1);
}

//transfer from one uniq_ptr to another
std::unique_ptr<int> up4 (new int (10));
std::unique_ptr<int> up5 = std::move(up4);

//u4 should here be nullptr
if(up4 == nullptr)
    std::cout << "nullptr" << std::endl;
```

## Default things in class after declaration
In C++03 there are 4:

Default constructor: Declared only if no user-defined constructor is declared. Defined when used

Copy constructor - declared only if the user hasn't declared one. Defined if used

Copy-assignment operator same as above

Destructor same as above

In C++11 there are two more:

Move constructor
Move-assignment operator

## lvalue and rvalue &&
lvalue:
* it's has address in memory
* lvalue is addressable
* can't be moved

rvalue:
* opposite to lvalue
* it's only on the right side of assignment such as literal, intended to be non modifiable.
* it's mostly temporarily objects

```cpp
//x - lvalue, 5 - rvalue
int x = 5;
//x - lvalue, (x + 1) - rvalue
int x = x + 1;
//a - lvalue, Animal() - rvalue
Animal a = Animal();

int i = 19;
int &i1 = i;
//ERROR
//int &i2 = 10;
const int &i3 = 10;

//so if you want to make function accept lvalue and rvalue use const

//this function accept lvalue and rvalue
void func(const type &x) {

//rvalue
int && x = 10;
}
```

## std::move
std::move convert lvalue to rvalue - now we do not make another copy of variable s
```cpp
class Animal {
public:
    mutable int age;
    std::string name;
    Animal(int a, std::string s): age(a), name(std::move(s)) {}
    virtual void hello() = 0;
};

class Tiger :  public Animal{
public:
    int xd = 12;
    Tiger(int a, std::string s) : Animal(a, std::move(s)) {
    }
};
```

## Virtual destructors
The virtual keyword in destructor is useful with polymorphisms types. It's protect our class to call their destructors even if they are used with base class pointer.

In Effective C++:
```
If a class has any virtual function, it should have a virtual destructor, and that classes not designed to be base classes or not designed to be used polymorphically should not declare virtual destructors.
```

```cpp
#include <iostream>

class Animal {
public:
    // no virtual destructor
    ~Animal() {
        std::cout << "Animal destructor" << std::endl;
    }
};

class Tiger : public Animal {
public:
    ~Tiger() {
        std::cout << "Tiger destructor" << std::endl;
    }
};

int main() {
    Animal * a = new Tiger();
    delete a;
    return 0;
}

//OUTPUT:
//Animal destructor

//OUTPUT with virtual destructor in Animal:
//Tiger destructor
//Animal destructor
```

## Volatile keyword
Volatile keyword is used to protect variables from compiler optimization, when we know it might be used/changed without action visible in the code (for compiler).

For example when you interact with the hardware that can change the value itself. Or when there is another thread/process running that also uses it.

So it means if variable have volatile keyword compiler is not permitted to cache it in a register. So if variable is primitive type and it's used for example between treads it should be set as volatile.

In embedded could be used to:
    - tell compiler to not optimize function that uses volatile
    - Used to get access to external memory location RAM, Flash to control memory mapped devices, CPU registers and others.


## Inheritance with visibility operations in C++
```cpp
class BaseClass
{
    //...
};

class inheritedClass : visibility_operator BaseClass
{
    //...
};
```

visibility_operators:
1. Public - means all inherited things from BaseClass will remain with the same visibility as there.
2. Protected - means all inherited things from BaseClass, which are not public will remain the same and public will be change to protected.
3. Private - all inherited things from BaseClass will be private in inherited one.


## Public, Protected, Private visibility operators
Public - members are accessible from outside the class.
Protected - members cannot be accessed from outside the class,
but can be used in inherited class.
Private - members cannot be accessed from outside the class.

## Difference between pointer and reference
1. Pointer can be re-assigned and reference cannot. It need to be assigned at moment of creation (like const).
    ```cpp
    char x = 'a';
    char y = 'b';

    char &ref = x; // legal
    char &ref = y; // compile error
    ```

2. Pointer is visible as another variable it's have it's own memory address that can be taken with & and size can be measured with sizeof, while reference is just an alias to some variable and it's do not have it's address at least it's not visible from the C++ level and it's size could not be measured with sizeof only object under it. (so it's just another name for the same variable)
    ```cpp
    char x = 'a';
    char &ref = x; // ref is now alias for x

    char * ptr = &ref; // ptr will be assigned to address of x

    printf("x size = %d, pointer size = %d\n", sizeof(ref), sizeof(ptr)); // size of ref is size of object it's set to

    // Output (on x86):
    // x size = 1, pointer size = 4
    ```

3. You couldn't have reference object to reference, while you can have pointer to pointer.
    ```cpp
    int x = 5;
    int &ref = x;
    int &&ref = ref; // compiler error

    int *ptr = &x;
    int **pptr = &ptr; // legal
    ```

4. Pointer can be assigned to NULL, while reference must be assigned to existing object.
    ```cpp
    int &ref = NULL; // compiler error
    int *ptr = NULL; // legal
    ```

5. const reference can be used to point to rvalue objects.
    ```cpp
    const char &c = 'a'; // no compiler error
    ```
    In the above case compiler will firstly create space for variable 'a' then it will create space for reference and assign to it our object not in all cases but usually. If it's for example global then it might create just space for original object. Also it's very depend of optimization level. 

6. In low assembly level reference is still another cells in memory like a pointer, but it's just behave differently from the C++ layer, but under works just like a pointer.

## Literal constants suffixes

In C/C++ there are literal constant suffixes, which could tell what type has given constant literals. By default any integer is of type `int` like `19`, any character is of type `char` like `'a'` and any floating point number is `double` like `12.34`, here is full list of default types for values:

| type             | value         | int                                  |
| ---------------- | ------------- | ------------------------------------ |
| integer          | 10, 0, -5     | char                                 |
| character        | 'a', '\0'     | long                                 |
| floating point   | 5.5, 0.0      | double                               |
| boolean          | true, false   | bool                                 |
| string literal   | 'abcdef'      | C: char[ 7 ], C++: const char[ 7 ]   |
| ---------------- | ------------- | ------------------------------------ |

For string literal size is 7, because terminating null byte is included.

In C/C++ there are also suffixes that could change default types, by adding them next to a value. Here is list of them:

| Type     | suffix   | result type          |
| -------- | -------- | -------------------- |
| int      | U        | unsigned int         |
| int      | L        | long                 |
| int      | UL       | unsigned long        |
| int      | LL       | long long            |
| int      | ULL      | unsigned long long   |
| double   | F        | float                |
| double   | L        | long double          |
| -------- | -------- | -------------------- |

For types like short (uint16_t) or unsigned char (uint8_t) use appropriate cast on int literal constant.

```cpp
unsigned char x = (unsigned char)1;
short y = (short)2;
```


## Slicing

Arr is an array of X, not of pointers to X. When an object of type Y is stored in it, it is converted to X, and we lose the "Y part" of the object. This is commonly known as "slicing".

https://cppquiz.org/quiz/question/44?result=OK&answer=YX&did_answer=Answer
