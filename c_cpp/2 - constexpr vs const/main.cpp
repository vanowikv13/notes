#include <iostream>

constexpr int test() {
    return 2*2;
}

int test1() {
    return 2*2;
}

//function could not be constexpr cause non constexpr function bascic_stream used by std::cin
int test2() {
    int i = 0;
    std::cin >> i;
    return i;    
}

constexpr int* test3() {
    //variables always need to be defined in constexpr function
    int * ptr = nullptr;
    //ERROR we cannot use heap in constexpr function
    //ptr = new int(3);
    return ptr;
}

static constexpr int global = 13;

class constX {
public:
	static constexpr int statConst() {
		return global;
	}

	constexpr int constExprFunc() const{
		//d = 1; in const function we can't change the value
		return d;
	}

	//all initialized data must be LiteralType or constexpr
	constexpr constX(int da):d(da), id(test()) {}

    constexpr int getid() {
        return id;
    }

protected:
	int d;
	const int id;
};

int main() {
    //0. Function with constexpr and with LiteralValue
    //compile-time
    constexpr int t = test();
    //also compile time
    const int tC = test();

    //array allocation
    //with test1 it would be danger cause test1() will be called at run-time
    //here test is called at compile time so in assembly constand is palced instead of function call
    int arr[test()];

    //1. Function without constexpr but with LiteralValue
    //ERROR: non-consexpr function
    //constexpr int t1 = test1();
    //run-time
    const int t1C = test1();
    
    //2. User input
    //run-time
    const int t2C = test2();
    //ERROR - non-constexpr function
    //constexpr int t2 = test2();

    //3. Heap allocatioin (commented)
    constexpr int * t3 = test3();
    const int * t3C = test3();

    //4. Accessing global static
    int g = global; //here value of global is placed here inplace

    //5. Class with constexpr
    constX x(12);
    //ERROR: x was not declareted constexpr
    //constexpr int t4 = x.constExprFunc();

    //here we ok cause we don't care about if class object is constexpr, we care only about function, and return variable
	constexpr int t5 = constX::statConst();

    //6. Constexpr class instantion
	constexpr constX constExprInstantion(1);
    constexpr int t6 = constExprInstantion.constExprFunc();
    //ERROR: We need to have constexpr method specifier even in constexpr object instantion to initialize new variable with constexpr
    //constexpr int t7 = constExprInstantion.getid();

    //7. Const class instantion
    constexpr constX constInstantion(1);
    //still work with const
    constexpr int t7 = constInstantion.constExprFunc();
    //ERROR: We cannot call function that do not have const operand having constexpr is not enough
    //constInstantion.getid();
    return 0;
}