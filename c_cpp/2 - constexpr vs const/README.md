
# Day 2 - const vs constexpr

### constexpr in c++ language
constexpr tells the compiler that this expression results is a compile time constant value. So it can be used in places like array lengths, assigning to const variables. A constexpr specifier used in an object declaration or non-static member function (until C++ 14) implies const. A constexpr specifier used in a function or static member variable declaration implies inline.

### 1. A constexpr object must satisfy the following requirements:
1. Type of object must be [LiteralType](http://en.cppreference.com/w/cpp/concept/LiteralType)
2. It must be immediately initialized
3. Full-expression of its initialization must be a const expression

### 2. A constexpr function must satisfy the following requirements
1. Can't be virtual (until C++ 20) and coroutine (until C++ 20)
2. Returned type if any must be [LiteralType](http://en.cppreference.com/w/cpp/concept/LiteralType)
3. Each of its parameters must be [LiteralType](http://en.cppreference.com/w/cpp/concept/LiteralType) and variables must be initialized.
4. There exists at least one set of argument values such that an invocation of the function could be an evaluated sub-expression of a core constant expression (for constructors, use in a constant initializer is sufficient) (since C++14). No diagnostic is required for a violation of this bullet.

More about function body: https://en.cppreference.com/w/cpp/language/constexpr

### 3. A constexpr constructor must satisfy the following requirements
1. Each arguments and parameters must be [LiteralType](http://en.cppreference.com/w/cpp/concept/LiteralType)
2. For constructor and destructor (since C++20), the class must have no virtual base classes.
3. Every non-varian or non static member must be initialized

### Const in c++ language
Const applies for variables, and prevents them from being modified in your code. It could be also used at compile-time and at run-time.

### 1. A const object must satisfy the following requirements:
1. Must be initializer in declaration.
2. Cannot be changed.
3. Can be initialize by constexpr function but it's always a compile time initialization.
4. Can be initialized at runtime.

### 2. A const in own data type
1. Data in const object must be initialize in constructor
2. Function can use const object only with const parameter after function
3. We can't cast const function for normal object
4. Object in const function can't be modify
5. Function can't be at the same time const and static


### Examples
```Cpp
//0. Function with constexpr and with LiteralValue
constexpr int test() {
    return 2*2;
}

//compile-time
constexpr int t = test();
//also compile time
const int tC = test();


//1. function without constexpr but with LiteralValue
int test1() {
    return 2*2;
}

//ERROR: non-constexpr function
//constexpr int t1 = test1();
//run-time
const int t1C = test1();

//constexpr is also good for array allocation
//with test1 it would be danger cause test1() will be called at run-time
//here test is called at compile time so in assembly constant is placed instead of function call
int arr[test()];

//2. user input
//function could not be constexpr cause non constexpr function basic_stream used by std::cin
int test2() {
    int i = 0;
    std::cin >> i;
    return i;    
}

//run-time
const int t2C = test2();
//ERROR - non-constexpr function
//constexpr int t2 = test2();

//3. heap allocation (commented)
constexpr int* test3() {
//variables always need to be defined in constexpr function, here is why we placed nullptr bellow
int * ptr = nullptr;
//ERROR we cannot use heap in constexpr function
//ptr = new int(3);
return ptr;
}

constexpr int * t3 = test3();
const int * t3C = test3();

//4. accessing static
static constexpr int global = 13;
int g = global; //here value of global is placed in place


//5, 6, 7 - classes
class constX {
public:
	static constexpr int statConst() {
		return global;
	}

	constexpr int constExprFunc() const{
		//d = 1; in const function we can't change the value
		return d;
	}

	//all initialized data must be LiteralType or constexpr
	constexpr constX(int da):d(da), id(test()) {}

    constexpr int getid() {
        return id;
    }

protected:
	int d;
	const int id;
};


//5. class with constexpr constructor and method
constX x(12);

//ERROR: x was not declared constexpr
//constexpr int t4 = x.constExprFunc();

//here we're ok cause we don't care about if class object is constexpr, we care only about function, and return variable
constexpr int t5 = constX::statConst();

//6. constexpr class instance
constexpr constX constExprInstance(1);
constexpr int t6 = constExprInstance.constExprFunc();
//ERROR: We need to have constexpr method specifier even in constexpr object instance to initialize new variable with constexpr
//constexpr int t7 = constExprInstance.getid();

//7. const class instance
constexpr constX constInstance(1);
//still work with const class instance
constexpr int t7 = constInstance.constExprFunc();

//ERROR: We cannot call function that do not have const operand having constexpr is not enough
//constInstance.getid();
```

### lninks:
* https://stackoverflow.com/questions/14116003/difference-between-constexpr-and-const
* https://pl.wikipedia.org/wiki/C%2B%2B11
* http://en.cppreference.com/w/cpp/language/constexpr