# Two dimensional arrays
## C++ way:
```cpp
int **ary = new int*[sizeY];
for(int i = 0; i < sizeY; ++i) {
    ary[i] = new int[sizeX];
}


// How to delete two dimensional array
for(int i = 0; i < sizeY; ++i) {
    delete [] ary[i];
}
delete [] ary;
```


## C way:
```cpp
// First approach
// creating two dimensional array of size [x, y]
int x = 5, y = 5;
int **arr = (int**)malloc(sizeof(int*)*x);
for(size_t i = 0; i < x; i++)
{
    arr[i] = (int*)malloc(sizeof(int) * y);
}

// clearing memory
for (size_t i = 0; i < x; i++)
{
    free(arr[i]);
}
free(arr);

// second approach ---------------------
int (*arr1)[x] = malloc(sizeof(int[x][y]));

free(arr1);

// third approach --------------
int **arr2 = malloc(sizeof(int) * x * y);

free(arr2);
```