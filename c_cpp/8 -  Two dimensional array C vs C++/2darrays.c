#include <stdlib.h>
#include <stdio.h>

void setTwoDimArrayOfIntToVal(int ** arr, int x, int y, int val)
{
    if(arr != NULL)
    {
        for(size_t i = 0; i < x; i++)
        {
            for (size_t j = 0; j < y; j++)
            {
                arr[i][j] = val;
            }
            
        }
    } else
    {
        return;
    }
}

void printTwoDimArrayOfInt(int ** arr, int x, int y)
{
    if(NULL != arr)
    {
        for (size_t i = 0; i < x; i++)
        {
            for (size_t j = 0; j < y; j++)
            {
                printf("%d", arr[i][j]);
            }
            puts("\n");
        }
        
    } else
    {
        return;
    }
}

int main()
{
    // First approach -----------------------------
    // creating two dimensional array of size [x, y]
    int x = 5, y = 5;
    int **arr = (int**)malloc(sizeof(int*)*x);
    for(size_t i = 0; i < x; i++)
    {
        arr[i] = (int*)malloc(sizeof(int) * y);
    }

    setTwoDimArrayOfIntToVal(arr, x, y, 2);
    printTwoDimArrayOfInt(arr, x, y);    


    // clearing memory
    for (size_t i = 0; i < x; i++)
    {
        free(arr[i]);
    }
    free(arr);
    // -----------------------------

    // second approach ---------------------
    int (*arr1)[x] = malloc(sizeof(int[x][y]));
    
    setTwoDimArrayOfIntToVal(arr1, x, y, 1);
    printTwoDimArrayOfInt(arr1, x, y);    

    free(arr1);
    // -----------------------------

    // third approach --------------
    int **arr2 = malloc(sizeof(int) * x * y);

    setTwoDimArrayOfIntToVal(arr1, x, y, 2);
    printTwoDimArrayOfInt(arr1, x, y);    

    free(arr2);
    // -----------------------------
    return 0;
}