# C++ language tutorial

This project was created to gather may knowledge about C++ and maybe help someone else.

## Chapters
[1. C++ casting (static_cast, dynamic_cast ...)](Day%201%20-%20casting/ReadMe.MD)

[3. Some of the C++ 11 concepts](Day%203%20-%20some%20concept%20of%20c++11/README.md)





## Great links

 - https://hackingcpp.com/
 - http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines