# Const in C++ vs Const in C using gcc x86

### What is actually const?
Const is a keyword that let us declare constant objects, which after initialization should not be possible to change those values.

### Const in C language
To compile I use gcc with version 10.2.0.
Command: gcc main.c -Wall -Wextra -pedantic -m32 -g -o test

1. Constant on stack (not global function scope):
    ```cpp
    void printInBytes(int8_t *ptr, size_t size) {
        for (size_t i = 0; i < size; ++i)
            printf("%02x", *(ptr+i));
        printf("\n");
    }

    int main() {
        //compile time known value
        const int x = 0xa;
        int8_t * ptr = (int8_t*)&x;
        printInBytes(ptr, sizeof(int));
        *ptr = 0xb; //próba zmiany stałej - undefined behavior
        printInBytes(ptr, sizeof(int));
        printf("%d\n", x);
        printInBytes(ptr, sizeof(int));
    }
    ```

    Running code above you can see that we're able to change the value of constant. Since it's undefined behavior in other compiler or some optimization this might not work this way. For Regular int* ptr works the same.

2. Global constant with value know on compile-time (C doesn't allow initialize globals on run-time)
    ```cpp
    const int y = 0xa;

    int main() {
        int8_t * ptr = (int8_t*)&y;
        printInBytes(ptr, sizeof(int));
        *ptr = 0xb; //error - trying to change in .rodata
        printInBytes(ptr, sizeof(int));
        printf("%d\n", y);
        printInBytes(ptr, sizeof(int));
    }
    ```

    Disassembly of re-initialization of constant:
    ![](img/c_2_1.png)

    When we make breakpoint at instruction: ```mov    BYTE PTR [eax],0xb```
    And printing address with value in gdb:

    ![](img/c_2_2.png)

    When we run in gdb:
    ```bash
    info file
    ```

    We can see that our value is in .rodata segment.
    ![](img/c_2_3.png)
    So it means that system send signal to stop a program.

1. Global value initialized at run-time (C doesn't support it)
    ```cpp
    int getFromUser() {
        int d;
        scanf("%d", &d);
        return d;
    }

    //ERROR: appears because we could not write .rodata
    //const int z = getFromUser(); //Initialized element is not compile time constant
    ```

### C Summary
From examples above we can observe that it's usually compiler responsibilities, to really take care of constant. If it's not global one.


### Const in C++ language
To compile I use gcc with version 10.2.0.
Command: g++ main.cpp -Wall -Wextra -pedantic -m32 -g -o test
1. Constant on stack (not global function scope) with Literal Type
    ```cpp
    void printInBytes(int8_t *ptr, size_t size) {
        for (size_t i = 0; i < size; ++i)
            printf("%02x", *(ptr+i));
        printf("\n");
    }

    int main() {
        //compile time known value
        const int x = 0xa;
        int8_t * ptr = (int8_t*)&x;
        printInBytes(ptr, sizeof(int));
        *ptr = 0xb; //undefined behavior
        printInBytes(ptr, sizeof(int));
        printf("%d\n", x);
        printInBytes(ptr, sizeof(int));
    }
    ```

    ![](img/cpp_1_1.png)
2. Constant on stack (not global function scope) with run-time value specified
    ```cpp
    int getFromUser() {
        int d;
        scanf("%d", &d);
        return d;
    }

    int main() {
        //run-time value definition
        const int x = getFromUser();
        int8_t * ptr = (int8_t*)&x;
        printInBytes(ptr, sizeof(int));
        *ptr = 0xb; //undefined behavior
        printInBytes(ptr, sizeof(int));
        printf("%d\n", x);
        printInBytes(ptr, sizeof(int));
    }
    ```

    ![](img/cpp_2_1.png)

3. Global constant specified on runtime
    ```cpp
    int getFromUser() {
        int d;
        scanf("%d", &d);
        return d;
    }

    //global constant initialized at run-time
    const int z = getFromUser();

    int main() {
        int8_t *ptr = (int8_t*)&z;
        printInBytes(ptr, sizeof(int));
        *ptr = 0xc;
        printf("After change\n");
        printf("%d\n", z);
        printInBytes(ptr, sizeof(int));
    }
    ```

    Insert as a user for example: 10. In hex 0xa, in the end we can see that our constant is overwritten. So we need to find out why.

    After we disassembly we got:
    ![](img/cpp_3_1.png)

    When we look closer at const address being at line main+29.
    We can read it's value with:

    ![](img/cpp_3_2.png)

    Using address we can see in which segment constant is written. Using:
    ```bash
    (gdb)$ info file
    ```

    In the end we can find a segment:
    ![](img/cpp_3_3.png)

4. Case when we have global but value is specified at compile time. Then the value is written to .rodata section.


### C++ summary
From this note you should probably remember only that trying to change const value is undefined behavior and it's could be really danger. So yeah don't do this.