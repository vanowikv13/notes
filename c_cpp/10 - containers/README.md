# C++ STL containers

## Sequence containers

### Vector - Sequence Container Representing an Array That Can Change in Size

```cpp
#include <vector>

// Creating a vector of type T
std::vector<T> vector_name;

// Accessing elements - constant O(1)
vector_name[0];

// Insert/delete element at the end - amortized constant O(1)
vector_name.push_back(t_elem);
vector_name.pop_back();

// Insert/delete element at the front - linear in the distance to the end of the vector O(n)
vector_name.insert(vector_name.begin(), t_elem);
vector_name.erase(vector_name.begin());
```

Vector uses contiguous storage locations for its elements that can be accessed with the `[]` operator or by offsetting their regular pointers to elements (like `vector_name.begin()`).

Using dynamically allocated arrays to store their elements, the array might need to reallocate if the size of the vector reaches its capacity. This is a relatively expensive operation, so vectors do not reallocate each time an element is added. All iterators and references are invalidated during reallocation.

The vector's pre-allocated size can be checked with the `capacity` function. Reallocation happens when the vector size reaches its capacity during the `push_back` call. The `reserve` function can be used to specify the number of elements to pre-allocate.

Vector template parameters should not **throw** while moving.

Compared to arrays:
- Use more memory to manage storage
- Can grow dynamically

Compared to other containers:
- Efficient in accessing its elements, just like in an array
- Efficient at adding/removing elements from the end, but less efficient when adding/removing from other positions.

**Data Structure**: Dynamic array
**Performance**: Access - O(1), Insert/Delete at End - O(1) amortized, Insert/Delete at Front - O(n)

[More details](https://en.cppreference.com/w/cpp/container/vector)

### List (Linked List)

```cpp
#include <list>

// Creating a list of type T
std::list<T> list_name;

// Insert/delete element at the front/end - always constant O(1)
list_name.push_front(t_elem);
list_name.pop_front();
list_name.push_back(t_elem);
list_name.pop_back();
```

A sequence container that allows iteration in both directions, implemented with double-linked lists. Similar to `forward_list`, which is implemented as a single-linked list. The ordering of a list is maintained internally by associating each element with a link to the preceding element.

Linked lists offer constant time removal/addition from the beginning or the end, and they can be iterated in both directions (unlike `forward_list`).

Compared to other containers:
- Perform better in inserting, extracting, and moving elements at any position within the container for which an iterator has already been obtained.
- Unlike a vector, a list doesn't provide random access to elements. It uses `std::advance` to iterate.
- Elements aren't stored in contiguous space like in a vector, so lists are not cache friendly.

**Data Structure**: Doubly-linked list
**Performance**: Insert/Delete at Front/End - O(1), Random Access - O(n)

[More details](https://en.cppreference.com/w/cpp/container/list)


### Deque (Double-Ended Queue)

An indexed sequence container that allows fast insertion/deletion at both the beginning and end. They're very similar to vectors but have more efficient insertion/deletion at the beginning. Unlike vectors, they are not guaranteed to store all their elements in contiguous storage locations. Accessing elements by offsetting a pointer to another element causes **undefined behavior**.

Vectors provide a similar interface, but they work differently. Vectors use a single array that occasionally has to be relocated, while a deque can be stored in different locations. A deque internally keeps pointers to each chunk of memory (each chunk is cache-friendly) and provides direct access to any of its elements in constant time.

For operations that involve a constant change in positions other than the beginning/end, they perform worse than a list.

```cpp
#include <deque>

// Creating a deque of type T
std::deque<T> deque_name;

// Random access - constant O(1)
deque_name[0];

// Insert/delete element at the front/end - constant O(1)
deque_name.push_front(t_elem);
deque_name.pop_front();
deque_name.push_back(t_elem);
deque_name.pop_back();

// Insert/delete element other than front/end - linear O(n)
deque_name.insert(deque_name.begin() + 1, t_elem);
deque_name.erase(deque_name.begin() + 1);
```

**Data Structure**: Double-ended queue
**Performance**: Access - O(1), Insert/Delete at Front/End - O(1), Insert/Delete at Middle - O(n)

[More details](https://en.cppreference.com/w/cpp/container/deque)


## Associative Containers / Unordered Associative Containers

Associative containers implement sorted data structures that can be quickly searched (O(log n) complexity), typically implemented by a type of balanced binary-tree (usually RBT).

Unordered associative containers implement non-sorted data structures (hashed) that can be quickly searched (O(1) average, O(n) worst-case complexity).

### Set / Multiset

A set is an associative container that stores a set of unique objects of type Key that are sorted.

A multiset is almost identical to a set, with the main exception that multiple keys with equivalent values are allowed.

```cpp
#include <set>

std::set<Key> set_name;
std::multiset<Key> mset_name;

// All these operations have O(log n) complexity (also for multiset)
set_name.insert(key_elem);
set_name.erase(key_elem);
set_name.contains(key_elem);
```

**Data Structure**: Balanced binary tree (e.g., Red-Black Tree)
**Performance**: Insert/Delete/Find - O(log n)

### Unordered Set / Unordered Multiset

An unordered set is identical to a set but implemented with a hash table instead of a binary tree, usually slightly faster than a set.

An unordered multiset is identical to an unordered set, with the main exception that multiple keys with equivalent values are allowed.

```cpp
#include <unordered_set>

std::unordered_set<Key> uset_name;
std::unordered_multiset<Key> umset_name;

// All these operations have O(1) average, O(n) worst-case complexity (also for unordered multiset)
uset_name.insert(key_elem);
uset_name.erase(key_elem);
uset_name.contains(key_elem);
```

**Data Structure**: Hash table
**Performance**: Insert/Delete/Find - O(1) average, O(n) worst-case

### Map / Multimap

A map is an associative container that stores elements formed by a combination of a key value and a mapped value, following a specific order.

A multimap is similar to a map, but it allows multiple elements with the same key.

```cpp
#include <map>

std::map<Key, Val> map_name;
std::multimap<Key, Val> mmap_name;

// O(log n) - store element at the key (not available for multimap)
map_name[key] = val;

// O(log n) - inserts elements into the container if the container doesn't already contain an element with an equivalent key
map_name.insert({key_elem, val_elem});

// O(log n) - inserts an element or assigns to the current element if the key already exists (not available for multimap)
map_name.insert_or_assign(key_elem, val_elem);

// O(log n) - inserts in-place if the key does not exist, does nothing if the key exists (not available for multimap)
map_name.try_emplace(key_elem, val_elem);
```

**Data Structure**: Balanced binary tree (e.g., Red-Black Tree)
**Performance**: Insert/Delete/Find - O(log n)

### Unordered Map / Unordered Multimap

An unordered map is identical to a map but implemented with a hash table instead of a binary tree, usually slightly faster than a map.

An unordered multimap is identical to an unordered map, with the main exception that multiple keys with equivalent values are allowed.

```cpp
#include <unordered_map>

std::unordered_map<Key, Val> umap_name;
std::unordered_multimap<Key, Val> ummap_name;

// All operations on unordered map or unordered multimap have O(1) average, O(n) worst-case complexity
umap_name.insert({key_elem, val_elem});
umap_name.erase(key_elem);
```

**Data Structure**: Hash table
**Performance**: Insert/Delete/Find - O(1) average, O(n) worst-case

[Iterator invalidation](https://stackoverflow.com/questions/6438086/iterator-invalidation-rules-for-c-containers):
![container invalidation](./invalidate.png)

### Additional Resources
- [Containers functions complexity](https://alyssaq.github.io/stl-complexities/)
- [Other tutorial about containers](https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md)
- [Video tutorial](https://www.youtube.com/watch?v=6OoSgY6NVVk&list=PLrOv9FMX8xJEEQFU_eAvYTsyudrewymNl&index=12)
- [Cheat Sheets Containers](https://hackingcpp.com/cpp/cheat_sheets.html#hfold2a)