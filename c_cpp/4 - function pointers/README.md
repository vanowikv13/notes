# Day 4 - Function pointers and conditional ? : operator
## Chapters
    1. Conditional ? : operator
    2. Function pointer
### 1. Conditional ? : operator
Conditional operator is shorter way to write a if statement with else.
      
- [ ] STRUCTURE
1. In typical way:
```cpp
if(condition)
    variable = x;
else
    variable = y;
```
2. Conditional ? : operator:
```cpp
variable = condition ? x : y;
```
- [ ] EXAMPLE (remember that before you will use this code first declar x variable as int)
1. In typical way:
```cpp
int x;

if(2 > 3)
    x = 123;
else
    x = 321;
```
2. Conditional ? : operator:
```cpp
x = (2 > 3) ? 123 : 321;
```

## 2. Function Pointer
### Function pointer like name says it's pointer to function, so it's some variable when we can store pointer to function and later use this pointer as function.
##### The most important question why to use it the most time because it's easier to use pointer to function than transport data in other way.
Let's say that you have some object and this object making every day some action(method) and this action is every time different.
So have method action but what will be next? You can realizate this by writing to the action method pointer to function in argument and you
will only send it to the obejct and object will just call it.
### How it's work in code?
      
Here you have typical form how to create pointer to function:
```
function_return_type (*pointer_name)(arguments ...);
```

Example:
```cpp
void(*pointer)(); // return void and don't have any argument.

int(*pointer2)(int,int); //return int and have two int argument

string(*pointer3)(char *tab, int); //return string and have arguments pointer to char and int

// return void and have int, string and pointer to function argument.
void(*writeData)(int,string, string(*function_with_data)(int));
             
// function that will be assigned to pointer
void sayHello() {
       std::cout << "Hello World " << std::endl;
}
             
pointer = &sayHello; //we put into our pointer address of function sayHello

// function call with pointer look just like normal function call
pointer();
```            
                      
                      
                      
### For more example and other situation go to main.cpp