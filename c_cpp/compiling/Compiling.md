# Compilers

## Stages of compilation

1. First there is source code in c or cpp files.

2. Preprocessor is called, which preprocess all preprocessor commands started with "#" sign and include header files (.h).

3. Compiler is called to translate source code into assembly language without resolving any external dependencies or adding startup information. Produces *.s files.

4. Then assembler is called to prepare object files. Each object file has its own address space.

5. Linker combines object files into an executable file, by relocating object's text and data segments, resolved yet unresolved symbols and create entry point for executable.

### Output from different compilation stages with GCC

```bash
$ ls
main.c

# Get preprocessed main.c file.
$ gcc -E main.c -o main.i

# Get code from main.c as assembly.
$ gcc -S main.c -o main.s

# Get object file form main.c.
$ gcc -c main.c -o main.o

# Get executable program after linking (consolidation).
$ gcc main.c -o main
```

### GCC options

```bash
# Search in directory dir header files.
gcc -I dir ...

# Search in directory dir libraries.
gcc -L dir ...
```



Links:
https://docs.google.com/document/d/1zdJlFogCDhKt2Qz_1LDhJ1r0UAuSUKzoMnbyaKc4or4/edit