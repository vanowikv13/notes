# Day 3 - Some of the most useful concept of c++ 11

### The full article contain also example in main.cpp

## 1. nullptr
The nullptr keyword represents a null pointer value

### Rules
1. We now shouldn't use previous NULL to initialize pointer
2. You cannot take address of nullptr, because it's prvalue
3. This is build especially to use it for pointer

```cpp
#include <iostream>

void test(int t) {
    std::cout << "Test int" << std::endl;
}

void test(int* t) {
    std::cout << "Test pointer" << std::endl;
}


int main() {
    //error compile do not know which one to call, cause NULL is define to integer 0
    //test(NULL);

    //nullptr is object of nullptr_t
    //output: Test pointer
    test(nullptr);

    //in the end on assembly level both NULL and nullptr are probably the same value (0x0)
    //so it's means that compilers take care for handling null pointer value.
}
```

## 2. Initializer_list
Initializes an object from braced-init-list

### The most important information
1. Every object have to be this same type
2. Object with initialization list send to it on construction is automatically constructed by compiler from initialization list
3. Whenever initialization list is used by other types it shall always include initialization_list header
4. The lifetime depends of how you use it's object but usually it's stack lifetime
5. You can make constructor used this type by argument and it is used in first place than other one
6. It can be used whatever data have to be send like array, vector etc.

Method of initializer_list object:
 - size(); 
 - begin();
 - end();

## 3. for each
Used to write really easily loop that make something for each element
### Rules
1. Normal keyword for (type argument: array) {} (in array place can be vector, tab[], etc.)
2. In type place can the best one is auto keyword

```cpp
int tab[3] = { 1,2,3 }; //
    //for each loop
    for (auto i : tab)
        cout << tab[i] << endl;
```

### More information and links I use:
* http://en.cppreference.com/w/cpp/language/list_initialization
* https://www.youtube.com/watch?v=U6mgsPqV32A
* https://stackoverflow.com/questions/1282295/what-exactly-is-nullptr