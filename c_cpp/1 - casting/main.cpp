#include <iostream>
#include <string>
#include <utility>
#include <cassert>
#include <cstdint>

struct Animal {
	int age;
	explicit Animal(int ag) : age(ag) {}
	virtual void sayHello() {
	    std::cout << "I'm Animal" << std::endl;
	};
};

struct Cow : virtual Animal {
    Cow() : Animal(12) {}

    virtual void sayHello() {
        std::cout << "I'm Cow" << std::endl;
    };
};

struct Pig : virtual Animal {
    Pig() : Animal(14) {}

    virtual void sayHello() {
        std::cout << "I'm Pig" << std::endl;
    };
};

struct Predator {
    std::string attack;
    explicit Predator(std::string a) : attack(std::move(a)) {}

    virtual void makeAttack() {
        std::cout << attack << std::endl;
    }
};

struct Tiger : Animal, Predator {
	int fangs;
	Tiger(int ag,int fg): Animal(ag), Predator("Bite"), fangs(fg) {}

	virtual void sayHello() {
		std::cout << "Hello I'm a tiger" << std::endl;
	}

	const int& getFangs() {
	    return const_cast<const int&>(fangs);
	}
};

class Car {
    std::string components[2] = {"Lights", "Wheel"};
    int other = 123;
public:
    const std::string* getComponents() {
        return const_cast<const std::string*>(components);
    }

    const int& getOther() {
        return const_cast<const int&>(other);
    }
};

void whichAnimal(Animal* ptr) {
    std::cout << "Your type is: ";
    if(dynamic_cast<Tiger*>(ptr))
        std::cout << "Tiger" << std::endl;
    else if(dynamic_cast<Cow*>(ptr))
        std::cout << "Cow" << std::endl;
    else if(dynamic_cast<Pig*>(ptr))
        std::cout << "Pig" << std::endl;
    else
        std::cout << "other" << std::endl;
}

int main()
{
	//static cast example -------------------------------------------------------------
	double y = 3.13;
	int x = static_cast<int>(y);
	void* xd = static_cast<void*>(&y);
	double* xf = static_cast<double*>(xd);

	// Const cast -------------------------------------------------------------------
	// pointer example
	int cx = 12;
	const int* ptr = &cx;
	int* cxd = const_cast<int*>(ptr);
	*cxd = 123;
	std::cout << cx << std::endl; //123

	// reference example
	int cx1 = 12;
	const int& ref = cx1;
	int& cxd1 = const_cast<int&>(ref);
	cxd1 = 123;
	std::cout << cx1 << std::endl; //123

	// wrong pointer example
	const int cx2 = 12;
	const int* ptr2 = &cx2;
	int* cxd2 = const_cast<int*>(ptr2);
	//*cxd2 = 123; //ERROR: undefined behavior
	std::cout << cx2 << std::endl; //123

	//adding constant to an array pointer in class and latter on removing constant
	Car car;
	const std::string* components = car.getComponents();
	std::cout << components[0] << ", " << components[1] << std::endl;
	auto* nonConst = const_cast<std::string*>(components);
    nonConst[0] = "123";
    std::cout << components[0] << ", " << components[1] << std::endl;

    //adding constant to an variable and then letter on removing constant
    const int& other = car.getOther();
    std::cout << other << std::endl;
    auto& nonConstOther = const_cast<int&>(other);
    nonConstOther = 10;
    std::cout << other << std::endl;


    // Dynamic cast ------------------------------------------------------------
    Animal * animal = new Tiger(12, 32);
    //casting to child
    Tiger * tiger = dynamic_cast<Tiger*>(animal);
    //casting to another parent
    Predator * predator = dynamic_cast<Predator*>(tiger);
    predator->makeAttack();
    //casting again from Tiger* to Predator*
    predator = dynamic_cast<Predator*>(animal);
    if(predator == nullptr)
        std::cout << "1. bad dynamic cast" << std::endl;
    else
        predator->makeAttack();

    //casting from Animal* to Predator*
    Animal *animal1 = new Animal(12);
    predator = dynamic_cast<Predator*>(animal1);
    if(predator == nullptr)
        std::cout << "2. bad dynamic cast" << std::endl;
    else
        predator->makeAttack();

    //downcast
    //here we see that we cannot downcast if we're not casting to correct type
    Cow* c = dynamic_cast<Cow*>(animal1);
    if(c == nullptr)
        std::cout << "3. bad dynamic cast" << std::endl;
    else
        c->sayHello();

    //downcast
    Tiger * tiger1 = dynamic_cast<Tiger*>(animal1);
    if(tiger1 == nullptr)
        std::cout << "4. bad dynamic cast" << std::endl;
    else
        tiger1->sayHello();

    //here we cannot side cause cause Tiger* has additional parent than Cow
    Cow *sTiger = dynamic_cast<Cow*>(tiger);
    if(sTiger == nullptr)
        std::cout << "5. bad dynamic cast" << std::endl;
    else
        sTiger->sayHello();

    //here we also cannot sidecast
    Cow *cow = new Cow();
    Pig *pig = dynamic_cast<Pig*>(cow);
    if(pig == nullptr)
        std::cout << "6. bad dynamic cast" << std::endl;
    else
        pig->sayHello();

    //check which type is which with dynamic cast
    whichAnimal(cow);

    //reinterpret_cast
    float f = 13.3;
    //address of f is exactly in fptr
    int * fptr = reinterpret_cast<int*>(&f);
    std::cout << *fptr << std::endl; //we get some weird number cause reinterpret_cast doesn't do any arithmetic
    *fptr = 1;
    std::cout << f << std::endl;

    Animal ani(12);
    std::cout << ani.age << std::endl;
    double *aptr = reinterpret_cast<double*>(&ani);
    std::cout << *aptr << std::endl; //some weird number
    *aptr = 1234.1232131; //as you can see can write to this address, using it as double

    Cow * rcow = new Cow();
    //unlike dynamic_cast we can convert to siblings types
    //but it's always safer to use dynamic_cast, even if this operation could not be done
    Pig * rpig = reinterpret_cast<Pig*>(cow);
    rpig->age = 123;

    //example bellow might work on your computer but it's undefined behavior
    Car * rcar = new Car();
    Animal * animalCar = reinterpret_cast<Animal*>(rcar);
    //what??? animalCar is pointing to Car object not Animal
    //using object of one type not being in any way connected with polymorphic type like other type is undefined behavior
    animalCar->age = 123; //undefined behavior
    std::cout << animalCar->age << std::endl; //undefined behavior

    //pointer to integer and back
    int arr[3] = {1, 2, 3};
    int *arrPtr = arr;
    auto first = reinterpret_cast<std::uintptr_t>(arrPtr); // static_cast is an error
    auto second = reinterpret_cast<std::uintptr_t>(arr+2);
    std::cout << "diff: " << (second - first) / sizeof(int) << std::endl;
    arrPtr = reinterpret_cast<int*>(first);
    std::cout << *arrPtr << std::endl; //ptr still points to first element of array

    // checking encoding
    int check = 0xff;
    int8_t* checkPtr = reinterpret_cast<int8_t*>(&check);
    if(checkPtr[0] == '\xff')
        std::cout << "Little endian" << std::endl;
    else
        std::cout << "Big-endian" << std::endl;

    //nullptr example
    //ERROR - not allowed conversion: Animal* animalx = reinterpret_cast<Animal*>(nullptr);
    Animal * nullAnimal = nullptr;
    // in this way it's allowed
    Tiger* nullTiger = reinterpret_cast<Tiger*>(nullAnimal);
    return 0;
}

