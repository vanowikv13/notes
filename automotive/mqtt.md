# MQTT

### Publish - Subscribe Pattern
![](img/mqtt.png)


MQTT Broker -  server that receives all messages from the clients and then routes the messages to the appropriate subscribers. An MQTT client is any device (from a micro controller up to a fully-fledged server) that runs an MQTT library and connects to an MQTT broker over a network.


### Topics - place when we want to store information
You don't need to create topic if client send that he want to publish to topic then it's created. Topics name should be short cause they are send always with each package.
```
food/vegetables/tomato  - topic
food/+/tomato - same topic like above
food/vegetables/# - select all vegetables
```

### Quality of service for publisher
QoS0 - At most one delivery (only one sent not receive from broker)
* recommended if messages queries is not required

If you disconnect then you should call to connect: CONNACK

Queued messages - MQTT broker will queue all QoS1 and QoS2 messages if you in persistent session while you're offline. You are able to expire also messages after some time.

### Clean session
* Stored client information session is deleted as soon we disconnects.


### Clean session vs Persistent session:
Clean session recommendations:
* a client only needs to publish
* message loss is acceptable

Persistent Session is recommended when:
* subscribers must not miss messages
* the broker should store subscription information


### Retained messages
Last message sent by the publisher stored on MQTT broker so when new subscribers are connected they can start their instance with last value and not the empty. While waiting for new to comes.

### Last Will & Testament (LWT)
Is a notification (send as retained message) to other clients when a client disconnect without a disconnect message. (for example we lose connectivity cause of power issues). So this client subscribers that subscribe to the topic of provider will get the message about client disconnect.

Connect parameters for last will:
* lastWillTopic: what topic should be sent to subscribers if our client dies.
* lastWillMessage: last message
* lastWillRetain: flag to set if retain

LWT usages:
* I\O error or network errors
* the client fails with the Keep Alive period
* the client do not send disconnect packages before it closes a network.
* the broker closes the connection of a protocol error.

Best practices:
* online/offline mechanism


### Keep Alive
Ensures that connection between MQTT Broker and the client is still open and both side are aware of connection being open.

If the client does not send any message during the keep alive time. It must send a PINGREQ package to the MQTT Broker to confirm that connection is still alive. Broker to confirm to the client that is still alive send PINGRESP.

PINGREQ - message from client that indicates to the broker that the client is still alive.

PINGRESP - response from the MQTT Broker to PINGREQ.


### Client Takeover
If client disconnect from MQTT Broker and send a CONNECT message with the same id to the broker and broker detect half-open connection then it performs a client takeover. The broker close previous connection with the same client and establish a new connection with the same client.

### Subscription vs shared subscription
If we have multiple clients connected to publisher with normal subscription they when new message appears they will got all the copy of message.

In shared subscription client are split with messages so for example we would use this to split a traffic of a web to not overload a single server.