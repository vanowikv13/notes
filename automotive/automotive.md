# CAN

In short, the Controller Area Network (CAN) is a standard used to allow Electronic Control Units (ECUs) to communicate in an efficient manner without a central computer. 

Messages are broadcast in a system that requires very little physical wiring making CAN bus low cost, robust and efficient. 

This is where the CAN standard comes in handy:

The CAN bus system enables each ECU to communicate with all other ECUs - without complex dedicated wiring.

Specifically, an ECU can prepare and broadcast information (e.g. sensor data) via the CAN bus (consisting of two wires, CAN low and CAN high). The broadcasted data is accepted by all other ECUs on the CAN network - and each ECU can then check the data and decide whether to receive or ignore it.


![](img/can.png)


![](img/frame.png)


Shortcuts:
NTP - Network time protocol - responsible for synchronization of elements.
SWCs - Software components - simples form of an aplication with certain functionality
MQTT Broker -  server that receives all messages from the clients and then routes the messages to the appropriate destination clients. An MQTT client is any device (from a micro controller up to a fully-fledged server) that runs an MQTT library and connects to an MQTT broker over a network.

# AUTOSAR
AUTOSAR - Automotive Open Systems Architecture
* Software widly independend of chardware
* Decoupling development activities
* Reuse the software modules

![](img/autosar.png)

BSW - basic software layers - responsible for managing hardware resources and providing common services for application software. Consist of:
* Microcontroller Abstraction Layer (MCAL) - provides direct access to all the On-Chip MIcrocontroller Peripheral and External Devices with are mapped to Memory. (drivers to devices etc. like: Microcontroller Drivers, Memory Drivers, Communication Drivers, I/O Drivers)
* ECU Abstraction Layer - makes higher levels independent of ECU hardware
* Service Layer - Highest layer in basic software layer. Provides background services fro applications, RTE, and BSW modules. (like: operating systems functionality, Vehicle network communication, diagnostic services ...)

RTE - Runtime Enviroment - Layer between aplication layer and basic software layer. Software components communicate with other components through RTE. RTE is used to create software independend of ECU.

Aplication Layer - represented as SWCs - Software components - simples form of an aplication with certain functionality.
